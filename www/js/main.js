function reload() {
    window.location.reload(true);
}

function loader(status, elem) {
    if(status && typeof elem != 'undefined') {
        elem
            .css({position: (elem.css('position')=='absolute'?'':'relative')})
            .append(
                $('<div id="loader-block" />')
                    .css({
                        position:'absolute',
                        height:'100%',
                        width:'100%',
                        top:'0',
                        left:'0',
                        background:'url(/www/images/loader.gif) no-repeat 50% 50% rgba(0,0,0,0.6)'
                    })
            )
        ;
    } else {
        $('#loader-block').remove();
    }
}

function showShade(reloadAfter) {
    $('<div id="screen" />')
        .append($('<div id="modal" />')
            .append($('<span class="closeModal" />').html('&times;')
                .on('click', function () {
                    hideShade(reloadAfter);
                }))
            .append($('<div class="body" />')))
        .appendTo('body');
    $('#screen')
        .on('click', function () {
            hideShade(reloadAfter);
        });
    $('#modal')
        .on('click', function (e) {
            e.stopPropagation();
        });
    window.scrollTo(0, 0);
    $('html, body').css({
        overflowY: 'hidden'
    });
}

function hideShade(reload) {
    $('#modal').remove();
    $('#screen').fadeOut(400, function () {
        $(this).remove();
    });
    $('html, body').css({
        overflowY: 'auto'
    });

    if (reload) {
        window.location.reload(true);
    }
}
function openModal(content, reloadAfter) {
    if(typeof reloadAfter == 'undefined')
        reloadAfter = false;

    showShade(reloadAfter);
    $('#modal .body').html(content);
}

function loadModal(url,text) {
    showShade();
   
    $('#modal .body').load(url);
}

function closeModal() {
    hideShade();
}

/*function disableForm(select) {
    var form = $(select);
    form.each('input,select', function(){
        $(this).prop('disabled',true);
    });
}

function enableForm(select) {
    var form = $(select);
    form.each('input,select', function(){
       $(this).prop('disabled',false);
    });
}*/

(function ($) {

    var directionsDisplay = new google.maps.DirectionsRenderer({draggable: false}),
        directionsService = new google.maps.DirectionsService(),
        map,
        riga = new google.maps.LatLng(56.93546935137281, 24.100531451404095),
        bounds = null,
        boundsReady = false,
        preDefined = false;

    function setDeparture(loc) {
        $('#order_departure_place,#cost_departure_place')
            .val(loc).css({outline:loc.length>0?'none':'red solid 2px'});
    }

    function setArrival(loc) {
        $('#order_arrival_place,#cost_arrival_place')
            .val(loc).css({outline:loc.length>0?'none':'red solid 2px'});
    }

    function triggerGeocode() {
        $('#cost_departure_place, #cost_arrival_place, #order_departure_place, #order_arrival_place')
            .trigger('geocode');
    }

    function triggerBounds() {
        if(bounds!==null && boundsReady) {
            map.fitBounds(bounds);
        }
    }

    function resetBounds() {
        bounds = null;
        boundsReady = false;
    }

    function resetRouteSelect() {
        $('#defined-places').val(0);
    }

    function setPrice(distance, duration) {
        var rate_value = parseFloat($('#rate-value').val()),
            dur_rate = parseFloat($('#duration-rate').val()),
            commission = parseFloat($('#commission').val()),
            price = parseFloat(
                (((distance / 1000) * rate_value) + ((duration / 60) * dur_rate)) + commission
            ).toFixed(2);

        if(typeof distance != 'undefined') {
            if(distance === true) {
                $('#total, #total_distance').html($('<img src="/www/images/loading.gif" alt="Loading" />'));
                $('#total_distance_input').val(0);
            } else {
                $('#total, #total_distance').html(price + ' EUR');
                $('#total_distance_input').val(distance);
            }
        } else {
            $('#total, #total_distance').empty().html('0.00 EUR');
            $('#total_distance_input').val(0);
            directionsDisplay.set('directions', null);
            map.setZoom(13);
            map.setCenter(riga);
        }
    }

    function calcRoute(form_departure_place, form_arrival_place) {

        if(typeof form_departure_place == 'undefined' || typeof form_arrival_place == 'undefined') {

            setPrice();

        } else {
            if(form_departure_place.length>0 && form_arrival_place.length>0) {
                setPrice(true);
            }

            var request = {
                origin: form_departure_place,
                destination: form_arrival_place,
                travelMode: google.maps.TravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    bounds = response.routes[0].bounds;//(new google.maps.LatLngBounds()).extend(response.routes[0].bounds);
                    console.log(response);
                    directionsDisplay.setDirections(response);
                } else {
                    setPrice();
                }
            });

        }
    }

    function computeTotalDistance(result) {
        var total = 0;
        var myroute = result.routes[0];
        for (var i = 0; i < myroute.legs.length; i++) {
            total += myroute.legs[i].distance.value;
        }
        return total;
    }

    function computeTotalDuration(result) {
        var total = 0;
        var myroute = result.routes[0];
        for (var i = 0; i < myroute.legs.length; i++) {
            total += myroute.legs[i].duration.value;
        }
        return total;
    }

    $(document)
        .on('focus', '.datepicker input, input.datepicker', function () {
            $(this).datepicker(window.datePicker);
        })
        .on('focus', 'input.datetimepicker', function () {
            $(this).datetimepicker(window.dateTimePicker);
        })
        .on('hide-hint', '[data-hint]', function(){
            var target = $(this),
                hint = $(target.data('hint'));

            hint.fadeOut('fast');
        })
        .on('click', '[data-hint]', function(){
            var target = $(this),
                selector = target.data('hint'),
                hint = $(selector);

            $('[data-hint]:not([data-hint="'+selector+'"])').trigger('hide-hint');
            hint.fadeToggle('fast');
        })
        .on('change', 'select[data-select-toggle]', function(){
            var target = $(this),
                val = target.val(),
                toggle = $(target.data('select-toggle'));

            if(val==0) {
                toggle.fadeOut();
            } else {
                toggle.fadeIn();
            }
        })
        .on('change', '#defined-places', function () {
            var target = $(this),
                url = target.attr('data-routes'),
                val = target.val();
            if(parseInt(val)!==0) {

                $.post(url, {
                    ajax_routes: val
                })
                    .done(function (data) {
                        if (data !== undefined) {
                            $('input[name=address_name]').val(data['address_name']).css({outline:data['address_name'].length>0?'none':'red solid 2px'});
                            $('input[name=address_surname]').val(data['address_surname']).css({outline:data['address_surname'].length>0?'none':'red solid 2px'});
                            $('input[name=address_phone]').val(data['address_phone']).css({outline:data['address_phone'].length>0?'none':'red solid 2px'});
                            $('input[name=address_email]').val(data['address_email']).css({outline:data['address_email'].length>0?'none':'red solid 2px'});
                            setDeparture(data['location_from']);
                            setArrival(data['location_to']);

                            triggerGeocode();

                            calcRoute(data['location_from'], data['location_to']);
                            //$('input[type="checkbox"][name="save_route"]').prop('disabled',true);
                            preDefined = true;
                        }
                        if (data == 'Error!') {
                            alert('Error!');
                        }
                    });

            } else {
                $('input[name=address_name]').val('');
                $('input[name=address_surname]').val('');
                $('input[name=address_phone]').val('');
                $('input[name=address_email]').val('');
                setDeparture('');
                setArrival('');
                //$('input[type="checkbox"][name="save_route"]').prop('disabled',false);
                preDefined = false;
                resetBounds();

                //triggerGeocode();

                setPrice();
            }
        })
        .ready(function () {

            /*-------------------------------------------------- MAPS ------------------------------------------*/

            function initialize() {

                var mapOptions = {
                    center: riga,
                    zoom: 13,
                    panControl: false,
                    zoomControl: true,
                    zoomControlOptions: {
                        style: google.maps.ZoomControlStyle.LARGE,
                        position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    
                    scaleControl: false,
                     scaleControlOptions: {
                         position: google.maps.ControlPosition.TOP_RIGHT
                    },
                    streetViewControl: false,
                    scrollwheel: false
                };
                map = new google.maps.Map(document.getElementById('map-block'), mapOptions);
                directionsDisplay.setMap(map);

                google.maps.event.addListener(directionsDisplay, 'directions_changed', function () {
                    var directions = directionsDisplay.getDirections(),
                        distance = computeTotalDistance(directions),
                        duration = computeTotalDuration(directions);
                    google.maps.event.trigger(map, 'resize');
                    setPrice(distance, duration);
                });

                google.maps.event.addListener(map, 'bounds_changed', function() {
                    boundsReady = true;
                    if(bounds!==null) {
                        triggerBounds();
                    }
                });

                google.maps.event.addListener(map, 'mouseover', function() {
                    resetBounds();
                });

                if($('#cost_departure_place').val().length>0 && $('#cost_arrival_place').val().length>0) {
                    $('#cost_departure_place, #cost_arrival_place').trigger('geocode');
                    calcRoute($('#cost_departure_place').val(), $('#cost_arrival_place').val());
                }

            }

            $('#cost_departure_place, #cost_arrival_place').on('geocode:result', function (e, result) {
                var cost_dep = $('#cost_departure_place').val();
                var cost_arr = $('#cost_arrival_place').val();
                var str_nr_err = $('#street_nr_error');
                var regex = /([0-9]+)/;
                if(!cost_dep.match(regex) || !cost_arr.match(regex)) {
                    str_nr_err.fadeIn();
                    str_nr_err.find('label div')
                        .css({color:'red',fontWeight:'bold'});
                    str_nr_err.find('input').on('click', function(){
                        if($(this).is(':checked')) {
                            str_nr_err.find('label div')
                                .css({color:'',fontWeight:''});
                        }
                    });
                }
                setDeparture(cost_dep);
                setArrival(cost_arr);
                resetBounds();
                calcRoute(cost_dep, cost_arr);
            });
            $('#order_departure_place, #order_arrival_place').on('geocode:result', function (e, result) {
                var cost_dep = $('#order_departure_place').val();
                var cost_arr = $('#order_arrival_place').val();
                var str_nr_err = $('#street_nr_error');
                var regex = /([0-9]+)/;
                if(!cost_dep.match(regex) || !cost_arr.match(regex)) {
                    str_nr_err.fadeIn();
                    str_nr_err.find('label div')
                        .css({color:'red',fontWeight:'bold'});
                    str_nr_err.find('input').on('click', function(){
                        if($(this).is(':checked')) {
                            str_nr_err.find('label div')
                                .css({color:'',fontWeight:''});
                        }
                    });
                }
                setDeparture(cost_dep);
                setArrival(cost_arr);
                resetBounds();
                calcRoute(cost_dep, cost_arr);
            });

            google.maps.event.addDomListener(window, 'load', initialize);

            $('#cost_departure_place, #cost_arrival_place, #order_departure_place, #order_arrival_place')
                .geocomplete({map: '#map-block', country: 'lv'})
                .css({border:'1px solid #F60'})
                .on('focus', function(){
                    $(this).select();
                })
                .on('change', function(){
                    if(preDefined) {
                        preDefined = false;
                        triggerGeocode();
                        resetRouteSelect();
                        resetBounds();
                    }
                })
            ;
            /*------------------------------------------ //MAPS ----------------------------------------*/

            /*-----------------------------------------------LOGIN/REG-------------------------------------------*/
            var login = $('#login'),
                reg = $('#register'),
                open = false;
            login.on('click', 'span:first-child', function () {
                var parent = $(this).parent('div');
                parent.toggleClass('active');
                reg.removeClass('active');
                open = parent.hasClass('active');
            });
            reg.on('click', 'span:first-child', function () {
                var parent = $(this).parent('div');
                parent.toggleClass('active');
                login.removeClass('active');
                open = parent.hasClass('active');
            });
            $('body').on('click', function(e){
                var target = $(e.target);
                if(open && (!target.parents('#login').length && !target.parents('#register').length)) {
                    $('#login, #register').removeClass('active');
                    open = false;
                }
            });
            /*----------------------------------------------- // LOGIN/REG  -------------------------------------------*/

            function initSlider() {
                $(window).trigger('resize');
                $('.flexslider')
                    .flexslider({
                        animation: "slide",
                        controlNav: true,
                        controlsContainer: ".flexslider",
                        directionNav: false,
                        animationLoop: false,
                        slideshow: true,
                        slideshowSpeed: 4000
                    });
            }

            /*--------------------------------------- NAV ---------------------------------------*/
            $('#menu-how').on('click', function () {
                $('#menu-how-block').css('display', 'inline-block');
                $('#menu-cost-block,#menu-order-block').hide();
                $('.road-holder').show();
                $(this).addClass('active-menu-button');
                $('#menu-cost,#menu-order').removeClass('active-menu-button');

                window.location.hash = '';

                initSlider();
            });
            $('#menu-cost, #backward').on('click', function () {
                $('#menu-cost-block').css('display', 'inline-block');
                $('#menu-how-block,#menu-order-block').hide();
                $(this).addClass('active-menu-button');
                $('#menu-how,#menu-order').removeClass('active-menu-button');
                $('.road-holder').hide();
                triggerBounds();
                google.maps.event.trigger(map, 'resize');

                window.location.hash = '#price';

            });
            $('#menu-order, #forward').on('click', function () {
                $('#menu-order-block').css('display', 'inline-block');
                $('#menu-how-block,#menu-cost-block').hide();
                $(this).addClass('active-menu-button');
                $('#menu-how,#menu-cost').removeClass('active-menu-button');
                $('.road-holder').hide();

                window.location.hash = '#order';

            });
            /*--------------------------------------- //NAV ---------------------------------------*/


            $(window).load(function () {
                var hash = window.location.hash;
                if(hash.length > 1) {
                    switch(hash) {
                        case '#order':
                            $('#menu-order').trigger('click');
                            break;
                        case '#price':
                            $('#menu-cost').trigger('click');
                            break;
                    }
                }
                initSlider();
            });

        })
        .on('click', 'input[type="checkbox"][data-slide-toggle]', function () {
            var target = $(this),
                elem = $(target.data('slide-toggle'));

            if (target.is(':checked')) {
                elem.slideDown();
            } else {
                elem.slideUp();
            }
        })
        .on('click', 'a[data-confirm]', function (e) {
            if (!confirm($(this).data('confirm'))) {
                e.preventDefault();
            }
        })
        .on('click', '#oj2', function(){
            $('#company-requisites').slideToggle();
        })

        .on('submit', '#order-form', function (e) {
            e.preventDefault();

            //disableForm('#order-form');

            $('input[name]').css({outline:'none'});

            var target = $(this),
                win;

            loader(true, $('#menu-order-block'));
            $.post(target.attr('action'), target.serialize() + '&ajax_order=true')
                .done(function (data) {
                    if (data.success === true && null !== data.payment_url) {
                        openModal(
                            $('<div />')
                                .css({textAlign:'center'})
                                .append(
                                    $('<p />')
                                        .html(data.phrase.pre)
                                )
                                .append(
                                    $('<a />')
                                        .attr({
                                            target: '_blank',
                                            href: data.payment_url
                                        })
                                        .addClass('payment_button')
                                        .on('click', function(e){
                                            e.preventDefault();
                                            win = window.open(data.payment_url, 'online_payment', 'height=360&width=240', true);
                                            if(win !== null) {
                                                win.focus();
                                            }
                                            //closeModal();
                                        })
                                        .html(data.phrase.pay)
                                )
                                .append(
                                    $('<p />')
                                        .html(data.phrase.post)
                                )
                        );
                    } else {
                        if(data.error !== null) {
                            var field;
                            $.each(data.error, function(i,error) {
                                if(error=='address_nr') {
                                    var str_nr_err = $('#street_nr_error');
                                    str_nr_err.fadeIn();
                                    str_nr_err.find('label div')
                                        .css({color:'red',fontWeight:'bold'});
                                    str_nr_err.find('input').on('click', function(){
                                        if($(this).is(':checked')) {
                                            str_nr_err.find('label div')
                                                .css({color:'',fontWeight:''});
                                        }
                                    });
                                } else {
                                    field = $('[name='+error+']');

                                    if(field.attr('type')=='checkbox') {
                                        field.next('label').find('div').css({color:'red',fontWeight:'bold'});
                                        field.on('change', function(){
                                            $(this).off('change')
                                                .next('label').find('div').css({color:'',fontWeight:''})
                                            ;
                                        });
                                    } else {
                                        field.css({outline:'red solid 2px'});
                                        field.on('input', function(){
                                            $(this).off('input').css({outline:'none'});
                                        });
                                    }
                                }
                            });

                        } else {
                            alert('Unknown error');
                        }

                        //enableForm('#order-form');
                    }
                })
                .always(function(){
                    loader(false);
                });
        })

        .on('submit', '#profile-form', function (e) {
            e.preventDefault();

            $('input[name^="user["]').css({outline:'none'});

            var target = $(this);

            loader(true, $('#profile-form'));
            $.post(target.attr('action'), target.serialize() + '&ajax_profile=true')
                .done(function (data) {
                    if (data.success === true) {
                        openModal(
                            $('<div />')
                                .css({textAlign:'center'})
                                .append(
                                    $('<p />')
                                        .html(data.phrase)
                                )
                        );
                    } else {
                        if(data.error !== null) {
                            var field;
                            $.each(data.error, function(i,error) {
                                field = $('input[name="user['+error+']"]');

                                if(field.attr('type')=='checkbox') {
                                    field.next('label').find('div').css({color:'red',fontWeight:'bold'});
                                    field.on('change', function(){
                                        $(this).off('change')
                                            .next('label').find('div').css({color:'',fontWeight:''})
                                        ;
                                    });
                                } else {
                                    field.css({outline:'red solid 2px'});
                                    field.on('input', function(){
                                        $(this).off('input').css({outline:'none'});
                                    });
                                }
                            });
                        } else {
                            alert('Unknown error');
                        }

                        //enableForm('#order-form');
                    }
                })
                .always(function(){
                    loader(false);
                });
        })

        .on('submit', '#reg-user form', function (e) {
            e.preventDefault();

            $('input[name^="register["]').css({outline:'none'});

            var target = $(this);

            loader(true, $('#reg-user'));
            $.post(target.attr('action'), target.serialize() + '&ajax_reg=true')
                .done(function (data) {
                    if (data.success === true) {
                        openModal(
                            $('<div />')
                                .css({textAlign:'center'})
                                .append(
                                    $('<p />')
                                        .html(data.phrase)
                                ),
                            true
                        );
                    } else {
                        if(data.error !== null) {
                            var field;
                            $.each(data.error, function(i,error) {
                                field = $('input[name="register['+error+']"]');

                                if(field.attr('type')=='checkbox') {
                                    field.next('label').find('div').css({color:'red',fontWeight:'bold'});
                                    field.on('change', function(){
                                        $(this).off('change')
                                            .next('label').find('div').css({color:'',fontWeight:''})
                                        ;
                                    });
                                } else {
                                    field.css({outline:'red solid 2px'});
                                    field.on('input', function(){
                                        $(this).off('input').css({outline:'none'});
                                    });
                                }
                            });
                        } else {
                            alert('Unknown error');
                        }

                        //enableForm('#order-form');
                    }
                })
                .always(function(){
                    loader(false);
                });
        })

        .on('submit', '#login-user form', function (e) {
            e.preventDefault();

            $('input[name^="user["]').css({outline:'none'});

            var target = $(this);

            loader(true, $('#login-user'));
            $.post(target.attr('action'), target.serialize() + '&ajax_login=true')
                .done(function (data) {
                    if (data.success === true) {
                        window.reload();
                    } else {
                        if(data.error !== null) {
                            var field;
                            $.each(data.error, function(i,error) {
                                field = $('input[name="user['+error+']"]');
                                field.css({outline:'red solid 2px'});
                                field.on('input', function(){
                                    $(this).off('input').css({outline:'none'});
                                });
                            });
                        } else {
                            alert('Unknown error');
                        }

                    }
                })
                .always(function(){
                    loader(false);
                });
        })


        .on('click', '[data-modal]', function (e) {
            e.preventDefault();
            var url = $(this).data('modal');
            loadModal(url);
        })
        .on('click', '#restore_submit', function(e){
            e.preventDefault();
            var val = $('#restore_email').val();
            var url = $('#restore_email').data('modal');
            $.post(url, {
                    email: val
                })
                    .done(function (data) {
                        loadModal(url);
                        /*if (data !== undefined) {
                           
                        }
                        if (data == 'Error!') {
                            alert('Error!');
                        }*/
                    });
        });

})(jQuery);
