
var geocoder = new google.maps.Geocoder();

function initialize() {

    if(document.getElementById("address_input")==undefined) return;

    var address = document.getElementById("address_input").value?document.getElementById("address_input").value:'RÄŤga';
    var lat=''
    var lng='';

    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {

            lat = results[0].geometry.location.lat();
            lng = results[0].geometry.location.lng();
            address = results[0].formatted_address;
            document.getElementById("address_input").value = results[0].formatted_address;

            var latLng = new google.maps.LatLng(lat, lng);
            var map = new google.maps.Map(document.getElementById('mapCanvas'), {
                zoom: 11,
                center: latLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
                position: latLng,
                title: address,
                map: map,
                draggable: markerDraggable
            });




            google.maps.event.addListener(marker, 'dragend', function() {

                geocoder.geocode({
                        latLng: marker.getPosition()
                    },
                    function(responses) {
                        if (responses && responses.length > 0) {

                            lat = responses[0].geometry.location.lat();
                            lng = responses[0].geometry.location.lng();

                            latLng = new google.maps.LatLng(lat, lng);

                            marker.setPosition(latLng);
                            marker.setTitle(responses[0].formatted_address);
                            document.getElementById("address_input").value = responses[0].formatted_address;

                        }
                    });

            });



        } else {
            document.getElementById('mapCanvas').innerHTML = "Geocode was not successful for the following reason: " + status

        }
    });

}



function updateMarkerPosition(latLng) {
    document.getElementById('info').innerHTML = [
        latLng.lat(),
        latLng.lng()
    ].join(', ');
}

function updateMarkerAddress(str) {
    document.getElementById('address').innerHTML = str;

}

google.maps.event.addDomListener(window, 'load', initialize);
