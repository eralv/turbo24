$(function(){
         $("#popup").addClass('mini-popup');
	 $("#user-submit").on('click', function(){
             $('#client-add').validate({
                rules: {
                    name: {
                      required: true,
                    },
                    surname: {
                      required: true,
                    },
                    identity_number: {
                      required: true,
                    },
                    email: {
                      required: true,
                      email: true
                    },
                    phone: {
                      required: true,
                      digits: true
                    },

                    
                },
                messages: {
                    email: {
                      required: "Ievadiet juusu email",
                      email: "E-mail vajag but formataa name@domain.com"
                    },

                }
             });
             var form = $('#user-form');
            form.attr({
                action: $(this).data('location'),
                method: 'post'
            });
         });
         $(".button-close").on('click', function(){
             $("#popup").remove();
             $("#screen").remove();
         });
});
