/*
 * Admin window
 */
function adminWindowClass(){
	
	this.windowClass = 'aWindow';
	this.append = 'body';
	this.window = null;
	
	this.openW = function( html )
	{
		hideShade(); showShade();
		jQuery('.' + this.windowClass).remove();
		var self = this;
		var w1 = jQuery('<div>').addClass(this.windowClass);
		var w2 = jQuery('<div>').html(html);
		var c = jQuery('<a>').addClass('close').html('x').click(function(){ self.closeW(); });
		w2.prepend(c);
		this.window = w1.append(w2);
		jQuery(this.append).append(this.window);
		rebindPencil();
	}
	
	this.closeW = function()
	{
		hideShade();
		this.window.remove();
		window.location = window.location.href;
	}
	
	
}

var adminWindow;

/*
 * other
 */
jQuery(document).ready(function(){
	
	adminWindow = new adminWindowClass;
	
	// admin bottom controls
	jQuery('#admin-bottom .toggle-arrow').click(function(){
		var p = jQuery(this).parent();
		
		if(p.hasClass('_close-bottom')){ p.addClass('close-bottom').removeClass('_close-bottom'); }
		else { p.addClass('_close-bottom').removeClass('close-bottom'); }
	})
	
	// edit controls on site menu
	jQuery('#sub-menu li:not(.add), #menu li:not(.home,.add), #footer-menu li:not(.home,.add)').hover(
		function(){ // mouse over
			var rel = jQuery(this).attr('rel');
			if(!rel) return;
			var s = jQuery('<span>Edit</span>');
			var d = jQuery('<div></div>')
				.click(function(){
				
					rel = rel.split('|');

					var id = isNaN(parseInt(rel[0])) ? 0 : parseInt(rel[0]);
					var parent = isNaN(parseInt(rel[1])) ? '' : '?parent=' + parseInt(rel[1]); 
					jQuery.ajax('/ll/ajax/admin/tree/' + id + parent).success(function(resp) 
					{ 
						adminWindow.openW(resp) ;
						/* BUG: dynamicall load langs*/
						/*tinyMCE.execCommand("mceAddControl", true, "description_lv");
						tinyMCE.execCommand("mceAddControl", true, "description_ru");
						tinyMCE.execCommand("mceAddControl", true, "description_en");*/
					}
					);
				})
				.addClass('edit-icon')
				.append(s);
			jQuery(this).prepend(d);
		},
		function(){ // mouse out
			jQuery('.edit-icon',this).remove();
		}
	);
	
	// add new meni item
	jQuery('#sub-menu li.add, #menu li.add, #footer-menu li.add').click(function(){
		var rel = jQuery(this).attr('rel').split('|');
		var parent = isNaN(parseInt(rel[1])) ? '' : '?parent=' + parseInt(rel[1]); 
		jQuery.ajax('/ll/ajax/admin/tree/0' + parent).success(function(resp) 
		{ 
			adminWindow.openW(resp); 
						/* BUG: dynamicall load langs*/
						/*tinyMCE.execCommand("mceAddControl", true, "description_lv");
						tinyMCE.execCommand("mceAddControl", true, "description_ru");
						tinyMCE.execCommand("mceAddControl", true, "description_en");*/
		}
		);
	});
	
	var sortingArr = {};
	jQuery('ul#sub-menu, #menu ul, ul#footer-menu')
		.sortable({
			placeholder: "ui-state-highlight",
			containment: 'parent',
			items: "li:not(.home,.add)",
			start: function(event, ui) {
				
			},
			update: function(event, ui) {
				var lis = jQuery('li:not(.home,.add)', this);
				var json = parseMenu4Sorting(lis);
				
				jQuery.post("/ll/ajax/admin/tree?command=sorting", json,
				function(data){}, "json");
			}
		
		})
		.disableSelection();
		
	function parseMenu4Sorting(lis)
	{
		var ids = new Array();
		lis.each(function(){
			var rel = jQuery(this).attr('rel').split('|');
			ids.push(rel[0]);
		});
		var json = {
			"ids": ids
		};
				
		return json;
	}	
	

	rebindPencil();
	
	jQuery('fieldset.toggler legend').live('click', function(){
		var els = jQuery(this).parent().find('*').not('legend,.edit-icon,:hidden');
		
		if(els.length) els.slideUp('slow');
		else jQuery(this).parent().find('>*').not('legend,.edit-icon').slideDown('slow');
		
	});



	jQuery('.tabs-list li').live('click',function(){
		

		jQuery('.tab-block-1.'+jQuery(this).attr('class'))
			.css('display','block')
			.siblings().css('display','none');
		
		jQuery(this)
			.addClass('active')
			.siblings().removeClass('active');

		
		
	})





});


function rebindPencil() 
{
	
	// edit controls on administratable items
	jQuery('*[name^=admin_controls]').hover(
		function(){ // mouse over
			
			jQuery(this).css({position : 'relative'});
			
			var url = jQuery(this).attr('name').split(' ');
			var control = url[1] ? url[1] : '';
			var param = url[2] ? url[2] : '';
			
			var s = jQuery('<span>Edit</span>');
			var d = jQuery('<div></div>')
				.click(function(){
					
					var id = isNaN(parseInt(jQuery(this).attr('rel'))) ? 0 : parseInt(jQuery(this).attr('rel'));
					jQuery.ajax('/ll/ajax/admin/' + (control ? control  + '/' : '') + param).success(function(resp)
					{ 
						adminWindow.openW(resp);
						/* BUG: dynamicall load langs*/
						tinyMCE.execCommand("mceAddControl", true, "description_lv");
						tinyMCE.execCommand("mceAddControl", true, "description_ru");
						tinyMCE.execCommand("mceAddControl", true, "description_en");
					});

				})
				.addClass('edit-icon')
				.append(s);
			jQuery(this).prepend(d);
		}

	,
			function(event){ // mouse out
//		console.log(jQuery(event.currentTarget).has('[name^=admin_controls]'));
			jQuery('.edit-icon',this).remove();
		}

	);	

}