function eraSlider(container, left, right, sdvig){	
	
	
	this.container	= container;	
	this.ul			= {};	
	this.li			= {};
	this.left		= left;
	this.right		= right;
	this.sdvig		= sdvig;
	this.slideInterval;
	this.animating = false;
	
	this.init = function()
	{		
		self = this;

		this.ul	= jQuery('ul', this.container);
		this.li = jQuery('li', this.ul);
	


		this.liWidth = this.li.outerWidth(true);
		this.ulWidth = this.liWidth * this.li.length;
		this.parentWidth = this.container.innerWidth();

		

		// binding events
		var self = this;

		this.left.click(function(){
			
			if(self.animating) return;
			clearInterval(self.slideInterval);
			self.animateLeft();
			
		});

		this.right.click(function(){

			if(self.animating) return;
			clearInterval(self.slideInterval);
			self.animateRight();
			
		});

		var self = this;
		
		function startSlide() {
			self.animateRight();
		}
		//this.slideInterval = setInterval(startSlide, 4000);
		
		if(this.sdvig){
			self.ul.css('margin-left' , this.sdvig);

		}

		this.refreshArrow();
	}

	this.animateLeft = function()
	{
		var self = this;
		self.animating = true;
		this.ul.animate({
			'margin-left' : '+=' + this.liWidth + 'px'
		},function(){
			self.refreshArrow();
			self.animating = false;
		});
		
	}

	this.animateRight = function()
	{
		var self = this;
		self.animating = true;
		this.ul.animate({
			'margin-left' : '-=' + this.liWidth + 'px'
		},function(){
			self.refreshArrow();
			self.animating = false;
		});

	}

	this.refreshArrow = function()
	{
		if(this.ulWidth <= this.parentWidth)
		{
			this.left.css('display','none');
			this.right.css('display','none');
			clearInterval(this.slideInterval);
		}

		
		var left = this.ul.css('margin-left');


		if(parseInt(left) >= 0) 
		{
			this.left.css('display','none');
		}
		else{
			this.left.css('display','block');
		}


		if(this.ulWidth-Math.abs(parseInt(left)) - this.liWidth/2  <= this.parentWidth)
		{
			
			this.right.css('display','none');
			clearInterval(this.slideInterval);
		}
		else{
			this.right.css('display','block');
		}
	}



	this.init();
}


