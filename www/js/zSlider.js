 (function( jQuery ){

  jQuery.fn.zSliderBig = function( method ) {

   jQuery(this).each(function() {
    
    var config = {
     options : {
      timeStep : 10000,
      timeAnimate : 2000
     },
     
     init : function( options ) {
      
      jQuery.extend(config.options, options);
      config.slider = jQuery(this);
      config.slides = config.slider.find('.slider-block');
      config.slides.hide().first().show();
      config.intervalId = setTimeout(function () { 
       config.switch2Slide('next');
      }, config.options.timeStep);
      config.buildNavigation();

     },
     destroy : function( )
     {
      return this.each(function(){
       jQuery(window).unbind('.zSliderBig');
      });
     },
     buildNavigation : function( )
     {
      config.navigation = jQuery('<ul class="slider-nav"></ul>');
      for(i=0; i<config.slides.length; i++)
      {
       element = jQuery('<li' + (!i ? ' class="active"' : '') + '></li>').bind('click.zSliderBig', function(){
        clearTimeout(config.intervalId);
        config.switch2Slide( jQuery(this).index() );
       });
       config.navigation.append(element);
      }
      config.slider.append(config.navigation);
     },
     
     switch2Slide : function( index ) 
     {
      var lis = config.navigation.find('li');
      
      if(typeof index == 'number')
      {
       var active = index;
       var activeEl = lis.eq(active);
       var nextAction = 'next';
      }
      else
      {
       var active = config.navigation.find('li.active').index();
       var activeEl = index=='next' ? 
        (lis.eq(active+1).length ? lis.eq(active+1) : lis.first()) :
        (lis.eq(active-1).length ? lis.eq(active-1) : lis.last());
       active = activeEl.index();
       var nextAction = index;
      }
      lis.removeClass('active');
      activeEl.addClass('active');
      
      config.slides.each(function(x){

       index2Hide = (typeof index == 'integer') ? config.navigation.find('li.active').index() : (index=='next' ? x-1 : x+1);
       
       if(config.slides.eq(index2Hide).length && (index2Hide)!=active)
       {
        config.slides.eq(index2Hide).fadeOut(config.options.timeAnimate);
       }
       
       if(x==active)
       {
        jQuery(this).fadeIn(config.options.timeAnimate, function(){
         config.intervalId = setTimeout(function () { config.switch2Slide(nextAction); }, config.options.timeStep);
        });
       }
      });
      
     },
    };
   
    if ( config[method] ) 
    {
     return config[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } 
    else if ( typeof method === 'object' || ! method ) 
    {
     return config.init.apply( this, arguments );
    } 
    else 
    {
     $.error( 'Method ' +  method + ' does not exist on jQuery.zSliderBig' );
    }  
   });
  };
  
 })( jQuery );