function formValidatorClass(form, settings){	
	
	this.form		= form;	
	this.settings	= settings;	
	this.errors		= 
	{		
		'length'	:	'Ievadiet vairāk par % simboliem',		
		'equal'		:	'Lauka vērtībai jāsakrīt ar "%"',
		'typeInt'	:	'Jāierakstā skaitli',		
		'email'		:	'Jāierakstā korekto e-pasta adresi',
		'date'		:	'Datumu jāierakstā formātā: 31.12.1999',
		'captcha'	:	'Drošības kods nesakrīt',
		'email_exists'	:	'Ieraksts ar tādu e-pasta adresi jau eksistē datu bazē'
	}		
		
	this.init = function()
	{		
		var owner = this;


		this.form.submit(function(event){

			if(owner.validate()) 
			{
				
				this.submit();
			}
			else
			{
				event.preventDefault();
				return false;
			}
			
		});
	}	
	
	this.clearErrors = function()
	{
		jQuery('input[type="text"],input[type="password"],select,textarea', this.form).not('.point').css({background : '#FFF'});
		jQuery('.validatorHint').remove();	
		
	}	
	
	this.validate = function()
	{
		
		this.clearErrors();
		var error = false;
		

		for(x in this.settings)
		{
			
			
			var item = jQuery('[name=' + x + ']', this.form);
			var v = item.val();
			var o =this.settings[x].split('=');



			switch(o[0])
			{
				case 'length':
					if(!this.validateLength(v, o[1]))
					{
						this.showError(item, this.errors.length.replace('%',o[1]));
						if(!error)
						{
							error = true;
							item.focus();
						}
					}
				break;
				
				case 'type':
				
					switch(o[1])
					{
						case 'formated_date':
							if(v.length!=10)
							{
								this.showError(item, this.errors.date);
								if(!error)
								{
									error = true;
									item.focus();
								}
							}						
						break;
						
						case 'integer':
							if(!(parseInt(v)==v && /^-?\d+$/.test(v+'')))
							{
								this.showError(item, this.errors.typeInt.replace('%'));	
								if(!error)
								{
									error = true;
									item.focus();
								}
							}
						break;
						
						case 'email':
							var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
							if(reg.test(v) == false)
							{
								this.showError(item, this.errors.email);
								if(!error)
								{
									error = true;
									item.focus();
								}
							}
						break;
						
						case 'decimal':
							if(!(parseFloat(v)==v))
							{
								this.showError(item, this.errors.typeInt.replace('%'));
								if(!error)
								{
									error = true;
									item.focus();
								}
							}
						break;
					}
				break;
				
				case 'equal':
					var compareWith = jQuery('[name=' + o[1] + ']', this.form);	

					if(v=='' || v!=compareWith.val())
					{
						this.showError(item, this.errors.equal.replace('%', jQuery(compareWith).parent().find('label').html()));
						if(!error)
						{
							error = true;
							item.focus();
						}
					}
				break;

				case 'email_exists':

					var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
					if(reg.test(v) == false)
					{
						this.showError(item, this.errors.email);
						if(!error)
						{
							error = true;
							item.focus();
						}
					}
					else{
						$.ajax({
						  url: "/ll/ajax/captcha_form_email",
						  data: "email="+v,
						  async: false,
						  complete: function(data){
						
							if(data.responseText)
							{
								
								
								item = jQuery('input[name=email]',ajax_captcha_form.form);

								ajax_captcha_form.showError(item, ajax_captcha_form.errors.email_exists);
								if(!error)
								{
									error = true;
									item.focus();
								}
							}
							
						  }
						});
					}


				break;

				case 'captcha':

					$.ajax({
					  url: "/ll/ajax/captcha_form_code",
					  data: "code="+v,
					  async: false,
					  complete: function(data){
						
						if(!data.responseText)
						{
							
							item = jQuery('input[name=captcha]', ajax_captcha_form.form);
							
							
							ajax_captcha_form.showError(item, ajax_captcha_form.errors.captcha);
							if(!error)
							{
								error = true;
								item.focus();
							}
						}
						
					  }
					});


				break;

				case 'comment_captcha':

					$.ajax({
					  url: "/ll/ajax/captcha_comment_form_code",
					  data: "code="+v,
					  async: false,
					  complete: function(data){
						
						if(!data.responseText)
						{
							
							item = jQuery('input[name=captcha]', ajax_captcha_form.form);
							
							ajax_captcha_form.showError(item, ajax_captcha_form.errors.captcha);
							if(!error)
							{
								error = true;
								item.focus();
							}
						}
						jQuery('#siimage').attr('src','/library/securimage/securimage_show.php?sid=' + Math.random());
						
					  }
					});


				break;

			}
		}
		
		jQuery('input.errors', this.form).blur(function()
		{
			//jQuery('.validatorHint[rel=' + this.name + ']').remove();
			jQuery('input[type="text"],input[type="password"],select,textarea', this.form).not('.point').css({background : '#FFF'});
			jQuery('.validatorHint').remove();
		});



		if(!error)
		{
			return true;
		}
		return false;
	}
	
	this.showError = function(item, err)
	{
		if(!item) return;
		if(!item.offset()) return;

		var l = item.offset().left + item.outerWidth(true);
		
		var hint = jQuery('<div></div>')
		.css({cursor : 'pointer',opacity : 0,left : parseInt(l + 200) + 'px',top : item.offset().top + 'px', 'z-index':1100}).text(err).addClass('validatorHint').click(function()
		{ 
			jQuery(this).remove();
		}).attr({'rel' : item.attr('name')});
		
		jQuery('body').prepend(hint);
		
		hint.animate({left : parseInt(l-10) + 'px',opacity : 1}, 300, function()
		{
			jQuery(this).animate({left : parseInt(l+5) + 'px'}, 200)
		});
		
		item.addClass('errors').css({background : '#ECB'});
	}
	
	this.validateLength = function(val, len)
	{
		if(val)
		{
			if(val.length>=len) return true;
		}
		
		return false;
	}

	this.init();
}