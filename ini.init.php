<?php

    session_start();

    define('_HOST', 'http://'. $_SERVER['HTTP_HOST'] .'/');
    define('_ROOT', $_SERVER['DOCUMENT_ROOT'] .'/');
    define('_APP', 'app');
    define('_SITE', '/');
    define('_DEV', $_SERVER['REMOTE_ADDR']=='127.0.0.1');

    define('UNDEFINED', 0x00);

    require_once('app/m/Loader.php');

    if(_DEV) {
        error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING ^ E_ERROR ^ E_RECOVERABLE_ERROR ^ E_STRICT);
        ini_set('display_errors', 'on');
    }
