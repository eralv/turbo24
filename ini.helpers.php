<?php



function mres($v)

{

    return mysql_real_escape_string($v);



}



function hsc($v)

{

    return htmlspecialchars($v);

}



function trim_array($a)

{

    if (!is_array($a)) return array();

    foreach ($a as $k => $v) {

        if ($v === '') unset($a[$k]);

    }

    return $a;

}



function redirect($url = false)

{



    $ref = $_SERVER['HTTP_REFERER'] ? $_SERVER['HTTP_REFERER'] : '/';

    header('Location: ' . (!$url ? $ref : $url));

    exit;

}



function asObj(array $data, $class = null)

{



    $obj = new stdClass;

    if ($class !== null) {

        if (is_object($class)) {

            $obj = $class;

        } elseif (is_string($class) && class_exists($class)) {

            try {

                $obj = new $class;

            } catch (Exception $e) {

                throw $e;

            }

        }

    }



    foreach ($data as $key => $val) {

        if (!is_numeric($key)) {

            $obj->{$key} = $val;

        }

    }



    return $obj;



}



function array_swap_vk(array $modify)

{

    $array = array();

    foreach ($modify as $val => $key) {

        if (is_string($key)) {

            $array[$key] = is_numeric($val) ? '' : $val;

        }

    }

    return $array;

}



function setIf_isset(&$setThis, &$ifThisIsset, $else = null)

{

    if (isset($ifThisIsset)) {

        $setThis = $ifThisIsset;

        return true;

    } else {

        $setThis = $else;

    }

    return false;

}



/**

 * Convert datetime to user friendly format

 */

function dateConvert($date)

{



    if ($date == '0000-00-00') return '';



    $d = array();

    if (strlen($date) == 19) {

        $d[] =

            substr($date, 8, 2) . "." .

            substr($date, 5, 2) . "." .

            substr($date, 0, 4) . " ";

        $d[] =

            substr($date, 11, 2) . ":" .

            substr($date, 14, 2);



    } elseif (strlen($date) == 10) {

        $d[] =

            substr($date, 8, 2) . "." .

            substr($date, 5, 2) . "." .

            substr($date, 0, 4);

    } else {

        $d[] = $date;

    }



    return count($d) == 1 ? implode("", $d) : $d;



}



function standartDate($d)

{

    if ($d == '0000-00-00') return '';

    $d = explode('.', $d);

    $d = array_reverse($d);

    return mres(implode('-', $d));

}



function changeURL($cgi = array())

{

    $url = '';



    $cgi = array_merge($_GET, $cgi);





    $replaces = array('1letter', 'page');

    foreach ($replaces as $r) {

        if (isset($cgi[$r])) {

            $tmp = is_array($cgi[$r]) ? $cgi[$r][1] : $cgi[$r];

            unset($cgi[$r]);

            $cgi[$r] = $tmp;

        }

    }





    if (count($cgi)) {

        $url = array();

        foreach ($cgi as $k => $v)

            if (!is_array($v)) {

                $url[] = $k . '=' . $v;

            } else {



                foreach ($v as $sub_k => $sub_v) {



                    $url[] = $k . '[' . $sub_k . ']' . '=' . (is_array($sub_v) ? end($sub_v) : $sub_v);



                }

            }

        $url = '?' . implode('&', $url);

    }





    return htmlspecialchars('/' . $url);

}



function addQuotes($arr)

{

    if (is_array($arr)) {

        foreach ($arr as $k => $a) {

            $arr[$k] = '"' . $a . '"';

        }

    } else {

        $arr = array();

    }

    return $arr;

}





function dateConvertStr($d)

{

    $year = substr($d, 0, 4);

    $day = substr($d, -2);

    $month_cipars = substr($d, 5, 2);

    $month_cipars = (int)$month_cipars;

    $m = array(

        1 => 'janvārī',

        2 => 'februārī',

        3 => 'martā',

        4 => 'aprīli',

        5 => 'maijā',

        6 => 'jūnijā',

        7 => 'jūlijā',

        8 => 'augustā',

        9 => 'septembrī',

        10 => 'oktobrī',

        11 => 'novembrī',

        12 => 'decembrī'

    );

    $month = $m[$month_cipars];

    $str = $year . '.gada ' . $day . '. ' . $month;



    return $str;

}



function getMonth($m)

{

    $a = array(

        1 => 'janvāris',

        2 => 'februāris',

        3 => 'marts',

        4 => 'aprīlis',

        5 => 'maijs',

        6 => 'jūnijs',

        7 => 'jūlijs',

        8 => 'augusts',

        9 => 'septembris',

        10 => 'oktobris',

        11 => 'novembris',

        12 => 'decembris'

    );

    return $a[(int)$m];

}



function str_replace_deep($search, $replace, $subject)

{

    if (is_array($subject)) {

        foreach ($subject as &$oneSubject)

            $oneSubject = str_replace_deep($search, $replace, $oneSubject);

        unset($oneSubject);

        return $subject;

    } else {

        return str_replace($search, $replace, $subject);

    }

}





/**

 * Returns file mime-type, extension, basename, name, size

 *

 * @param string $file

 * @return Array

 */

function fileInfo($file)

{



    if ($_SERVER['REMOTE_ADDR'] == '193.68.74.82' && false) {

        error_reporting(E_ALL);

        ini_set('display_errors', 'on');

        $finfo = new finfo(FILEINFO_MIME_TYPE, $file);

        if (!$finfo) echo 'false';

        echo $finfo->file($file);

        die();

    }



    if (function_exists('finfo_open')) {

        $finfo = finfo_open(FILEINFO_MIME_TYPE);

        $type = finfo_file($finfo, $file);

        finfo_close($finfo);

    } else {

        $fileSHL = escapeshellarg($file);

        $type = `file -i -b $fileSHL`;

        $type = reset(explode(';', $type));

    }



    $extension = pathinfo($file, PATHINFO_EXTENSION);

    $extension = strtolower($extension);



    $name = pathinfo($file, PATHINFO_FILENAME);

    $baseName = pathinfo($file, PATHINFO_BASENAME);



    $fileSize = filesize($file);



    return Array(trim($type), $extension, $baseName, $name, $fileSize);

}





function pp()

{

    $args = func_get_args();

    echo '<pre>';

    foreach ($args as $arg) {

        var_dump($arg);

    }

    echo '<pre>';

}



function multi_array(&$array) {



    $new = array();



    foreach($array as $key=>$elem) {

        if(is_array($elem)) {

            foreach($elem as $x=>$val) {

                if(is_array($val)) {



                } else {

                    $new[$x][$key] = $val;

                }

            }

        }

    }



    $array = $new;



}





/*

*	R E S I Z E R

*/

function makeIcons_MergeCenter($src, $dst, $dstx, $dsty)

{



    //$src = original image location

    //$dst = destination image location

    //$dstx = user defined width of image

    //$dsty = user defined height of image



    $allowedExtensions = 'jpg jpeg gif png JPG JPEG GIF PNG';



    $name = explode(".", $src);

    $currentExtensions = $name[count($name) - 1];

    $extensions = explode(" ", $allowedExtensions);



    for ($i = 0; count($extensions) > $i; $i = $i + 1) {

        if ($extensions[$i] == $currentExtensions) {

            $extensionOK = 1;

            $fileExtension = $extensions[$i];

            break;

        }

    }



    if ($extensionOK) {



        $size = getImageSize($src);

        $width = $size[0];

        $height = $size[1];



        if ($width >= $dstx AND $height >= $dsty) {



            $proportion_X = $width / $dstx;

            $proportion_Y = $height / $dsty;



            if ($proportion_X > $proportion_Y) {

                $proportion = $proportion_X;

            } else {

                $proportion = $proportion_Y;

            }



            $target['width'] = $dstx * $proportion;

            $target['height'] = $dsty * $proportion;



            $original['diagonal_center'] =

                round(sqrt(($width * $width) + ($height * $height)) / 2);



            $target['diagonal_center'] =

                round(sqrt(($target['width'] * $target['width']) +

                        ($target['height'] * $target['height'])) / 2);



            $crop = round($original['diagonal_center'] - $target['diagonal_center']);



            if ($proportion_X < $proportion_Y) {

                $target['x'] = 0;

                $target['y'] = round((($height / 2) * $crop) / $target['diagonal_center']);

            } else {

                $target['x'] = round((($width / 2) * $crop) / $target['diagonal_center']);

                $target['y'] = 0;

            }





            if ($fileExtension == "jpg" OR $fileExtension == 'jpeg' OR $fileExtension == 'JPG' OR $fileExtension == 'JPEG') {

                $from = ImageCreateFromJpeg($src);

            } elseif ($fileExtension == "gif") {

                $from = ImageCreateFromGIF($src);

            } elseif ($fileExtension == 'png') {

                $from = imageCreateFromPNG($src);

            } else {

                $from = imageCreateFromJpeg($src);

            }



            $new = ImageCreateTrueColor($width / $proportion, $height / $proportion);



            imagecopyresampled(

                $new,

                $from,

                0,

                0,

                0,

                0,

                $width / $proportion,

                $height / $proportion,

                $width,

                $height

            );



            # 	$width / $proportion." ".$height / $proportion." ".$width." ".$height;



            if ($fileExtension == "jpg" OR $fileExtension == 'jpeg' OR $fileExtension == 'JPG' OR $fileExtension == 'JPEG') {

                imagejpeg($new, $dst, 90);

            } elseif ($fileExtension == "gif") {

                imagegif($new, $dst);

            } elseif ($fileExtension == 'png') {

                imagepng($new, $dst);

            } else {

                imagejpeg($new, $dst, 90);

            }

        }

    }

}



function makeIcons_MergeCenter_crop($src, $dst, $dstx, $dsty)

{

    /**

     * @var $src = original image location

     * @var $dst = destination image location

     * @var $dstx = user defined width of image

     * @var $dsty = user defined height of image

     */





    $allowedExtensions = 'jpg jpeg gif png JPG JPEG GIF PNG';



    $name = explode(".", $src);

    $currentExtensions = $name[count($name) - 1];

    $extensions = explode(" ", $allowedExtensions);



    for ($i = 0; count($extensions) > $i; $i = $i + 1) {

        if ($extensions[$i] == $currentExtensions) {

            $extensionOK = 1;

            $fileExtension = $extensions[$i];

            break;

        }

    }



    if ($extensionOK) {



        $size = getImageSize($src);

        $width = $size[0];

        $height = $size[1];



        if ($width >= $dstx AND $height >= $dsty) {



            $proportion_X = $width / $dstx;

            $proportion_Y = $height / $dsty;



            if ($proportion_X > $proportion_Y) {

                $proportion = $proportion_Y;

            } else {

                $proportion = $proportion_X;

            }

            $target['width'] = $dstx * $proportion;

            $target['height'] = $dsty * $proportion;



            $original['diagonal_center'] =

                round(sqrt(($width * $width) + ($height * $height)) / 2);

            $target['diagonal_center'] =

                round(sqrt(($target['width'] * $target['width']) +

                        ($target['height'] * $target['height'])) / 2);



            $crop = round($original['diagonal_center'] - $target['diagonal_center']);



            if ($proportion_X < $proportion_Y) {

                $target['x'] = 0;

                $target['y'] = round((($height / 2) * $crop) / $target['diagonal_center']);

            } else {

                $target['x'] = round((($width / 2) * $crop) / $target['diagonal_center']);

                $target['y'] = 0;

            }



            if ($fileExtension == "jpg" OR $fileExtension == 'jpeg' OR $fileExtension == 'JPG' OR $fileExtension == 'JPEG') {



                $from = ImageCreateFromJpeg($src);

            } elseif ($fileExtension == "gif") {

                $from = ImageCreateFromGIF($src);

            } elseif ($fileExtension == 'png') {

                $from = imageCreateFromPNG($src);

            } else {

                $from = imageCreateFromJpeg($src);

            }



            $new = ImageCreateTrueColor($dstx, $dsty);



            imagecopyresampled($new, $from, 0, 0, $target['x'],

                $target['y'], $dstx, $dsty, $target['width'], $target['height']);



            if ($fileExtension == "jpg" OR $fileExtension == 'jpeg' OR $fileExtension == 'JPG' OR $fileExtension == 'JPEG') {



                imagejpeg($new, $dst, 90);

            } elseif ($fileExtension == "gif" OR $fileExtension == "GIF") {

                imagegif($new, $dst);

            } elseif ($fileExtension == "png" OR $fileExtension == "PNG") {



                imagepng($new, $dst);

            } else {

                imagejpeg($new, $dst, 90);

            }

        }

    }

}

function isMuch($int) {
    return !(bool)preg_match('/^(\d*?[^1]1|1)$/', (int)$int);
}

function amountToString($amount = 0.00, $major = array('lats','lati','latu'), $minor = array('santīms','santīmi')) {
    list($singleMajorStr, $multipleMajorStr, $muchMajorStr) = $major;
    list($singleMinorStr, $multipleMinorStr) = $minor;

    list($majorAmount, $minorAmount) = explode('|',number_format($amount, 2, '|', ''), 2);

    $majorAmountStr = '';
    $veryMuch = false; // Ja ir apaļi simti, tūkstoši, utt. tiek izmantots $muchMajorStr -> divi simti latu, pieci tūkstoši latu

    $words = array(
        1    => array('nulle', 'viens', 'divi', 'trīs', 'četri', 'pieci', 'seši', 'septiņi', 'astoņi', 'deviņi'),
        10   => array(1=>'desmit', 'divdesmit', 'trīsdesmit', 'četridesmit', 'piecdesmit', 'sešdesmit', 'septiņdesmit', 'astoņdesmit', 'deviņdesmit'),
        11   => array(11=>'vienpadsmit', 'divpadsmit', 'trīspadsmit', 'četrpadsmit', 'piecpadsmit', 'sešpadsmit', 'septiņpadsmit', 'astoņpadsmit', 'deviņpadsmit'),
        100  => array('simts', 'simti'),
        1000 => array('tūkstotis', 'tūkstoši')
    );

    if($majorAmount < 10) { // Vieni

        $majorAmountStr = $words[1][$majorAmount];
        var_dump(true);

    } elseif(($onlyTens = !($majorAmount % 10) && 100 > $majorAmount) || ($majorAmount >= 20 && 100 > $majorAmount)) { // Desmiti

        $majorAmountStr = $words[10][(int)($majorAmount/10)];
        if(!isset($onlyTens) || !$onlyTens) {
            $majorAmountStr .= ' '. $words[1][$majorAmount % 10];
        }
        var_dump($majorAmountStr);

    } elseif($majorAmount > 10 && 20 > $majorAmount) { // Padsmiti

        $majorAmountStr = $words[11][$majorAmount];
        var_dump(true);

    } elseif($majorAmount >= 100) { // Simti

        if((!($majorAmount % 100) && 1000 > $majorAmount)) {
            if(($majorAmount/100)===1) {
                $majorAmountStr = $words[100][0];
            } else {
                $majorAmountStr = $words[1][(int)($majorAmount/100)] .' '. $words[100][1];
                $veryMuch = true;
            }
        }
        var_dump(true);

    }

    return $amount .' => '. $majorAmountStr .' '. (isMuch($majorAmount)?($veryMuch?$muchMajorStr:$multipleMajorStr):$singleMajorStr) .', '. $minorAmount .' '. (isMuch($minorAmount)?$multipleMinorStr:$singleMinorStr);
}


function amountToStrint($amount, $major = 'EUR', $minor = 'centi')

{



    //die(strlen)$amou_);



    $tisja4i = array("tūkstotis", "tūkstoši", "tūkstošu");

    $sotni = array("simts", "simti");

    $digs1 = array("", "viens", "divi", "trīs", "četri", "pieci", "seši", "septiņi", "astoņi", "deviņi");

    $desjatki = array("", "", "divdesmit", "trīsdesmit", "četrdesmit", "piecdesmit", "sešdesmit", "septiņdesmit", "astoņdesmit", "deviņdesmit");

    $digs2 = array("desmit", "vienpadsmit", "divpadsmit", "trīspadsmit", "četrpadsmit", "piecpadsmit", "sešpadsmit", "septiņpadsmit", "astoņpadsmit", "deviņpadsmit");



    $digs_str = "";



    $amountArr = explode(',', $amount);



    $amount = str_replace(' ', '', (string)$amountArr[0]);





    if (strlen($amount) == 5) {

        if ($amount[0] == 1) {

            $digs_str .= " " . $digs2[$amount[1]] . " " . $tisja4i[1];

        } else {

            $digs_str .= " " . $desjatki[$amount[0]] . " " . ($amount[1] != 0 ? $digs[$amount[1]] : "") . " " . ($amount[1] == 0 ? $tisja4i[1] : ($amount[1] == 1 ? $tisja4i[0] : $tisja4i[1]));

        }

    }



    if (strlen($amount) == 4) {

        $digs_str .= " " . $digs1[$amount[0]] . " " . ($amount[0] == 1 ? $tisja4i[0] : $tisja4i[1]);

    }





    if (strlen($amount) == 3) {

        $digs_str .= " " . $digs1[$amount[0]] . " " . ($amount[0] == 1 ? $sotni[0] : $sotni[1]);

        if ($amount[1] == 1) {

            $digs_str .= " " . $digs2[$amount[2]];

        } else {

            $digs_str .= " " . $desjatki[$amount[1]] . " " . $digs1[$amount[2]];

        }

    }





    if (strlen($amount) == 2) {

        if ($amount[0] == 1) {

            $digs_str .= " " . $digs2[$amount[1]];

        } else {

            $digs_str .= " " . $desjatki[$amount[0]] . " " . $digs1[$amount[1]];

        }

    }





    if (strlen($amount) == 1) {

        $digs_str .= " " . $digs1[$amount[0]];

    }



    $digs_str .= ' '. $major .' '. $amountArr[1] .' '. $minor;





    return $digs_str;



}





function sendMail($emailArr, $subject, $body, $attachment = '')

{





    require_once(_PHPMAILER . 'class.phpmailer.php');

    $mail = new PHPMailer();



    /*

    $mail->IsSMTP();

    $mail->SMTPAuth = true;

    $mail->Username = "smtp@europrojects.org";

    $mail->Password = "dldn01";

    $mail->SMTP_PORT = 25;

    $mail->Host       = "mail.era.lv"; // SMTP server

    */



    $mail->From = ADMIN_MAIL;

    $mail->FromName = ADMIN_NAME;



    $mail->MsgHTML($body);





    $mail->Subject = $subject;

    $mail->Body = $body;

    $mail->AltBody = strip_tags($body);





    foreach ($emailArr as $_email) {

        $mail->AddAddress($_email);

    }



    if ($attachment)

        $mail->AddAttachment($attachment); // attachment



    $mail->Send();





    if ($mail->ErrorInfo) {

        echo '<h3>' . $mail->ErrorInfo . '</h3>';

        return false;

    } else {

        return true;

    }



}





function getExtension($str)

{

    $i = strrpos($str, ".");

    if (!$i) {

        return "";

    }

    $l = strlen($str) - $i;

    $ext = substr($str, $i + 1, $l);

    return $ext;

}





function recursiveDelete($str)

{



    if (is_file($str)) {

        return @unlink($str);

    } elseif (is_dir($str)) {



        $scan = glob(rtrim($str, '/') . '/*');

        foreach ($scan as $index => $path) {

            recursiveDelete($path);

        }

        return @rmdir($str);

    }

}





function sluggify($url)

{



    # Remove quotes (can't, etc.)

    $url = str_replace('\'', '', $url);

    $replaces = array(

        'ķ' => 'k', 'Ķ' => 'k', 'ā' => 'a', 'Ā' => 'a',

        'ļ' => 'l', 'Ļ' => 'l', 'č' => 'c', 'Č' => 'c',

        'ņ' => 'n', 'Ņ' => 'n', 'ē' => 'e', 'Ē' => 'e',

        'Ģ' => 'g', 'ģ' => 'g', 'Š' => 's', 'š' => 's',

        'ī' => 'i', 'Ī' => 'i', 'ū' => 'u', 'Ū' => 'u',

        'ž' => 'z', 'Ž' => 'z', 'а' => 'a', 'А' => 'A',

        'б' => 'b', 'Б' => 'B', 'в' => 'v', 'В' => 'V',

        'г' => 'g', 'Г' => 'G', 'д' => 'd', 'Д' => 'D',

        'е' => 'e', 'Е' => 'E', 'ё' => 'jo', 'Ё' => 'Jo',

        'ж' => 'zh', 'Ж' => 'Zh', 'з' => 'z', 'З' => 'Z',

        'и' => 'i', 'И' => 'I', 'й' => 'j', 'Й' => 'J',

        'к' => 'k', 'К' => 'K', 'л' => 'l', 'Л' => 'L',

        'м' => 'm', 'М' => 'M', 'н' => 'n', 'Н' => 'N',

        'о' => 'o', 'О' => 'O', 'п' => 'p', 'П' => 'P',

        'р' => 'r', 'Р' => 'R', 'с' => 's', 'С' => 'S',

        'т' => 't', 'Т' => 'T', 'у' => 'u', 'У' => 'U',

        'ф' => 'f', 'Ф' => 'F', 'х' => 'h', 'Х' => 'H',

        'ц' => 'c', 'Ц' => 'C', 'ч' => 'ch', 'Ч' => 'Ch',

        'ш' => 'sh', 'Ш' => 'Sh', 'щ' => 'shj', 'Щ' => 'Shj',

        'ы' => 'i', 'Ы' => 'I', 'э' => 'e', 'Э' => 'E',

        'ю' => 'ju', 'Ю' => 'Ju', 'я' => 'ja', 'Я' => 'Ja'



    );

    foreach ($replaces as $k => $v) {

        $url = mb_eregi_replace($k, $v, $url);

    }



    # Prep string with some basic normalization

    $url = html_entity_decode(stripslashes(strip_tags(strtolower($url))));



    # Replace non-alpha numeric with hyphens

    $url = trim(preg_replace('/[^a-z0-9]+/', '-', $url), '-');



    return $url;

}



function isEmail($email)

{

    if (preg_match("/^(\w+((-\w+)|(\w.\w+))*)\@(\w+((\.|-)\w+)*\.\w+$)/", $email)) {

        return true;

    } else {

        return false;

    }

}



function file_array(&$files)

{



    $array = array();

    if (setIf_isset($pointer, $files['name']) && isset($pointer[0])) {

        foreach ($pointer as $i => $name) {

            $array[] = array(

                'name' => $files['name'][$i],

                'tmp_name' => $files['tmp_name'][$i],

                'error' => $files['error'][$i],

                'size' => $files['size'][$i],

                'type' => $files['type'][$i],

            );

        }

    }



    $files = $array;



}

function validate_value($key, $value = '')
{
  return isset($_POST[$key]) ? $_POST[$key] : $value;
}

function changeFileName($newFilename, $oldOne)

{



    return preg_replace("/^([^\.]+)/i", $newFilename, $oldOne);



}