<?php

	date_default_timezone_set('Europe/Riga');
	
	set_include_path(
		'.' . PATH_SEPARATOR . './library'
		. PATH_SEPARATOR . './app/m/'
		. PATH_SEPARATOR . get_include_path()
	);
	
	include 'ini.init.php';
	include 'ini.helpers.php';

	$controller = Engine_Controller::getInstance();

	$controller->dispatch();