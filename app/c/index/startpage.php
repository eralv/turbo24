<?php
$orders = new CMS_Orders();
$rates = new CMS_Rates();
$routes = new CMS_Routes();
$slider = new CMS_Slider();
$news = new CMS_News();

$payment = new CMS_CreditCards($this->get('users')->getId(),2 /* DEV env */);

$user = $this->get('users')->gi();

$rate_value = $rates->getRate($user);

$route_list = $routes->getList(array('user_id'=>$this->get('users')->getId()));

$limit = array(
    'start_page'=> 0,
    'per_page'	=> 1
);
$order = array(
    'order_by'=> 'id',
    'sort' => 'DESC'
    );
$news_list = $news->getList(NULL, null, $limit, $order);
if(!empty($_POST) && isset($_POST['ajax_order'])) {
    header('Content-Type: application/json; charset=utf-8');

    $success = false;
    $error = array();
    $url = null;

    $orders->validate = true;
        if($oid = $orders->saveItem($_POST)) {

            if(isset($_POST['save_route']) && $this->get('users')->is()) {
		        $routes->saveItem($_POST);
            }
            if($item = $orders->getItem($oid)) {
                //$payment->startDMSAuth($orders->calcPrice($item['distance']),$oid,'',false);
                if(/*$payment->url*/ true) {
                    $success = true;
                    $url = '/ll/order_callback?oid='.$oid/*$payment->url*/;
                } else {
                    $error = null;
                }
            } else {
                $error = null;
            }

        } else {
            $error += $orders->saveErrors;
        }
    echo json_encode(array('success'=>$success, 'error'=>$error, 'payment_url'=>$url, 'phrase'=>array(
        'pay'=>$this->get('langs')->text('site','orders:go_to_payment'),
        'pre'=>$this->get('langs')->text('site','orders:go_to_payment-pre_text'),
        'post'=>$this->get('langs')->text('site','orders:go_to_payment-post_text')
    )));
    exit;

} elseif(isset($_POST['ajax_routes'])){
    header('Content-Type: application/json; charset=utf-8');
    if(setIf_isset($route, $route_list[$_POST['ajax_routes']])){
        echo json_encode($route);
    } else echo 'Error!';
    exit;
}


$view = 'front/startpage/textpage';
$params = array(
    'user' => $this->get('users')->gi(),
    'route_list' => $route_list,
    'news_list' => $news_list,
    'rate_value' => $rate_value,
    'slides' => $slider->getList()
);
return array($view, $params);
