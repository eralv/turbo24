<?php
$news = new CMS_News();
setIf_isset($id, $this->route[1], null);
if($id){

    $item = $news->getItem(array('id'=>$id));

    $view = 'front/news/item';
    $params = array(
        'item' => $item,
    );
} else {

    $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
    $per_page = 5;
    $list = $item = $news->getList(null, null, array('start_page'=>(($page-1)*$per_page), 'per_page'=>$per_page), array('order_by'=>'id','sort'=>'desc'));

    $view = 'front/news/list';
    $params = array(
        'list' => $list,
        'page' => $page,
        'pages' => ceil($news->get_Count() / $per_page)
    );
}
return array($view, $params);
