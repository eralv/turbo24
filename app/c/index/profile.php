<?php
/**
 * @var Engine_Templates $this
 */

if(!$this->get('users')->is()) {
    return array('front/error/403',array());
}
setIf_isset($action, $this->route[1], null);

$orders = new CMS_Orders();
$routes = new CMS_Routes();
switch($action) {
    case 'add_route':
    case 'edit_route':
        $newItem = true;
        if(setIf_isset($route_id,$this->route[2]) && $route = $routes->getItem($route_id)) {
            $newItem = false;
        } else {
            $route = $routes->getBlank(true);
        }

        if(setIf_isset($post, $_POST['route'])) {
            $routes->saveItem($post, $route);
            if($newItem) {
                redirect('/ll/profile');
            } else {
                redirect();
            }
        }

        return array('front/user/add_route', array(
            'item' => $route
        ));

    break;
    case 'delete_route':

        if(setIf_isset($route_id,$this->route[2])) {
            $routes->deleteItem($route_id);
        }
        redirect();

        break;
    case 'edit':
    default:

	 $user_id = $this->get('users')->getId();
        //setIf_isset($user_id,$this->route[3]);
        $user = $this->get('users')->getItem($user_id);

        $route_list = $routes->getList(array(
            'user_id'=>$user_id
        ));
        $order = $orders->getList(array(
            'user_id'=> $user_id,
        ));

        if(!empty($_POST) && isset($_POST['ajax_profile']) && setIf_isset($post, $_POST['user'])) {
            header('Content-type: application/json; charset=utf-8');

            $this->get('users')->validate = true;
            $success = false;
            if($this->get('users')->saveItem($post, $user)) {
                $success = true;
            }
            //redirect();

            echo json_encode(array('success'=>$success, 'error'=>$this->get('users')->saveErrors, 'phrase'=>$this->get('langs')->text('site','profile:save_success')));
            exit;
        }
	
        return array('front/user/edit', array(
            'item' => $user,
            'orders' => $order,
            'routes' => $route_list,
        ));

    break;
}
