<?php

$orders = new CMS_Orders();
if(setIf_isset($id, $this->route[1]) && $order = $orders->getItem($id)) {
    if($this->get('users')->admin() || $this->get('users')->is(Engine_Users::OPERATOR) ||
        ($this->get('users')->is() && $this->get('users')->getId()==$order['user_id'])
    ) {
        $orders->generateBillPdf($id, false, 'I');
    } else {
        echo 'Not available';
    }
} else {
    echo 'Does not exist';
}
exit;