<?php

if(setIf_isset($soc,$this->route[1])) {
    switch($soc) {
        case 'login':

            header('Content-type: application/json; charset=utf-8');

            $success = false;
            $error = array();

            if(isset($_POST['ajax_login']) && setIf_isset($user,$_POST['user']))
            {
                if(!empty($user['username']) && !empty($user['password'])) {
                    if(!$this->get('users')->login($user['username'], $user['password'])) {
                        $error[] = 'username';
                        $error[] = 'password';
                    } else {
                        $success = true;
                    }
                } else {
                    if(empty($user['username'])) $error[] = 'username';
                    if(empty($user['password'])) $error[] = 'password';
                }
            }
            echo json_encode(array('success'=>$success, 'error'=>$error));
            break;
        case 'register':

            header('Content-type: application/json; charset=utf-8');

            $success = false;
            $error = array();

            if(isset($_POST['ajax_reg']) && !empty($_POST['register']) && isset($_POST['register']['confirm']))
            {
                $post = $_POST['register'];
                if(!empty($post['name']) && !empty($post['surname']) && !empty($post['email']) &&
                    preg_match($this->get('users')->validate_fields['email']['valid'], $post['email']) &&
                    (!empty($post['password']) && !empty($post['repeat_password']) &&
                        md5($post['password'])===md5($post['repeat_password'])
                    )
                ) {
                    if($this->get('users')->checkUnique('username', $post['email'])) {
                        if($this->get('users')->saveItem($post)) {
                            $success = true;
                        }
                    } else {
                        $error[] = 'email';
                    }

                } else {
                    if(empty($post['name'])) $error[] = 'name';
                    if(empty($post['surname'])) $error[] = 'surname';
                    if(empty($post['email']) || !preg_match($this->get('users')->validate_fields['email']['valid'], $post['email'])) $error[] = 'email';
                    if(empty($post['password'])) $error[] = 'password';
                    if(empty($post['repeat_password'])) $error[] = 'repeat_password';
                    if(md5($post['password'])!==md5($post['repeat_password'])) {
                        $error[] = 'password';
                        $error[] = 'repeat_password';
                    }
                }

            } else {
                $error[] = 'confirm';
            }

            echo json_encode(array('success'=>$success, 'error'=>$error, 'phrase'=>$this->get('langs')->text('site','register:success')));
            break;
        case 'draugiem': {

            if(isset($_GET['link'])) {
                $dr = new CMS_Draugiem();
                redirect('http://api.draugiem.lv/authorize/?app='.$dr->app_id.'&hash='.md5($dr->app_key . $dr->getUrl()).'&redirect='.$dr->getUrl());
            } else {
                new CMS_Draugiem();
            }

            break;
        }
        case 'twitter': {

            if(isset($_GET['link'])) {

                $tw = new CMS_Twitter();

                $twitter = new TwitterOAuth($tw->consumer_key, $tw->consumer_secret);

                $request_token = $twitter->getRequestToken($tw->getUrl());

                // Saving them into the session
                $_SESSION['oauth_token'] = $request_token['oauth_token'];
                $_SESSION['oauth_token_secret'] = $request_token['oauth_token_secret'];

                if($twitter->http_code==200) {

                    redirect($twitter->getAuthorizeURL($request_token['oauth_token']));

                } else {

                    CMS_SystemMessages::save('error','twitter_oauth_failed');
                    redirect(_HOST);
                }

            } else {
                new CMS_Twitter();
            }

            break;
        }
        case 'facebook': {

            if(isset($_GET['link'])) {
                $fb = new CMS_Facebook();
                redirect('https://www.facebook.com/dialog/oauth?client_id='.$fb->app_id.'&scope=email&redirect_uri='. $fb->getUrl() .'');
            } else {
                new CMS_Facebook();
            }

            break;
        }
    }
}