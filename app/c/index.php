<?php
/**
 * @var Engine_Controller $this
 */
$js = $css = array();

if($this->get('users')->admin() || ($this->get('users')->is(Engine_Users::OPERATOR)))
{
    $css[] = 'admin.css';
    $css[] = 'formValidator.css';
    $js[] = 'admin.js';
    $js[] = 'formvalidator.js';

}

$tree = new CMS_SiteTree($this->route);

$view = 'error/404';
$params = array();

if($this->route && ($tree->treeId || end($this->route)=='index')) {
    if(!empty($tree->treeInfo) && !empty($tree->treeInfo['controller'])) {
        $this->setIndexController($tree->treeInfo['controller'], $tree->treeInfo['action']);
    }

    if(is_file($this->indexController)) {
        list($view,$params) = include($this->indexController);
        if($params===null) $params = array();
    }

   
}

$mainParams = array();
if(isset($params['-main']) && is_array($params['-main'])) {
    $mainParams = $params['-main'];
    unset($params['-main']);
}
// Header
$this->get('tpls')
    ->loadVars(array('tree' => &$tree))
    ->setContent('header','template/header');
//login form
$this->get('tpls')
    ->loadVars(array('tree' => &$tree))
    ->setContent('login_form','template/login_form');

//register form
$this->get('tpls')
    ->loadVars(array('tree' => &$tree))
    ->setContent('register_form','template/register_form');

// User Panel
$this->get('tpls')
    ->loadVars(array('tree' => &$tree))
    ->setContent('userPanel','template/user-panel');

// Content
$this->get('tpls')
    ->loadVars($params + array('tree' => &$tree))
    ->setContent('main',$view);

$this->get('tpls')
    ->loadVars($params + array('tree' => &$tree))
    ->setContent('footer','template/footer');



// Template
$this->get('tpls')
    ->loadVars(array(
        'js'	=> $js,
        'css'	=> $css,
        'tree'  => &$tree,
        'vars'  => $mainParams
    ))
    ->show('template/main');
