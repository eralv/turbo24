<?php
$order = new CMS_Orders();
if(setIf_isset($oid, $_GET['oid']) && $item = $order->getItem($oid)) {

    $this->get('db')->update($order->getTable(), array('status'=>1), 'id='.(int)$oid);
    CMS_SystemMessages::save('success','order_complete',true);

    $mail = new CMS_Mailing();

    $mail->to = $item['sender_email'];

    $mail->subject = 'Rēķins no Turbo24.lv';
    $mail->body = 'Mail body...';

    $mail->attachment[] = $order->generateBillPdf($oid, true, 'FE');

    $mail->sendMail();
}
?><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=utf-8;" />
        <title>Order #<?=$oid;?> Online Payment</title>
        <script type="text/javascript">
            (function(win){
                window.onload = function() {
                    if (window.opener && !window.opener.closed && typeof window.opener.reload == 'function') {
                        window.opener.reload();
                        window.close();
                    }
                };
            })(window);
        </script>
    </head>
    <body></body>
</html><?php
exit;