<?php
/**
 * @var Engine_Templates $this
 */
$redi = false;
if(setIf_isset($phrases, $_POST['phrase'])) {
    foreach($phrases as $lang=>$phs) {
        foreach($phs as $name=>$ph) {
            $this->get('db')->saveText($ph, $name, 0, 'sys_texts', $lang);
        }
    }
    $redi = true;
}
if(setIf_isset($config, $_POST['config'])) {
    foreach($config as $name=>$cfg) {
        $this->get('cfg')->write($name, $cfg);
    }
    $redi = true;
}
if($redi)
    redirect();

if(setIf_isset($cfg, $this->route[3])) {
    return array('back/config/single', array('config'=>$cfg));
} else {
    return array('back/config/edit',array(
        'phrases'=>array(
            'title'=>'field',
            'description'=>'text',
            'keywords'=>'field'
        )
    ));
}