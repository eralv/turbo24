<?php
/**
 * @var Engine_Templates $this
 */

setIf_isset($action, $this->route[2], null);
$couriers = new CMS_Couriers;

switch($action) {
    case 'edit':
	
        setIf_isset($courier_id,$this->route[3]);
        $courier = $couriers->getItem($courier_id);
        if(setIf_isset($post, $_POST['courier'])) {
            $couriers->saveItem($post, $courier_id);
	        redirect();
        }

        return array('back/couriers/edit', array(
            'item' => $courier
        ));

    break;
    case 'add':
        if(setIf_isset($post, $_POST['courier'])) {
            $couriers->saveItem($post);
            redirect();
        }

        return array('back/couriers/add', array(
            'item' => array_swap_vk($couriers->self_fields)
        ));

    break;
    case 'delete':

        setIf_isset($courier_id,$this->route[3]);
        $couriers->deleteItem($courier_id);
        redirect();

        break;
    case 'list':
    default:

        $couriers = $couriers->getList();
	
        return array('back/couriers/list', array(
            'list' => $couriers
        ));

    break;
}