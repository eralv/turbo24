<?php
/**
 * @var Engine_Templates $this
 */

setIf_isset($action, $this->route[2], null);
$rates = new CMS_Rates();

switch($action) {
    case 'edit':

        setIf_isset($rate_id,$this->route[3]);
        $rate = $rates->getItem($rate_id);
        if(setIf_isset($post, $_POST['rate'])) {
	    $post['value'] = (float)$post['value'];
            $rates->saveItem($post, $user);
        }

        return array('back/rates/edit', array(
            'item' => $rate
        ));

    break;
    case 'add':
        if(setIf_isset($post, $_POST['rate'])) {
	    $post['value'] = (float)$post['value'];
            $rates->saveItem($post);
            redirect();
        }

        return array('back/rates/add', array(
            'item' => array_swap_vk($rates->self_fields)
        ));

    break;
    case 'delete':
	
        setIf_isset($rate_id,$this->route[3]);
        $rates->deleteItem($rate_id);
        redirect();

        break;
    case 'list':
    default:

        $filter = isset($_POST['search']) ? $_POST['search'] : array();
        $rates = $rates->getList($filter);
	
        return array('back/rates/list', array(
            'list' => $rates
        ));

    break;
}