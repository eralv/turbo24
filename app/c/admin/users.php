<?php
/**
 * @var Engine_Templates $this
 */

setIf_isset($action, $this->route[2], null);
$rates = new CMS_Rates;
$orders = new CMS_Orders;

switch($action) {
    case 'add':
    case 'edit':
        $newItem = true;
        if(setIf_isset($user_id,$this->route[3]) && $user = $this->get('users')->getItem($user_id)) {
            $newItem = false;
            $order = $orders->getList(array(
                'user_id'=> $user_id,
            ));
        } else {
            $user = array_swap_vk($this->get('users')->self_fields);
            $order = array();
        }

        if(setIf_isset($post, $_POST['user'])) {
            $this->get('users')->saveItem($post, $user);
            if($newItem) {
                redirect(_HOST .'/ll/admin/users');
            } else {
                redirect();
            }
        }

        return array('front/user/edit', array(
            'item' => $user,
            'orders' => $order,
            'admin' => true
        ));

    break;
    case 'delete':

        setIf_isset($user_id,$this->route[3]);
        $this->get('users')->deleteItem($user_id);
        redirect();

        break;
    case 'list':
    default:
	
        if(setIf_isset($post, $_POST['status'])) {

            $post['status'] = (int) $post['status'];
            $post['id'] = (int) $post['id'];
	    
            $this->get('users')->saveItem($post, $post);
            redirect();
        }

        if(setIf_isset($post, $_POST['rate'])) {

            $post['rate'] = (int) $post['rate'];
            $post['id'] = (int) $post['id'];
	    
            $this->get('users')->saveItem($post, $post);
            redirect();
        }

        $per_page = 20;
        isset($_GET['page'])?$cur_page = (int)$_GET['page']:$cur_page = 0;
        $start = $cur_page>0?($cur_page - 1) * $per_page:0;

        $filters = array();
        if(setIf_isset($post, $_POST['search'])) {
            $filters = array(
                'name'  =>	(isset($post['name'])?$post['name']:NULL),
                'surname'  =>   (isset($post['surname'])?$post['surname']:NULL),
                'company_name'  =>   (isset($post['company'])?$post['company']:NULL),
                'juridic'  =>   (isset($post['juridic'])&&$post['juridic']>=0?$post['juridic']:NULL),
                'rate'  =>   (isset($post['rate'])&&$post['rate']>=0?$post['rate']:NULL),
            );
            $users = $this->get('users')->getList($filters,null,array('order'=>'id','sort'=>'desc','limit'=>$start.','.$per_page));
        } else {
            $users = $this->get('users')->getList($filters,null,array('order'=>'id','sort'=>'desc','limit'=>$start.','.$per_page));
        }

        $num_pages = ceil($this->get('users')->countItems($filters) / $per_page);
        foreach ($users as $key=>$user){
            $users[$key]['order_count'] = $orders->get_Count(array(
                'user_id'=>$user['id']
            ));
        }

	    $rate_list = $rates->getList();
        return array('back/users/list', array(
            'list'=>$users,
	        'rate_list'=>$rate_list,
            'num_pages' => $num_pages,
            'cur_page' => $cur_page,
        ));

    break;
}