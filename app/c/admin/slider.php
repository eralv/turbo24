<?php
$slider = new CMS_Slider();

setIf_isset($action, $this->route[2], null);
switch($action) {
    case 'sort':
        if(setIf_isset($order, $_POST['order']) && !empty($order)) {
            foreach($order as $i=>$id) {
                $slider->saveItem(array('sort'=>$i), $id);
            }
            exit;
        }
        break;

    case 'add':
    case 'edit':

        $isNew = true;
        if(setIf_isset($id, $this->route[3]) && $slide = $slider->getItem($id)) {
            $isNew = false;
        } else {

            $slide = $slider->getBlank();
        }

        $upl = new CMS_Uploads();
        if(setIf_isset($post, $_POST['slider']) && setIf_isset($ufile,$_FILES['slide'])) {
            $item_id = $slider->saveItem($post, $slide);
            if($file = $upl->saveFile($ufile, null, $slider->dir, array('image/jpeg','image/png'))) {

                if($file_id = $file->saveRecord($slider->getTable(), $item_id, 'image')) {
                    $imager = new Imager;
                    $imager->image = $file->filePath;
                    $imager->type = $file->type;
                    $imager->thumb($file->filePath, 622, 237);//622, 237 ///594, 224
                    $imager->thumb($file->filePath, 100, 63, 'thumb');
                }
                $file->reset();

            } elseif($ufile['error']==1) {

            }

            if($isNew)
                redirect('/ll/admin/slider/');
            else
                redirect();
        }

        return array('back/slider/edit',array('slide'=>$slide));

        break;

    case 'delete':
        if(setIf_isset($id, $this->route[3]) && $slide = $slider->getItem($id)) {
            $slide->delete();
        }
        redirect();
        break;

    case 'list':
    default:

        $slides = $slider->getList();
        return array('back/slider/list',array('slides'=>$slides));

}