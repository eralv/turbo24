<?php

class imager {

	public $multiple;
	public $file;
    public $image;
    public $tmp;
    public $type;
    public $ext = '.jpg';

    public function __construct($temp_name = null) {
        $this->tmp = $temp_name;
    }

	public function file_set($index, array $types) {
	
		if(!empty($_FILES) && isset($_FILES[$index])) {
		
			if($_FILES[$index]['error']==0) {
			
				if(in_array($_FILES[$index]['type'],$types)) {
				
					$this->file = $_FILES[$index];
					return true;
					
				} else return false;
				
			} else return false;
			
		} else return false;
	}
	
	public function save($target_path) {
		if($this->file) {

            $this->type = $this->file['type'];
            list(,$format) = explode('/',$this->type);
            $this->ext = $format == 'jpeg' ? '.jpg' : $format == 'png' ? '.png' : $format == 'gif' ? '.gif' : '.jpg';
            move_uploaded_file($this->file['tmp_name'],$target_path);
			
			$this->image = $target_path;
			
		} elseif($this->tmp) {

            move_uploaded_file($this->tmp,$target_path);

            $this->image = $target_path;

        }
		return $this;
	}
	
	public function thumb($target_path,$wid,$hig, $prefix = null) {
		$source = $this->image===null?$this->image:$target_path;

        list(,$format) = explode('/',$this->type);
        $get_img_func = 'imagecreatefrom' . $format;
        $set_img_func = 'image' . $format;
		$image = $get_img_func($source);
        $quality = ($format == 'jpeg' ? 100 : ($format == 'png' ? 9 : 100));

        list($w, $h) = getimagesize($source);

			$left = 0; $top = 0;
		
			$higX = ceil($h / ($w / $wid));
            $hig = $hig == 0 ? $higX : $hig;

			$widX = $wid;
			if($higX < $hig) {
				$higX = $hig;
				$widX = ceil($w / ($h / $hig));
			}
			
			if($widX > $wid) {
				$left = -1 * ceil(($widX - $wid) / 2);
			} elseif($higX > $hig) {
				$top = -1 * ceil(($higX - $hig) / 2);
			}
			
            $image_p = imagecreatetruecolor($wid, $hig);
            if($format == 'png') {
                imagealphablending($image_p, false);
                imagesavealpha($image_p, true);
            }
            imagecopyresampled($image_p, $image, /**left*/ $left, /**top*/ $top, /**right*/ 0, /**bottom*/ 0, $widX, $higX, $w, $h);

            if($prefix!==null) {
                $target_path = str_replace(basename($target_path), $prefix .'_'. basename($target_path), $target_path);
            }

            $set_img_func($image_p,$target_path,$quality);
			
	}

	public function files_set($index, array $types) {
		if(!empty($_FILES) && isset($_FILES[$index])) {
			$stack = array();
			foreach($_FILES[$index]['type'] as $i=>$type) {
				if(in_array($type,$types)) {
					$stack[] = array(
						'name'=>$_FILES[$index]['name'][$i],
						'type'=>$_FILES[$index]['type'][$i],
						'error'=>$_FILES[$index]['error'][$i],
						'tmp_name'=>$_FILES[$index]['tmp_name'][$i],
						'size'=>$_FILES[$index]['size'][$i]
					);
					return true;
				}
			}
			if($_FILES[$index]['error']==0) {
				return true;
			}
		}
		return false;
	}

}