<?php

class Engine_Router extends Engine_Controller
{

	public $route = array();
	public $params = array();
	public $ajax = false;

	public function __construct($route = '', $lang = null)
	{       
        $uri = explode('?',$_SERVER['REQUEST_URI'],2);
		if(!$route) $route = trim_array(explode('/', $uri[0]));
                
		$lang = array_shift($route);
		//var_dump($_SERVER['REQUEST_URI']);
		//var_dump($route);
	
		/*if($lang == _ALIAS && _ALIAS) {
			$lang = array_shift($route);
			
		}*/
		if(!$lang)
		{
			$lang = self::get('langs')->getLang();
		}
		
		if(reset($route)=='ajax')
		{
			$this->ajax = true;
			array_shift($route);
		}

		$this->get('langs')->setLang($lang);
		
		$this->setRoute($route);
		//var_dump($this->route);
		return $this;
	}
	
	public function changeLang( $l )
	{
		if(empty($this->route)) $this->route = array();
		return '/' . implode('/',trim_array(array_merge(array($l),$this->route)));
	}
	
	public function genUri()
	{
		return '/' . self::get('langs')->getLang() . '/' . implode('/', $this->route);
	}
	
	public function getRoute()
	{
		return $this->route;
	}

	public function changeRoute($param, $value){

		$paramArr = $parArr = array();

		foreach($_GET as $param_name => $param_value){
			if($param_name!='q'){
				$paramArr[$param_name] = $param_value;
			}
		}


		$paramArr[$param] = $value;



		foreach($paramArr as $param_name => $param_value){
			if(!is_array($param_value)){
				$parArr[] = $param_name.'='.$param_value;
			}
			else{
				foreach($param_value as $key=>$value){
					$parArr[] = $param_name.'['.$key.']='.$value;
				}
			}
		}

		if(sizeof($parArr))
			return '/'.$_GET['q'].'?'.implode('&',$parArr);
		else
			return '/'.$_GET['q'];
	}
	
	public function setRoute($r)
	{
		$this->params = array();
		end($r); // pointer to last element
		
		$_ = explode('?',end($r));
		
		$r[key($r)] = array_shift($_);
		if(count($_)>1)
		{
			foreach(explode('&',$_) as $__)
			{
				$val = explode('=', $__);
				$this->params[reset($val)] = end($val);
			}
		}
		
		$r = trim_array($r);
		foreach($r as $k=>$v)
		{
			$r[$k] = urldecode($v);
		}
		
		$this->route = $r;
	}
	
	public function parseRoute($r = null)
	{
		if($r) {
			$this->setRoute($r);
		}

		if(empty($this->route)) {
			$this->route = array('index');
		}

		$this->execController($this->route);
		
	}

	public static function _idByRoute($route, $table)
	{		
		$route_end = array_pop($route);

		if(sizeof($route)) //parents exist
		{
			$j = '';
			$max = sizeof($route);
			for($i=1; $i<=$max; $i++)
			{
				$link = array_pop($route);
				$t = 't' . $i; // cms_tree
				$tt = 'tt' . ($i); // sys_texts
				$j .= 
					'inner join ' . mres($table) . ' ' . $t . ' on (' . $t . '.id=t' . ($i-1) . '.parent) ' . 
					'inner join ' . Engine_DataIO_mysql::_PHRASES_TBL . ' ' . $tt . ' on (' . $tt . '.`table` = "' . mres($table) .'" and ' . $tt . '.record="link" and ' . $tt . '.txt="' . mres($link) .'" and ' . $t . '.id = ' . $tt . '.record_id)';
			}
		}
		
		$id = Engine_Controller::get('db')->field('
			select t0.id from  ' . Engine_DataIO_mysql::_PHRASES_TBL . ' tt0
			inner join ' . mres($table) . ' as t0 on (t0.id =tt0.record_id)
			' . $j . ' 
			where tt0.`table` = "' . mres($table) .'" and tt0.record="link" and tt0.txt="' . mres($route_end) .'"
			group by t0.id',false);
		
		return $id;
	}

}