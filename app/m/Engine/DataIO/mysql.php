<?php

	class Engine_DataIO_mysql extends Engine_DataIO
	{
		const _PHRASES_TBL = 'sys_texts';
		const _FILES_TBL = 'sys_files';
		
		public $_link = null;
		protected $_DB_HOST = 'localhost';
		protected $_DB_NAME = 'turbo24a_web';

		protected $_DB_USER = 'turbo24a_wap';
//		protected $_DB_USER = 'root';

		protected $_DB_PASS = 'RkC[2]9F3Akb';
//		protected $_DB_PASS = '';

		public function __construct()
		{
			
			$this->_link = mysql_connect($this->_DB_HOST, $this->_DB_USER, $this->_DB_PASS);
			if (!$this->_link) {
				die('Not connected : ' . mysql_error());
			}
			$db_selected = mysql_select_db($this->_DB_NAME, $this->_link);
			if (!$db_selected) {
				die (mysql_error());
			}

			mysql_query('SET NAMES "utf8"', $this->_link);
		}

        /**
         * @param $q
         * @param bool $debug
         * @return string|number
         */
        public function field($q, $debug=false)
		{
			if($debug) echo $q;
			$r = mysql_query($q, $this->_link);
            $rez = mysql_fetch_array($r);
            if($rez) {
                return reset($rez);
            }
			return false;
		}

        private function on_object($data , $givenObject) {
            if($data!==null && is_array($data) && is_object($givenObject)) {
                $object = clone $givenObject;
                foreach($data as $k=>$val) {
                    if(!is_numeric($data)) {
                        $object->{$k} = $val;
                    }
                }
                return $object;
            } else {
                return false;
            }
        }

        /**
         * @param $q
         * @param null $object
         * @return array|bool|object
         */
        public function row($q, $object = null, $debug = false)
		{
            if($debug) echo $q;
			$r = mysql_query($q, $this->_link);
            if($r) {
                $data = mysql_fetch_assoc($r);
                if(null!==$object) {
                    $return = $this->on_object($data, $object);
                    return $return;
                } else {
                    return $data;
                }
            }
            return false;
		}

        /**
         * @param String $q
         * @param null|object $object
         * @param null|Closure $rebuild
         * @param bool $debug
         * @return array|object
         */
        public function rows($q, $object = null, $rebuild = null, $debug = false) {

			$r = mysql_query($q, $this->_link);

            if($debug) {
                pp(mysql_error(), $q, $r);
            }

			$results = array();

            if($r) {

                while($row = mysql_fetch_assoc($r)) {

                    $key = reset($row);
                    $keys = array_keys($row);
                    $primary = reset($keys);

                    $val = $row;

                    if(null!==$object) {
                        $val = $this->on_object($val, $object);
                    }

                    $val = ($rebuild !== null && is_callable($rebuild) ? $rebuild($key,$val) : $val);

                    if($primary == 'id') {
                        $results[$key] = $val;
                    } else {
                        $results[] = $val;
                    }

                }

                return $results;

            }

            return false;
		}

		
		public function update($table, $fields, $where, $debug = false)
		{
			$fieldsQ = array();
			foreach($fields as $key=>$f)
			{
				$fieldsQ[] = '`' . $key . '` = "' . mres($f) . '"';
			}
			
			$sql = 'UPDATE `' . $table . '` SET ' . implode(',',$fieldsQ) . ' WHERE ' . $where;

            if($debug)
                echo $sql;

			mysql_query($sql, $this->_link);
			
			return 0 < mysql_affected_rows($this->_link);
		}

		public function delete($table, $where)
		{
			$sql = 'DELETE FROM `' . $table . '` WHERE ' . $where;
			
			mysql_query($sql, $this->_link);
			
			return (bool) mysql_error();
		}
		
		public function insert($table, $fields, $replace = false, $debug = false)
		{
			$fieldsQ = array();

			foreach($fields as $key=>$f)
			{
				$fieldsQ[] = '`' . $key . '` = "' . mres($f) . '"';
			}
			
			$q = ($replace ? 'replace' : 'insert') . ' into `' . $table . '` set '. (empty($fieldsQ) ? '`id`=null' : implode(',',$fieldsQ));

			if($debug){
				echo $q;
			}
			if(mysql_query($q, $this->_link)) {
                return mysql_insert_id($this->_link);
            }
            return false;
		}
		
		
		
		/*
		* Phrases
		*
		*/

		public function aliasExists($alias){

			$sql = 'SELECT record_id FROM '.Engine_DataIO_mysql::_PHRASES_TBL.' WHERE record="link" AND txt="'.$alias.'"';

			$alias_res = mysql_query($sql);

			if(mysql_num_rows($alias_res)){
				return true;
			}
			else{
				return false;
			}


		}
		
		public function text($table, $record, $record_id=0, $lang=null)
		{
			$l = $lang ? $lang : $this->get('langs')->getLang();

			$value = $this->field('select `txt`
			from `' . self::_PHRASES_TBL . '`
			where
			    `table` = "' . mres($table) .'" and
			    `record` = "' . mres($record) . '" and
			    `record_id` = ' . (int)$record_id .' and
			    `lang` = "' . $l . '"');
			if(!$value) {

				if($table==self::_PHRASES_TBL) {

					$value = $this->get('users')->admin() ? $record : '';

				} else {

					$value = '';

				}

			}
			return $value;
		}
		
		
		public function saveText($value, $record, $record_id=0, $table='', $lang=null)
		{
			$l = $lang ? $lang : $this->get('langs')->getLang();

            $table = $table ? $table : self::_PHRASES_TBL;

            if(!$this->update(self::_PHRASES_TBL, array('txt'=>$value), ' record="'.$record.'" AND `table`="'.$table.'" AND `lang`="'.$l.'" AND record_id='.(int)$record_id)) {
                $this->insert(
                    self::_PHRASES_TBL,
                    array(
                        'table' => $table,
                        'record' => $record,
                        'record_id' => $record_id,
                        'txt' => $value,
                        'lang' => $l
                    )
                );
            }
		}
		
		public function saveFile($value,$record_id=0, $table='', $lang=null)
		{
			$l = $lang ? $lang : $this->get('langs')->getLang();

			return $this->insert(
				self::_FILES_TBL,
                array(
                    'table' => $table ? $table : self::_FILES_TBL,
                    'record_id' => $record_id,
                    'file' => $value,
                    'lang' => $l
                ),
				true
			);
		}

		public function phrase($alias, $lang = null)
		{
			return $this->text(self::_PHRASES_TBL, $alias, 0, $lang);
		}
	}
