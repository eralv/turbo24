<?php

	class Engine_Users_Administration extends Engine_Users {
	    public $access;

	    public function __construct() {
            $this->access = array(
                Engine_Users::OPERATOR => array('order_list','couriers')
            );
	    }

	    public function listControls() {
		    $controls = array();
		    $path = _ROOT . '/app/c/admin';
		    foreach(scandir($path) as $ctrl) {
                if(in_array($ctrl,array('.','..'))) continue;
                $name = basename($ctrl,'.php');
                //var_dump($name);
                if($this->canAccess($name)) {
                    $controls[] = $name;
                }
			
		    }
		    return $controls;
		}

	}