<?php

class Engine_Model extends stdClass {

    const TXT_TBL = 'sys_texts';
    const TBL = 'sys_texts';

    public $treeId = 0;

    public $saveErrors = array();

    protected $_blank = null;
    protected $_filters = array();

    public $primary_key = 'id';

    public $validate = false;

    public $self_fields = array();
    public $text_fields = array();
    public $filter_fields = array();
    public $validate_fields = array();

    public function getTable() {
        return static::TBL;
    }

    public function getId($item = null) {
        if(null!==$item) {
            return $this->parseId($item);
        } elseif(isset($this->id)) {
            return $this->id;
        }
        return false;
    }

    public function getBlank($array = false) {

        $data = array();
        $that = clone $this;
        $fields = array_merge($this->self_fields, $this->text_fields);

        foreach($fields as $fld) {
            $that->{$fld} = '';
            if($array) {
                $data[$fld] = '';
            }
        }
        return $array ? $data : $that;

    }

    public function getList($filters = array(), $language = null) {
        return false;
    }

    public function getItem($filter, $language = null) {
        return array();
    }

    public function parseId($item) {
        if(is_array($item) && isset($item[$this->primary_key]) && $item[$this->primary_key]) {
            return $item[$this->primary_key];
        } elseif(is_object($item) && isset($item->{$this->primary_key}) && $item->{$this->primary_key}) {
            return $item->{$this->primary_key};
        } elseif(is_numeric($item) && $item) {
            return $item;
        } elseif($item === null) {
            return $this->parseId($this);
        }
        return false;
    }

    public function parseFields($fields) {

        if(!empty($fields) && is_array($fields)) {
            return implode(', ', $fields);
        }
        return '*';

    }

    public function parseSettings($settings = array()) {
        $sql = '';
        if(setIf_isset($field, $settings['order']) && in_array($field, $this->self_fields)) {
            $sql .= ' ORDER BY `'.$field.'` '.
                (setIf_isset($sort,$settings['sort']) && in_array(strtolower($sort),array('asc','desc'))
                    ? $sort
                    : ''
                );
        }
        if(setIf_isset($limit, $settings['limit'])) {
            $sql .= ' LIMIT '.$limit;
        }

        return $sql;
    }

    public function parseLang($language = null) {
        return $language!==null
            ? (cms::get('langs')->isLang($language)
                ? $language
                : cms::get('langs')->defLang())
            : cms::get('langs')->getLang();
    }

    public function getFields($alias = null, $fieldList = false) {
        $fields = array();

        foreach(is_array($fieldList) ? $fieldList : $this->self_fields as $field) {
            $fields[] = ($alias!==null ? '`'.$alias.'`.' : '') .'`'.$field.'`';
        }

        return $fields;
    }

    public function joinTexts($table, $alias, $on = 0, $language = null) {

        $fields = $joins = array();

        $i = 0;
        foreach($this->text_fields as $field) {
            $txt_alias = 'txt'.$i++;
            $fields[] = '`'.$txt_alias.'`.`txt` as `'.$field.'`';
            $joins[] = $this->textJoin(array($table, $txt_alias), $field, $on, $language);
        }

        return array($fields, $joins);

    }

    public function textJoin($table_n_alias, $field, $on = 0, $language = null) {

        list($table, $alias) = $table_n_alias;

        $lang = $this->parseLang($language);

        return 'LEFT JOIN `'. self::TXT_TBL .'` as `'.$alias.'` ON
        `'.$alias.'`.`record_id`='.$on.' AND
        `'.$alias.'`.`table`="'.$table.'" AND
        `'.$alias.'`.`record`="'.$field.'" AND
        `'.$alias.'`.`lang`="'.$lang.'"';

    }

    public function delete($item = null) {
        if($id = $this->getId($item)) {
            cms::get('db')->delete($this->getTable(),' id='.(int)$id);
            cms::get('db')->delete(self::TBL,' `table`="'.$this->getTable().'" AND record_id='.(int)$id);
        }
    }

    public function saveTexts($text = array(), $record, $record_id = null) {
		    // var_dump($text);die();
        if(is_array($text)) {
            $rec_id = null!==$record_id ? $record_id : $this->getId();
            foreach($text as $lang=>$val) {
                cms::get('db')->saveText($val, $record, $rec_id, $this->getTable(), $lang);
            }
        }

    }

    public function getText($record, $language = null, $item = null) {

        if(in_array($record, $this->text_fields)) {

            if($language === false) {

                $rez = array();
                foreach(cms::get('langs')->listLangs() as $lang) {
                    $rez[$lang] = cms::get('db')->text($this->getTable(), $record, $this->getId($item), $lang);
                }
                return $rez;

            } else {

                return cms::get('db')->text($this->getTable(), $record, $this->getId($item), $this->parseLang($language));

            }
        }
        return false;

    }

    public function parseUriFilters($route) {

        $fetch = false;
        $args = array();
        foreach($route as $elm) {
            if($elm=='filters') {
                $fetch = true;
                continue;
            } elseif(!$fetch) continue;
            if(false!==strpos($elm, ':')) {
                list($key, $val) = explode(':', $elm, 2);
                if(in_array($key, $this->_filters)) {
                    $args[$key] = $val;
                }
            }
        }

        return $args;
    }

    public function filter(array $filters) {
        $uri = array();

        foreach($filters as $filt=>$val) {
            $val = trim($val);
            if(in_array($filt, $this->_filters) && !empty($val)) {
                $uri[] = $filt .':'. $val;
            }
        }

        return 'filters/'. implode('/',$uri);
    }

    public function filteredFields($post = array()) {

        $results = array();

        if(setIf_isset($fields, $this->filter_fields) && !empty($fields)) {
            foreach($this->filter_fields as $field) {
                if(setIf_isset($data, $post[$field])) {
                    $results[$field] = hsc($data);
                }
            }
        }

        return $results;

    }

    public function setError($field, $param, $singleError = false, $fieldNamesOnly = false) {

        if($singleError && $fieldNamesOnly && !in_array($field, $this->saveErrors)) {

            $this->saveErrors[] = $field;

        } elseif($singleError && !$fieldNamesOnly && !isset($this->saveErrors[$field])) {

            $this->saveErrors[$field] = cms::get('langs')->text('site','validator_error:'.$param.'-'.$field.'');

        } elseif(!$singleError) {
            if(!setIf_isset($error, $this->saveErrors[$field]) || !is_array($error)) {
                $this->saveErrors[$field] = array();
            }
            $this->saveErrors[$field][$param] = cms::get('langs')->text('site','validator_error:'.$param.'-'.$field.'');
        }
    }

    public function validate($data = array(), $singleError = false, $fieldNamesOnly = false) {

        foreach($this->validate_fields as $field=>$valids) {

            $value = isset($data[$field]) ? $data[$field] : null;

            if(in_array('trim', $valids)) {
                $value = trim($value);
            }
            if(in_array('required', $valids) && empty($value)) {
                $this->setError($field, 'required', $singleError, $fieldNamesOnly);
            }

            foreach($valids as $param=>$reg_exp) {
                if(!is_numeric($param)) {
                    if(!preg_match($reg_exp, $value)) {
                        $this->setError($field, $param, $singleError, $fieldNamesOnly);
                    }
                }
            }


        }

    }

} 