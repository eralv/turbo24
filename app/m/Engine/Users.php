<?php

	class Engine_Users extends Engine_Model {

		const TBL = 'sys_users';
		const SUPERUSER = 4;
		const OPERATOR = 2;
		const USER = 1;

        public $self_fields = array(
            'id', 'token', 'status', 'rate', 'username', 'name', 'rules', 'group_id', 'surname', 'pers_code',
            'phone', 'email', 'company_name', 'reg_number', 'pvn_number', 'jur_address',
            'fact_address', 'iban_number', 'position', 'juridic', 'social', 'social_id', 'address',
            'bank_name', 'bank_swift'

        );

        public $validate_fields = array(
            'name' => array('trim','required'),
            'surname' => array('trim','required'),
            'pers_code' => array('trim','required'),
            'phone' => array('trim','required',
                'valid'=>'/^\+?[\d]+$/'
            ),
            'email' => array('trim','required',
                'valid'=>'/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i'
            )
        );

        public $filter_fields = array(
            'status', 'rate', 'username', 'name', 'surname', 'pers_code',
            'phone', 'email', 'company_name', 'reg_number', 'pvn_number', 'jur_address',
            'fact_address', 'iban_number', 'address', 'position',
            'bank_name', 'bank_swift'

        );

        public $rules = array(
            'guest', 'user', 'operator', 'admin', 'superuser'
        );


        public function __construct() {
            if(self::gi()) {
                $_SESSION[__CLASS__] = $this->getItem($this->getId());
            }
        }

        /**
         * @return bool|array[]
         */
        public static function gi() {
            return isset($_SESSION[__CLASS__]) ? $_SESSION[__CLASS__] : false;
        }

        public static function is($status = NULL)
        {
            $user = self::gi();
            if($user!==false){
                if($status!==NULL){
                    return $user['rules']==$status;
                }
                return true;
            }
            return FALSE;
        }

        public static function su() {

            return self::gi() ? $_SESSION[__CLASS__]['rules'] == self::SUPERUSER : false;

        }

        public static function rule() {

            return self::gi() ? $_SESSION[__CLASS__]['rules'] : false;

        }


        public static function admin() {
            $that = new self;
            return self::gi() ? $_SESSION[__CLASS__]['rules'] >= array_search('admin', $that->rules) : false;

        }

        public function logout() {

            unset($_SESSION[__CLASS__]);
        }
		
		public function login($login, $pass)
		{
			$success = false;

			$l = mres($login);
			$p = $this->hash($pass);

			$sql = 'select `id`, `status`, `rules` from `'. self::TBL .'` where `username`="'. $l .'" AND `password` LIKE "'. $p .'"';
			
			
			$user = cms::get('db')->row($sql);

			if($user['id'] && $user['status'] > 0) {
			    $_SESSION[__CLASS__] = $user;
			    $success = true;
			}

			//CMS_SystemMessages::save( $success ? 'success' : 'error', 'Autorizācija '. ($success?'':'ne') .'veiksmīga');
			
			return $success;
		}
		public function accept_user($token) {
		    $success = false;
		    $sql = 'select `id` from `'. self::TBL .'` where `token`="'. $token .'" ';
		    $user = cms::get('db')->row($sql);
		    if($user['id']) {
                $_SESSION[__CLASS__] = $user;
                $success = true;
                cms::get('db')->update(self::TBL, array('token' =>'','status'=>'1'),'token = "'.$token.'"');
		    }
			
			CMS_SystemMessages::save( $success ? 'success' : 'error', 'Lietotajs '. ($success?'':'ne') .'akceptēts');

			return $success;
		}

        public function social() {

            if($this->is()) {
                if($user = $this->gi()) {
                    if(null !== $user['social']) {
                        return $user['social'];
                    }
                }
            }

            return false;

        }

		public function restore_password($user)
		{
		    
		    if($user) {

                $token = str_replace('/','',base64_encode(sha1(date('Y-m-d H:m:s').$user['id'])));

                $mail = new CMS_Mailing();

                $mail->to = $user['email'];

                $mail->subject = 'Turbo24.lv User Accept';
                $mail->body = 'Uzspiediet uz sho  <a href="'._HOST.'lv/?restore_password&token='.$token.'">linku </a>';

                $mail->sendMail();

                cms::get('db')->update(self::TBL, array('token' =>$token),' id = "'.$user['id'].'"');

		    }

		}

		public function get_restore_token($token)
		{
		    return cms::get('db')->row('select `id` from `'. self::TBL .'` where `token`="'. mres($token) .'" ');
		}

		public function hash($string) {
            return md5(hash('sha1', $string));
        }

		public function getId($item = null) {
            if($this->is()) {
                return (int)$_SESSION[__CLASS__]['id'];
            }
            return false;
		}
		
		public function getName($id) {
			$sql = "SELECT name FROM ".self::TBL." WHERE id=".(int)$id;
			return cms::get('db')->field($sql);
		}

        public function getItem($filter = array(), $language = null) {

            $where = array();

            if(is_numeric($filter)) {
                $where[] = 'id='.$filter;
            } elseif(is_array($filter)) {
                foreach ($filter as $key=>$value){
                    if(in_array($key,$this->self_fields)) {
                        $where[] = $key.'="'.mres($value).'"';
                    }
                }
            }
	    
            $sql = 'SELECT '. $this->parseFields($this->getFields()) .'
            FROM `'. $this->getTable() .'`
            WHERE '. (!empty($where) ? implode(' AND ', $where) : '');

            return cms::get('db')->row($sql);

        }
	
        public function getItemAll($filter = array(), $language = null) {
	    
	        $where = array();

            if(is_numeric($filter)) {
                $where[] = 'id='.$filter;
            } elseif(is_array($filter)) {
                foreach ($filter as $key=>$value){
                    if(in_array($key,$this->self_fields)) {
                        $where[] = $key.'="'.mres($value).'"';
                    }
                }
            }

            $sql = 'SELECT '. $this->parseFields($this->getFields()) .' FROM `'. self::TBL .'` '. (!empty($where)?('WHERE '. implode(' AND ',$where)):'');

            return cms::get('db')->row($sql);

        }
	

        public function getList($filters = array(), $language = null, $settings = array()) {

            $fields = array();

            $alias = 'us';
            if(isset($filters['juridic']) && $filters['juridic']<0) {
                unset($filters['juridic']);
            }
	            foreach ($filters as $key=>$value){
                    if(in_array($key,$this->self_fields) && strlen($value)) {
                        if(in_array($key, array('name','surname','company_name'))) {
                            $where[] = $key.' LIKE "%'.mres($value).'%"';
                        } else {
                            $where[] = $key.'="'.mres($value).'"';
                        }
                    }
                }
            array_merge($fields,$this->getFields($alias));
	    
            return cms::get('db')->rows(
                'SELECT '. $this->parseFields($fields) .'
                FROM `'. self::TBL .'` as `'. $alias .'`'.(!empty($where)?('WHERE '. implode(' AND ',$where)):'') .'
                '. $this->parseSettings($settings)
            );

        }

        public function countItems($filters = array()) {

            if(isset($filters['juridic']) && $filters['juridic']<0) {
                unset($filters['juridic']);
            }
            foreach ($filters as $key=>$value){
                if(in_array($key,$this->self_fields) && strlen($value)) {
                    if(in_array($key, array('name','surname','company_name'))) {
                        $where[] = $key.' LIKE "%'.mres($value).'%"';
                    } else {
                        $where[] = $key.'="'.mres($value).'"';
                    }
                }
            }

            return cms::get('db')->field('SELECT COUNT(1)
            FROM '. $this->getTable() .'
            '.(!empty($where)?('WHERE '. implode(' AND ',$where)):''));
        }

        public function checkUnique($field, $value) {
            if(in_array($field, $this->self_fields) && !$this->getItem(array($field=>$value))) {
                return true;
            }
            return false;
        }

        public function saveItem($post, $item = false) {
            $id = $this->parseId($item);
            $fields = $this->filteredFields($post);

            if(isset($post['password'])) {
                $pw = trim($post['password']);
                if(!empty($pw))
                    $fields['password'] = $this->hash($pw);
            }
            if(!$id) {
                $fields['username'] = $fields['email'];
            }

            if(isset($post['rules']) && cms::get('users')->admin()) {
                $fields['rules'] = (int)$post['rules'];
            }

            $fields['juridic'] = (int)isset($post['juridic']);
            if($fields['juridic'] && $this->validate) {
                $this->validate_fields += array(
                    'company_name' => array('trim','required'),
                    'reg_number' => array('trim','required'),
                    'pvn_number' => array('trim','required'),
                    'jur_address' => array('trim','required'),
                    'fact_address' => array('trim','required'),
                    'iban_number' => array('trim','required'),
                    'position' => array('trim','required'),
                    'bank_name' => array('trim','required'),
                    'bank_swift' => array('trim','required')
                );
            } elseif(!$fields['juridic'] && $this->validate) {
                unset($fields['company_name'],$fields['reg_number'],$fields['pvn_number'],$fields['jur_address'],$fields['iban_number'],$fields['bank_name'],$fields['bank_swift'],$fields['position']);
            }

            if($this->validate)
                $this->validate($post,true,true);

            if(empty($this->saveErrors)) {

                if($id) {
                    cms::get('db')->update(self::TBL, $fields, ' id='.$id);
                } else {
                    if(isset($post['type_add'])){
                        $fields['status'] = 1;
                    } else {
                        $fields['token'] = base64_encode(sha1(date('Y-m-d H:m:s').$fields['email']));
                    }

                    if($id = cms::get('db')->insert(self::TBL, $fields) && !isset($post['type_add'])) {
                        $mail = new CMS_Mailing();

                        $mail->to = $fields['email'];

                        $mail->subject = 'Turbo24.lv User Accept';
                        $mail->body = 'Sveicināti '.$fields['name'].' '.$fields['surname'].',<br />
                        Lai apstuprinātu reģistrāciju Turbo24, spiediet uz <a href="'._HOST.'/'.cms::get('langs')->getLang().'/validation/'.$fields['token'].'">šīs saites</a>';

                        $mail->sendMail();
                    }
                }

                return $id;
            }

            return false;
        }

        public function deleteItem($id) {
            cms::get('db')->delete(self::TBL, ' id='.(int)$id);
        }

		public function fetch()
		{
			if($this->is()) {

				if($this->getId()) {

                    return cms::get('db')->row('select * from `'. self::TBL .'` where id = "'. $this->getId() .'"');

				}
			}
			return false;
		}
		
		public function canAccess($controller){
            if($this->is()){
                $rules = $this->rule();
                $admin = new Engine_Users_Administration();
                if($this->admin() || (isset($admin->access[$rules]) && in_array($controller, $admin->access[$rules]))){
                    return true;
                }
		    }
		    return false;
		}


		// admin controls 
		public function control($command, $param = null, $adds = array())
		{ 
			if(!$this->admin() && (!$this->is(self::OPERATOR) || $command!='menu')) return null;
			$controls = new Engine_Users_Administration;
			$tpl = cms::get('tpls');
			switch($command)
			{
			
				case 'menu':
					
					if($param) {

                        return ' data-edit="admin_controls tree '. $param .'" ';

					} else {

						$controls = $controls->listControls();
						if(!empty($controls)) {
							$p = array(
								'menu' => $controls
							);

							return $tpl
								->loadVars($p)
								->returnTpl('back/admin_controls/menu');
						}

					}
					
				    break;

                case 'config':

                    if(isset($param)) {
                        return ' data-edit="admin_controls config ' . $param . '" ';
                    }

                    break;
				
				case 'phrase':
					
					if(isset($param)) {
						return ' data-edit="admin_controls phrase ' . $param . '" ';
					}

				    break;

			}
		
			return null;
		}
	}
