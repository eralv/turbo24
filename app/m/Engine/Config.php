<?php

class Engine_Config extends Engine_Controller {

    const TBL = 'sys_config';

    public function read($name) {
        return $this->get('db')->field('SELECT `value` FROM '. self::TBL .' WHERE `record`="'. $name .'"');
    }

    public function write($name, $value) {
        if($this->read($name) !== false) {
            $this->get('db')->update(self::TBL, array('value'=>mres($value)), ' record="'.$name.'"');
        } else {
            $this->get('db')->insert(self::TBL, array('record'=>$name,'value'=>mres($value)));
        }
    }


    /** ===================== */


    public function analytics() {

        if($code = $this->read('analytics')) {
            echo $this->get('tpls')->loadVars(array(
                'code'=>$code,
                'domain'=>_HOST
            ))->returnTpl('back/config/analytics');
        }

    }

} 