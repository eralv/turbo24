<?php

require_once(_ROOT .'library/draugiem/DraugiemApi.php');

class CMS_Draugiem {

	const TBL = 'sys_users';

	private $allowedMethods = array(
		'complete_registration'
	);

	public $app_id = 15017398;
	public $app_key = "203ab4d5a4e732e9bd12b4deab54b3df";

    public $url = "ll/auth/draugiem/";


	function __construct()
	{
		
		if(isset($_GET['action']) && in_array($_GET['action'], $this->allowedMethods)) {

			$this->{$_GET['action']}($_GET);

		} elseif(!empty($_GET['draugiem_key']) && $user = $this->getUser()) {
            list($id) = $user;

            if($user_row = cms::get('users')->getItem(array('social_id'	=> $id, 'social' => 'draugiem'))) {

                $this->authDraugiem();

            } elseif(!isset($_GET['email_req']) && !isset($_GET['email'])) {

                redirect($this->getUrl() .'?action=complete_registration&draugiem_key='. $_GET['draugiem_key']);

            }

        } elseif($_GET["dr_auth_status"]=='ok') {

			$this->authDraugiem();

		}

	}

    public function getUrl() {
        return _HOST . $this->url;
    }

    public function getUser($token = false) {
        $dr = new DraugiemApi($this->app_id, $this->app_key);

        if($dr->getSession() && $userData = $dr->getUserData()) {
            if($token)
                return $dr->getUserKey();
            else
                return array($userData['uid'],  $userData['name'], $userData['surname']);
        }

        return false;
    }

	public function authDraugiem(){

		if($user = $this->getUser()) {
            list($id) = $user;

            $user_row = cms::get('users')->getItem(array(
                'social_id'	=> $id,
                'social'    => 'draugiem'
            ));

            if(!$user_row['id']){

                redirect($this->getUrl() .'?draugiem_key='. $this->getUser(true));

            } else {
                $_SESSION['Engine_Users'] = $user_row;
                unset($_SESSION['requestToken']);

                if(isset($_SESSION['referrer'])) {
                    redirect($_SESSION['referrer']);
                } else {
                    redirect(_HOST);
                }

            }

		}
		else{

			CMS_SystemMessages::save( 'error', 'draugiem_auth_error');

		}

        redirect(_HOST);


	}

	public function complete_registration($params) {
		
		if($params['draugiem_key'] && $user = $this->getUser()) {
            list($id, $name, $surname) = $user;

            $fields = array(
                'name'	    => $name,
                'surname'   => $surname,
                'social_id'	=> $id,
                'social'    => 'draugiem'
            );

            $uid = Engine_Controller::get('db')->insert(self::TBL, $fields);

            if($user_row = cms::get('users')->getItem($uid)) {
                $_SESSION['Engine_Users'] = $user_row;

                if(isset($_SESSION['referrer']))
                    redirect($_SESSION['referrer']);

            }

        }

        redirect(_HOST);
	}


	public function logout(){

		session_destroy();

	}

}