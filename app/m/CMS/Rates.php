<?php

	class CMS_Rates extends Engine_Model {

	    const TBL = 'cms_rates';
	    const SUPERUSER = 4;

    public $self_fields = array(
	'id', 'name', 'value'
    );

    public $filter_fields = array(
	'name', 'value'
    );


    public function getName($id) {
	    $sql = "SELECT name FROM ".self::TBL." WHERE id=".(int)$id;
	    return cms::get('db')->field($sql);
    }

    public function getItem($filter = array(), $language = null) {
	$sql = 'SELECT '. $this->parseFields($this->getFields()) .' FROM `'. self::TBL .'` WHERE `id`='. (int)$filter;

	return cms::get('db')->row($sql);

    }

        public function getRate($user = null) {

            if($user && setIf_isset($rate, $user['rate']) && $rate_value = $this->getItem($rate)){
                return $rate_value['value'];
            }
            return cms::get('cfg')->read('default_rate');
        }

        public function getList($filters = array(), $language = null) {

            $fields = array();

            $alias = 'us';
            $where = array();

            foreach ($filters as $key=>$value){
                if(in_array($key,$this->self_fields)) {
                    if(in_array($key,array('name'))) {
                        $where[] = $key.' LIKE "%'.mres($value).'%"';
                    } else {
                        $where[] = $key.'="'.mres($value).'"';
                    }
                }
            }
            array_merge($fields,$this->getFields($alias));

                $sql = 'SELECT '. $this->parseFields($fields) .' FROM `'. self::TBL .'` '. (!empty($where)?('WHERE '. implode(' AND ',$where)):'');
            return cms::get('db')->rows($sql);

        }

        public function saveItem($post, $item = false) {
            $id = $this->parseId($item);

            $fields = $this->filteredFields($post);

            if($id || setIf_isset($id,$post['id'])) {
                cms::get('db')->update(self::TBL, $fields, ' id='.$id);
            } else {
                cms::get('db')->insert(self::TBL, $fields);
            }

        }

        public function deleteItem($id) {
            cms::get('db')->delete(self::TBL, ' id='.(int)$id);
        }

	    public function fetch()
	    {
		    if($this->is()) {

			    if($this->getId()) {

		return cms::get('db')->row('select * from `'. self::TBL .'` where id = "'. $this->getId() .'"');

			    }
		    }
		    return false;
	    }


    }