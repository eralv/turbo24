<?php

class CMS_Slider extends Engine_Model {

    const TBL = 'cms_slider';

    public $self_fields = array(
        'id', 'link', 'title', 'sort'
    );

    public $text_fields = array(
        'text'
    );

    public $dir = 'slider';

    public function imgPlaceholder() {
        return '/www/images/slide.jpg';
    }

    public function getImgPath($prefix = null) {
        if(isset($this->slide)) {
            return CMS_Uploads::getPath( (null!==$prefix?$prefix.'_':''). $this->slide, $this->dir);
        }
        return $this->imgPlaceholder();
    }

    public function getList($filter = array(), $language = null) {

        $alias = 'sl';
        $fields = $this->getFields($alias);
        $joins = array();

        list($txt_fields,$txt_joins) = $this->joinTexts($this->getTable(), $alias, '`'.$alias.'`.`'.reset($this->self_fields).'`', $language);

        $fields = array_merge($fields, $txt_fields);
        $joins = array_merge($joins, $txt_joins);

        $fields[] = 'fl.file as `slide`';
        $joins[] = 'LEFT JOIN `sys_files` fl ON fl.record_id='.$alias.'.id AND fl.`table`="'.$this->getTable().'" AND fl.id = (
            SELECT MAX(flz.id) FROM `sys_files` flz WHERE flz.record_id='.$alias.'.id AND flz.`table`="'.$this->getTable().'")';

        return cms::get('db')->rows(
            'SELECT '. $this->parseFields($fields) .'
            FROM `'. $this->getTable() .'` as `'. $alias .'`
            '. (!empty($joins)?implode("\n",$joins):'') .'
            ORDER BY `'. $alias .'`.`sort` ASC'
            , (new self));

    }

    /**
     * @param $filter
     * @param null $language
     * @return self
     */
    public function getItem($filter, $language = null) {

        $fields = $this->getFields('sl');
        $fields[] = 'fl.file as `slide`';
        $joins = array('LEFT JOIN `sys_files` fl ON fl.record_id=sl.id AND fl.`table`="'.$this->getTable().'" AND fl.id = (
            SELECT MAX(flz.id) FROM `sys_files` flz WHERE flz.record_id=sl.id AND flz.`table`="'.$this->getTable().'")');

        $sql = 'SELECT '. $this->parseFields($fields) .'
        FROM `'. $this->getTable() .'` sl
        '. (!empty($joins)?implode("\n",$joins):'') .'
        WHERE sl.`id`='. (int)$filter;

        return cms::get('db')->row($sql, (new self));

    }

    /**
     * @param array $post
     * @param bool|array|stdClass $item
     * @return int
     */
    public function saveItem(array $post, $item = false) {
        $item_id = $this->parseId($item);

        $data = array_intersect_key($post, array_swap_vk($this->self_fields));

        if($item_id) {
            cms::get('db')->update($this->getTable(), $data, ' id='.(int)$item_id);
        } else {
            $item_id = cms::get('db')->insert($this->getTable(), $data);
        }

        if(isset($post['text']))
            $this->saveTexts($post['text'], 'text', $item_id);

        return $item_id;
    }

} 