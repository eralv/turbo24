<?php

class CMS_Uploads extends Engine_Model {

    const TBL = 'sys_files';

    public $uploadDir = 'uploads';

    public $self_fields = array(
        'id', 'table',
        'record_id', 'lang',
        'file', 'file_type',
        'dir', 'primary',
        'sort'
    );

    public $file = null;
    public $type = null;
    public $dir = null;
    public $filePath = null;

    private function mkdir($path) {
        $nPath = '';
        $dirs = explode('/',$path);
        foreach($dirs as $dir) {
            $nPath .= $dir .'/';
            if(!file_exists($nPath)) mkdir($nPath,0777);
        }
    }

    public static function getExt($file_name) {

        $ex = explode('.',$file_name);
        return end($ex);

    }

    public static function getUrl($dir) {
        $that = new self;
        return _HOST . $that->uploadDir .'/'. $dir .'/';
    }

    public static function getDir($dir) {
        $that = new self;
        return _ROOT . $that->uploadDir .'/'. $dir .'/';
    }

    public static function getPath($file, $dir, $onSys = false) {
        $that = new self;
        if($onSys) return $that->getDir($dir) . $file;
        else return $that->getUrl($dir) . $file;
    }

    public function reset() {
        $this->file = null;
        $this->type = null;
        $this->dir = null;
        $this->filePath = null;
    }

    /**
     * @param array $file
     * @param string|null $fileName
     * @param string $dir
     * @param array $types
     * @return self|bool
     */
    public function saveFile(array $file, $fileName = null, $dir = '', $types = array()) {
        $this->file = $fileName!==null ? $fileName : md5($file['name'] . time()) .'.'. $this->getExt($file['name']);
        $this->dir = $dir;
        $this->type = $file['type'];
        if($file['error']==0 && (empty($types) || in_array($file['type'],$types))) {
            $path = $this->getDir($this->dir);
            $this->mkdir($path);
            $this->filePath = $path . $this->file;
            if(move_uploaded_file($file['tmp_name'], $this->filePath)) {
                return $this;
            }
        }
        $this->reset();
        return false;
    }

    public function setPrimary($image_id, $table, $rec_id, $dir) {
        cms::get('db')->update($this->getTable(), array('primary'=>0), ' `table`="'.mres($table).'" AND `rec_id`='.(int)$rec_id.' AND `dir`="'.mres($dir).'"');
        cms::get('db')->update($this->getTable(), array('primary'=>1), ' `id`='.(int)$image_id);
    }

    public function saveRecord($table, $rec_id = 0, $type = 'file', $unique = false) {

        if($id = cms::get('db')->insert($this->getTable(), array('file'=>$this->file,'table'=>$table, 'record_id'=>$rec_id, 'file_type'=>$type, 'dir'=>$this->dir))) {
            return $id;
        }

        return false;
    }

    public function saveItem(array $post, $item = false) {
        $item_id = $this->parseId($item ? $item : $this);

        $data = array_intersect_key($post, array_swap_vk($this->self_fields));

        if($item_id) {
            cms::get('db')->update($this->getTable(), $data, ' id='. (int)$item_id);
        } else {
            cms::get('db')->insert($this->getTable(), $data);
        }

    }

    public function getRecords($table, $dir, $rec_id = 0, $type = 'file') {

        $sql = 'SELECT *
        FROM `'. $this->getTable() .'`
        WHERE `table`="'.$table.'" AND `dir`="'.$dir.'" AND `record_id`='. (int)$rec_id .' AND `file_type`="'.$type.'"
        ORDER BY `sort` ASC';

        return cms::get('db')->rows($sql);
    }

    public function getPrimary($table, $dir, $rec_id = 0, $type = 'file') {

        $sql = 'SELECT *
        FROM `'. $this->getTable() .'`
        WHERE `table`="'.$table.'"
            AND `dir`="'.$dir.'"
            AND `record_id`='. (int)$rec_id .'
            AND `file_type`="'.$type.'"
        ORDER BY `primary` DESC, `id` DESC, `sort` ASC
        LIMIT 1';

        return cms::get('db')->row($sql);
    }

} 