<?php

	class CMS_Routes extends Engine_Model {

		const TBL = 'cms_routes';
		const SUPERUSER = 4;

        public $self_fields = array(
            'id', 'user_id', 'location_from', 'location_to', 'address_name', 'address_surname',
	    'address_phone', 'address_email'
        );

        public $filter_fields = array(
	    'location_from', 'location_to', 'address_name', 'address_surname',
	    'address_phone', 'address_email'
        );

   
		public function getName($id) {
			$sql = "SELECT name FROM ".self::TBL." WHERE id=".(int)$id;
			return cms::get('db')->field($sql);
		}

        public function getItem($filter = array(), $language = null) {

            $sql = 'SELECT '. $this->parseFields($this->getFields()) .' FROM `'. self::TBL .'` WHERE `id`='. (int)$filter;

            return cms::get('db')->row($sql);

        }

        public function getList($filters = array(), $language = null) {

            $fields = array();

            $alias = 'us';
	    $where = array();

            foreach ($filters as $key=>$value){
                if(in_array($key,$this->self_fields)) {
                    $where[] = $key.'="'.mres($value).'"';
                }
            }
            array_merge($fields,$this->getFields($alias));
	    
		$sql = 'SELECT '. $this->parseFields($fields) .' FROM `'. self::TBL .'` '. (!empty($where)?('WHERE '. implode(' AND ',$where)):'');
            return cms::get('db')->rows($sql);

        }

        public function saveItem($post, $item = false) {
            $id = $this->parseId($item);

            $fields = $this->filteredFields($post);
	        $fields['user_id'] = cms::get('users')->getId();
	    
            if($id) {
                cms::get('db')->update(self::TBL, $fields, ' id='.$id);
            } else {
                cms::get('db')->insert(self::TBL, $fields);
            }

        }

        public function deleteItem($id) {
            cms::get('db')->delete(self::TBL, ' id='.(int)$id);
        }

		public function fetch()
		{
			if($this->is()) {

				if($this->getId()) {

                    return cms::get('db')->row('select * from `'. self::TBL .'` where id = "'. $this->getId() .'"');

				}
			}
			return false;
		}
		

		// admin controls 
		public function control($command, $param = null, $adds = array())
		{
		
			if(!$this->admin()) return null;
			
			$controls = new Engine_Users_Administration;
			$tpl = cms::get('tpls');
			
			switch($command)
			{
			
				case 'menu':
					
					if($param) {

                        return ' data-edit="admin_controls tree '. $param .'" ';

					} else {

						$controls = $controls->listControls();
						if(!empty($controls)) {
							$p = array(
								'menu' => $controls
							);

							return $tpl
								->loadVars($p)
								->returnTpl('back/admin_controls/menu');
						}

					}
					
				    break;

                case 'slider':

                    if(isset($param)) {
                        return ' data-edit="admin_controls slider ' . $param . '" ';
                    }

                    break;
				
				case 'phrase':
					
					if(isset($param)) {
						return ' data-edit="admin_controls phrase ' . $param . '" ';
					}

				    break;

			}
		
			return null;
		}
	}
