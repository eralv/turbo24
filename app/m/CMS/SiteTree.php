<?php

class CMS_SiteTree extends Engine_Model
{
    const TBL = 'cms_tree';

    static $_tree_fields = array(
        'link',
        'title',
        'description',
        'metatitle',
        'metakeywords',
        'metadescription'
    );

    public $primary_key = 'treeId';

    public $text_fields = array(
        'link',
        'title',
        'description',
        'metatitle',
        'metakeywords',
        'metadescription'
    );




    public $treeId = null;
    public $treeInfo = array();
    public $parents = array();
    public $childs = array();

    public function __construct($p = null)
    {
        if ($p) {
            switch (gettype($p)) {
                case 'array': // all route

                    $tp = array_reverse($p);

                    foreach ($tp as $tlink) {
                        $p = $this->siteIdByAlias($tlink);
                        if($p) break;
                    }


                    $this->treeId = $p;
                    if ($this->treeId) {
                        $this->treeInfo = $this->treeById($this->treeId);
                        if ($this->isParent($this->treeId) && $this->treeInfo['default_child']) {
                            $this->treeId = $this->treeInfo['default_child'];
                            $this->treeInfo = $this->treeById($this->treeId);
                        }
                    }
                    break;

                case 'integer':
                    $this->treeId = $p;
                    break;
            }


            // getting menu parents
            $parents = array($this->treeId);


            $parent = $this->treeInfo;
            while ($parent = $this->treeById($parent['parent'])) {
                $parents[] = $parent['id'];
            }

            $this->parents = $parents;

        }
    }

    public function meta($key, $val) {

        if(in_array('meta'. $key, self::$_tree_fields)) {
            $this->treeInfo['meta'. $key] = $val;
        }

    }

    public function isParent($id)
    {
        $sql = "SELECT count(1) FROM `" . $this->getTable() . "` WHERE parent=" . (int)$id;
        $count = Engine_Controller::get('db')->field($sql);
        if ($count > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function getParent($id)
    {

        $sql = "SELECT parent FROM `" . $this->getTable() . "` WHERE id=" . (int)$id;
        return Engine_Controller::get('db')->field($sql);

    }

    public function has($id)
    {


        //$parents = $this->getIdHistory($id);
        //return !empty($parents) ? in_array($id, $parents) : false;

        return !empty($this->parents) ? in_array($id, $this->parents) : false;
    }

    public function is_active($id) {
        return $id == $this->treeId || $this->has($id);
    }

    public function __get($n)
    {
        if (isset($this->treeInfo[$n])) {
            return $this->treeInfo[$n];
        }
        return '';
    }

    public function treeById($id = null, $lang = null)
    {
        $id = $id !== null ? $id : $this->treeId;
        $l = $lang ? $lang : cms::get('langs')->getLang();

        $j = $fields = '';
        $i = 0;

        foreach (self::$_tree_fields as $f) {
            $t = 't' . (++$i);
            $j .= 'inner join ' . Engine_DataIO_mysql::_PHRASES_TBL . ' ' . $t . ' on (' . $t . '.table="' . $this->getTable() . '" and ' . $t . '.record="' . $f . '" and ' . $t . '.record_id=ct.id and ' . $t . '.lang = "' . $l . '") ';
            $fields .= ', ' . $t . '.txt as `' . $f . '`';
        }

        $fields .= ', `fl`.`file` as `image`';
        $j .= 'LEFT JOIN `sys_files` as `fl` ON `ct`.`id`=`fl`.`record_id` AND `fl`.`table`="'.$this->getTable().'"';

        $tree = cms::get('db')->row('select ct.* ' . $fields . '
				from `' . $this->getTable() . '` ct
				' . $j . '
				where ct.id = ' . (int)$id);

        if($tree) {
            $upl = new CMS_Uploads();
            $tree['image'] = $upl->getPrimary($this->getTable(), 'tree', (int)$id, 'image');
            return $tree;
        }

        return array();
    }

    public function getTitleById($id = null)
    {
        $id = $id !== null ? $id : $this->treeId;

        $treeItem = $this->treeById($id);

        return $treeItem['title'];

    }

    public function treeByIdAllLangs($id = null)
    {
        $res = array();
        $id = $id !== null ? $id : $this->treeId;
        $langs = cms::get('langs')->listLangs();

        $j = $fields = '';
        $i = 0;

        foreach ($langs as $lang) {
            foreach (self::$_tree_fields as $f) {
                $t = 't' . (++$i);
                $j .= 'inner join ' . Engine_DataIO_mysql::_PHRASES_TBL . ' ' . $t . ' on (' . $t . '.table="' . $this->getTable() . '" and ' . $t . '.record="' . $f . '" and ' . $t . '.record_id=ct.id and ' . $t . '.lang = "' . $lang . '") ';
                $fields .= ', ' . $t . '.txt as `' . $f . '`';
            }

            $tree = Engine_Controller::get('db')->row('
					select ct.id, ct.parent' . $fields . ', ct.sort
					from `' . $this->getTable() . '` ct
					' . $j . '
					where ct.id = ' . (int)$id . '
				');

            $res[$lang] = $tree ? $tree : array();


        }
        return $res;
    }

    public function phraseAllLangs($alias)
    {
        $langs = cms::get('langs')->listLangs();
        $res = array();
        foreach ($langs as $lang) {

            $sql = 'select txt from ' . Engine_DataIO_mysql::_PHRASES_TBL . ' where
					`table` = "' . Engine_DataIO_mysql::_PHRASES_TBL . '" and
					`record` = "' . mres($alias) . '" and
					`lang` = "' . $lang . '"
				';
            $aliasItem = cms::get('db')->field($sql);


            $res[$lang] = $aliasItem ? $aliasItem : '';

        }


        return $res;


    }


    public function siteIdByAlias($alias)
    {
        $sql = 'select record_id from ' . Engine_DataIO_mysql::_PHRASES_TBL . ' where
				`table` = "cms_tree" and
				`record` = "link" and
				`lang` = "' . Engine_Controller::get('langs')->getLang() . '" and
				`txt` = "' . mres($alias) . '"
			';


        $id = Engine_Controller::get('db')->field($sql);
        return $id;
    }

    public function idByAlias($alias)
    {
        $id = Engine_Controller::get('db')->field('select record_id from ' . Engine_DataIO_mysql::_PHRASES_TBL . ' where
				`table` = "' . $this->getTable() . '" and
				`record` = "link" and
				`lang` = "' . Engine_Controller::get('langs')->getLang() . '" and
				`txt` = "' . mres($alias) . '"');
        return $id;
    }

    public function linkById($id, $lang = null, $ajaxUri = false)
    {


        $lang = $lang ? $lang : Engine_Controller::get('langs')->getLang();

        //  var_dump($lang);
        $parents = $this->getIdHistory($id);

        $pArr = array();
        if (!empty($parents)) {
            foreach ($parents as $pid) {
                $sql = 'select `txt` from ' . Engine_DataIO_mysql::_PHRASES_TBL . ' where
						`table` = "' . $this->getTable() . '" and
						`record` = "link" and
						`lang` = "' . $lang . '" and
						`record_id` = "' . (int)$pid . '"
					';

                $pArr[] = Engine_Controller::get('db')->field($sql);
            }
        }


        return '/' . $lang . '/'. ($ajaxUri?'ajax/':'') . implode('/', $pArr);
    }

    public function controllerLink($controller = 'textpage', $action = 'default') {

        if($treeId = cms::get('db')->field('SELECT `id` FROM `cms_tree` WHERE `controller`="'.$controller.'" AND `action`="'.$action.'"')) {

            return $this->linkById($treeId);
        }

        return false;
    }

    public function getMenu($menu_type)
    {
        $main_menu = array();
        $sql = "SELECT id FROM " . $this->getTable() . " WHERE menu_type='$menu_type'";
        if ($menu_id = Engine_Controller::get('db')->field($sql)) {
            $main_menu = $this->byParent($menu_id);
            foreach ($main_menu as $sub) {
                $submenu = $this->byParent($sub['id']);
                if (!empty($submenu)) {
                    $main_menu[$sub['id']]['submenu'] = $submenu;
                }
            }
        }
        return $main_menu;
    }

    public function byParent($p = null, $lang = null)
    {


        $p = $p !== null ? $p : $this->treeId;
        $l = $this->parseLang($lang);

        $j = $u = '';
        $i = 0;

        foreach (self::$_tree_fields as $f) {
            $t = 't' . (++$i);
            $u .= ', t' . $i . '.txt as `' . $f . '` ';
            $j .= 'inner join ' . Engine_DataIO_mysql::_PHRASES_TBL . ' ' . $t . ' on
            (' . $t . '.table="' . $this->getTable() . '"
            and ' . $t . '.record="' . $f . '"
            and ' . $t . '.record_id=ct.id
            and ' . $t . '.lang = "' . $l . '") ';
        }

        $u .= ', `fl`.`file` as `image`';
        $j .= 'LEFT JOIN `sys_files` as `fl` ON `ct`.`id`=`fl`.`record_id` AND `fl`.`table`="'.$this->getTable().'"';

        $tree = Engine_Controller::get('db')->rows(
            'select ct.* ' . $u . '
				from `' . $this->getTable() . '` ct
				' . $j . '
				where ct.parent = ' . (int)$p . '
				order by sort');
        return $tree;
    }


    public function save($id, $p)
    {

        $arr = array(
            'parent' => $p['parent']
        );

        if(strstr($p['controller'],':')) {
            list($ctrl, $act) = explode(':',$p['controller']);
            $arr['controller'] = $ctrl;
            $arr['action'] = $act;
        } else {
            $arr['controller'] = $p['controller'];
        }

        $id = Engine_Controller::get('db')->field('select id from `' . $this->getTable() . '` where id = ' . (int)$id);

        if ($id) {
            Engine_Controller::get('db')->update($this->getTable(), $arr, ' id = ' . (int)$id);
        } else {
            $arr['sort'] = Engine_Controller::get('db')->field('select max(sort) from ' . $this->getTable() . ' limit 1') + 1;
            $id = Engine_Controller::get('db')->insert($this->getTable(), $arr);
        }

        if(setIf_isset($img, $_FILES['treeImg'])) {
            $upl = new CMS_Uploads();
            if($file = $upl->saveFile($img, null, 'tree', array('image/jpeg','image/png'))) {

                if($file_id = $file->saveRecord($this->getTable(), $id, 'image', true)) {
                    $imager = new Imager;
                    $imager->image = $file->filePath;
                    $imager->type = $file->type;
                    if($info = cms::getInfo($arr['controller'])) {
                        if(setIf_isset($dims, $info['thumb'])) {
                            $imager->thumb($file->filePath, $dims['w'], $dims['h']);
                        }
                    }
                }
                $file->reset();

            }
        }

        $records = self::$_tree_fields;


        foreach ($p as $lang => $_) {
            if (is_array($_)) {
                foreach ($records as $record) {
                    Engine_Controller::get('db')->saveText($_[$record], $record, $id, $this->getTable(), $lang);
                }
            }
        }

        return $id;
    }

    public function delete($id = false)
    {
        if($id) {
            Engine_Controller::get('db')->delete($this->getTable(), 'id=' . (int)$id);
            Engine_Controller::get('db')->delete(Engine_DataIO_mysql::_PHRASES_TBL, 'table="' . $this->getTable() . '" AND record_id=' . (int)$id);

            $sql = "SELECT id FROM " . $this->getTable() . " WHERE parent=" . (int)$id;
            $childArr = Engine_Controller::get('db')->rows($sql);

            foreach ($childArr as $child_id) {
                Engine_Controller::get('db')->delete($this->getTable(), 'id=' . (int)$child_id);
                Engine_Controller::get('db')->delete(Engine_DataIO_mysql::_PHRASES_TBL, 'table="' . $this->getTable() . '" AND record_id=' . (int)$child_id);
            }
        }
    }

    public function saveSorting($ids)
    {
        $sorting = 1;
        foreach ($ids as $id) {
            Engine_Controller::get('db')->update($this->getTable(), array('sort' => $sorting++), ' id = ' . (int)$id);
        }
    }

    function getIdHistory($id)
    {

        $parents = array();
        while ($id != 0) {
            if ($id != 0) array_push($parents, $id);
            $sql = "SELECT t.parent FROM " . $this->getTable() . " t
                                        inner join " . $this->getTable() . " p on p.id=t.parent and p.use_link=1
                                        WHERE t.id=" . $id;


            $id = Engine_Controller::get('db')->field($sql, false);
        }
        $parents_ = array_reverse($parents);
        // var_dump($parents_);
        return $parents_;
    }


    function getChilds($id = null)
    {

        if ($id) {
            $this->childs = array($id);
            $sql = '
				SELECT
					ct1.id, ct2.id, ct3.id, ct4.id, ct5.id, ct6.id
				FROM
					cms_tree ct1
				left join cms_tree ct2 on ct2.parent = ct1.id
				left join cms_tree ct3 on ct3.parent = ct2.id
				left join cms_tree ct4 on ct4.parent = ct3.id
				left join cms_tree ct5 on ct5.parent = ct4.id
				left join cms_tree ct6 on ct6.parent = ct5.id
				where ct1.parent = ' . $id . '
			';
            $res = mysql_query($sql);
            while ($c = mysql_fetch_row($res)) {

                foreach ($c as $id) {
                    if ($id && !in_array($id, $this->childs)) {
                        $this->childs[] = $id;
                    }
                }
            }

            return $this->childs;
        }
    }

    function makeMain($parent, $child)
    {
        $sql = "UPDATE " . $this->getTable() . " SET default_child='$child' WHERE id='$parent'";
        mysql_query($sql);
    }

}