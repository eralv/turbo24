<?php

class CMS_Mailing
{
	
	public $from_email = 'info@era.lv';
	public $from_name = 'Turbo24';
	
	
	public $to = ' ej@era.lv';
	public $subject = '';
	public $body = '';
	public $error_msg = '';

    public $attachment = array();

	public function sendMail()
	{
        $status = false;

		include_once("phpmailer/class.phpmailer.php");
		
		$mail = new PHPMailer();
		$relatedHTML = '';

		$mail->IsSMTP();
		/*$mail->SMTPAuth   = false;
		$mail->Port       = 25;
		$mail->Host       = "localhost";*/
		$mail->SetFrom($this->from_email, $this->from_name);
		$mail->CharSet='utf-8';
		$mail->Subject = $this->subject;
		$mail->MsgHTML($this->body);
		//$mail->AddAddress($this->to); TODO: Revert temp email receivers
        $mail->AddAddress('ej@era.lv');
        $mail->AddAddress('eb@era.lv');
		$message = '';

        if(!empty($this->attachment)){
            foreach($this->attachment as $attachment){
                $mail->AddAttachment($attachment);
            }
        }
		
	
		try {
			$status = (bool) $mail->Send();
			sleep(2);	
		} 
		catch (Exception $e) {
			$e->getMessage();
		}

		$this->error_msg = $mail->ErrorInfo;

		return $status;


	}



}