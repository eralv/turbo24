<?php

class CMS_Group extends Engine_Model {

    const TBL = 'cms_groups';

    public $self_fields = array(
        'id', 'tour_id', 'op_sys_id',
        'from_date', 'to_date',
        'seats', 'price', 'new_price',
        'guide', 'guide_lang'
    );

    public function enrich($id, $item) {
        return $item;
    }

    public function getItem($filter, $language = null) {

        $id = is_numeric($filter) ? $filter : 0;

        $fields = $this->getFields('gr');
        $joins = array();

        list($txt_fields,$txt_joins) = $this->joinTexts($this->getTable(), 'gr', '`gr`.`'.reset($this->self_fields).'`', $language);

        $fields = array_merge($fields, $txt_fields);
        $joins = array_merge($joins, $txt_joins);

        $sql = 'SELECT '. $this->parseFields($fields) .'
        FROM `'. $this->getTable() .'` gr
        '. (!empty($joins)?implode("\n",$joins):'') .'
        WHERE gr.`id`='. (int)$id;

        if($item = cms::get('db')->row($sql, (new self))) {

            return $this->enrich($item->id, $item);

        }

        return false;
    }

    public function getList($filter, $language = null) {

        $alias = 'gr';
        $fields = $this->getFields($alias);
        $joins = array();

        list($txt_fields,$txt_joins) = $this->joinTexts($this->getTable(), $alias, '`'.$alias.'`.`'.reset($this->self_fields).'`', $language);

        $fields = array_merge($fields, $txt_fields);
        $joins = array_merge($joins, $txt_joins);

        return cms::get('db')->rows(
            'SELECT '. $this->parseFields($fields) .'
            FROM `'.  $this->getTable() .'` as `'. $alias .'`
            '. (!empty($joins)?implode("\n",$joins):'') .'
            WHERE `'.$alias.'`.`tour_id`='.(int)$filter.'
            ORDER BY `'.$alias.'`.`from_date`'
            , (new self), function($key, $item) {
                /** @var CMS_Group $item */
                return $item->enrich($key, $item);
            });
    }

    public function getCheapest($tour_id) {

        $alias = 'gr';
        $fields = $this->getFields($alias);
        $joins = array();

        list($txt_fields,$txt_joins) = $this->joinTexts($this->getTable(), $alias, '`'.$alias.'`.`'.reset($this->self_fields).'`', $language);

        $fields = array_merge($fields, $txt_fields);
        $joins = array_merge($joins, $txt_joins);

        return cms::get('db')->row('SELECT '.$this->parseFields($fields).'
        FROM '.$this->getTable().' `'.$alias.'`
        WHERE `'.$alias.'`.`tour_id`='.(int)$tour_id.'
        ORDER BY `'.$alias.'`.`new_price` ASC, `'.$alias.'`.`price` ASC',(new self));
    }

    /**
     * @param array $post
     * @param bool|CMS_Group|array $item
     * @return bool|int
     */
    public function saveItem(array $post, $item = false) {
        $item_id = $this->parseId($item);

        $data = array_intersect_key($post, array_swap_vk($this->self_fields));

        if(setIf_isset($from, $data['from_date']) && !is_int($from)) {
            $from = strtotime($from);
            if(!$from) $from = time();
            $data['from_date'] = $from;
        }
        if(setIf_isset($to, $data['to_date']) && !is_int($to)) {
            $to = strtotime($to);
            if(!$to) $to = time();
            $data['to_date'] = $to;
        }

        if($item_id) {
            cms::get('db')->update($this->getTable(), $data, ' id='.(int)$item_id);
        } else {
            $item_id = cms::get('db')->insert($this->getTable(), $data, false);
        }

        return $item_id;

    }

} 