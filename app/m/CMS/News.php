<?php

	class CMS_News extends Engine_Model {

		const TBL = 'cms_news';
		const SUPERUSER = 4;

        public $dir = 'news';

        public $self_fields = array(

            'id', 'image', 'date'
        );

        public $filter_fields = array(

        );
	
        public $text_fields = array(
            'title', 'news'
        );

        public function getImage($item = null) {
            $id = $this->parseId($item);

            $upl = new CMS_Uploads();
            if($image = $upl->getPrimary($this->getTable(), $this->dir, $id, 'image')) {
                return CMS_Uploads::getPath($image['file'], $this->dir);
            }

            return false;

        }

        public function getItem($filter = array(), $language = null) {

	        $where = array();

            if(is_numeric($filter)) {
                $where[] = 'bs.id='.$filter;
            } elseif(is_array($filter)) {
                foreach ($filter as $key=>$value){
                    if(in_array($key,$this->self_fields)) {
                        $where[] = 'bs.'.$key.'="'.mres($value).'"';
                    }
                }
            }

            $fields = $this->getFields('bs');
            $joins = array();

            list($txt_fields,$txt_joins) = $this->joinTexts($this->getTable(), 'bs', '`bs`.`'.reset($this->self_fields).'`', $language);

            $fields = array_merge($fields, $txt_fields);
            $joins = array_merge($joins, $txt_joins);

            $sql = 'SELECT '.$this->parseFields($fields).' FROM `'. $this->getTable() .'` as `bs` '.
		    implode(' ',$joins).' '. (!empty($where)?('WHERE '. implode(' AND ',$where)):'');// die();
            return cms::get('db')->row($sql,(new self));

        }
	

        public function getList($filters = array(), $language = null, $limit = NULL, $order = NULL) {

            $where = array();
            if(is_array($filters)) {
                foreach ($filters as $key=>$value){
                    if(in_array($key,$this->self_fields)) {
                        $where[] = $key.'="'.mres($value).'"';
                    }
                }
            }

            $alias = 'ord';
            $joins = array();

            list($txt_fields,$txt_joins) = $this->joinTexts($this->getTable(), $alias, '`'.$alias.'`.`'.reset($this->self_fields).'`', $language);

            $fields = array_merge($this->getFields($alias), $txt_fields);
            $joins = array_merge($joins, $txt_joins);

            return cms::get('db')->rows(
                'SELECT '. $this->parseFields($fields) .'
                FROM `'. self::TBL .'` as `'. $alias .'` '. implode(' ',$joins).
			(isset($order)?' ORDER BY '.$order['order_by'].
			' '.$order['sort']:'').
			(isset($limit)?' LIMIT '.$limit['start_page'].
			' , '.$limit['per_page']:''),(new self));

        }
	public function get_Count($filters = null){

        $where = array();

        if(is_array($filters)) {

            foreach ($filters as $key=>$value){
                if(in_array($key,$this->self_fields)) {
                    $where[] = $key.'="'.mres($value).'"';
                }
            }
        }

	    return cms::get('db')->field('SELECT count(*) FROM `'. self::TBL .'`'.(!empty($where)?('WHERE '. implode(' AND ',$where)):''));

	}

	public function saveItem(array $post, $item = false) {
	    $item_id = $this->parseId($item);

        $data = array();

        if(!$item_id) {
            $data['date'] = time();
        }

        if(isset($post['image'])) {
            $data['image'] = (int)$post['image'];
        }

        if(!empty($data)) {
            if($item_id) {
                cms::get('db')->update($this->getTable(), $data, ' id='.(int)$item_id);
            } else {
                $item_id = cms::get('db')->insert($this->getTable(), $data);
            }
        }

        if($item_id) {
            if(isset($post['title']))
                $this->saveTexts($post['title'], 'title', $item_id);

            if(isset($post['news']))
                $this->saveTexts($post['news'], 'news', $item_id);
        }

	    return $item_id;
	}

        public function deleteItem($id) {
            $this->delete((int)$id);
	    
        }

        public function calcPrice($distance) {

            return number_format(($distance * 1000) * 0.075, 2, '.', '');

        }

        public function orderNum($id) {

            $num_id = strlen($id) > 2 ? implode('-', str_split($id, 2)) : '00-'.($id<10?'0':'').$id;

            return 'TR-'. $num_id;
        }


        public function generateInvoicePdf($order_id, $rewrite = false, $output = 'F', $ppr = false){

            $order = $this->getItem($order_id);
            $price = $this->calcPrice($order['distance']);

            $invoiceRootPath = $_SERVER['DOCUMENT_ROOT'].'/storage/orders/';
            $invoicePath = $invoiceRootPath . 'order_'.$order_id.'.pdf';

            if($rewrite) @unlink($invoicePath);

            if(!file_exists($invoicePath)){

                if(!file_exists(dirname($invoicePath)))
                    mkdir(dirname($invoicePath),0777);


                require_once('library/tcpdf/tcpdf.php');


                $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
                $pdf->SetHeaderMargin(0);
                $pdf->SetFooterMargin(0);
                $pdf->setPrintFooter(false);
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

                $bill_bg = 'library/tcpdf/images/bill_bg.jpg';
                $orange_bg = 'library/tcpdf/images/orange_bg.jpg';

                $pdf->AddPage();



                $x = 12;
                $y = round($pdf->getPageHeight()-660);

                $pdf->SetFont('dejavusans', '', 10, '', true);


                $html = '<table border="0" cellspacing="0" cellpadding="4" width="618">';
                if($ppr)
                    $html .= '	<tr><td align="right"><b>PREČU PAVADZĪMES RĒĶINS NR. '.$order['invoice_nr'].'</b></td></tr>';
                else
                    $html .= '	<tr><td align="right"><b>AVANSA RĒĶINS: '.$order['order_num'].'</b></td></tr>';

                $html .= '	<tr><td align="right">DATUMS: '.date('d.m.Y', $order['date']).'</td></tr>';
                $html .= '</table>';

                $pdf->writeHTML($html, false, false, true, 0);

                // ------------------------ TABLE 1

                $bill_bg_image = $_SERVER['DOCUMENT_ROOT'].'/'.$bill_bg;
                list($w, $h) = $pdf->getWidthHeight($bill_bg_image,11.6);
                $y = $pdf->getY()+10;
                //$pdf->Image($bill_bg_image, $x, $y, $w, $h+30, '', '', '', false, 300, '', false, false, 0);

                $y = $pdf->getY()+13;
                $pdf->setY($y);

                $x = 10;
                $pdf->setX($x);

                $html = '
					<table  border="0" cellspacing="0" cellpadding="6" width="830">


						<tr>
							<td width="200" bgcolor="#f2f2f2">Piegādātājs</td>
							<td bgcolor="#f2f2f2"><b>SIA Turbo24</b></td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2">Reģ.Nr.</td>
							<td bgcolor="#f2f2f2">LV545626155616</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2">Adrese</td>
							<td bgcolor="#f2f2f2">Rīgas iela 10, Rīga LV-1000</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2">Konts</td>
							<td bgcolor="#f2f2f2">LV80HABA6800543367123</td>
						</tr>
						<tr>
							<td colspan="2" bgcolor="#f2f2f2">&nbsp;</td>
						</tr>';

                /*$requisites = json_decode($order['requisites'],true);
                if(sizeof($requisites)){
                    foreach($requisites as $req_row){
                        $reqArr = explode(':', strip_tags($req_row), 2);

                        $html .= '<tr>
											<td bgcolor="#f2f2f2">'.$reqArr[0].'</td>
											<td bgcolor="#f2f2f2">'.$reqArr[1].'</td>
										</tr>';

                    }
                }*/

                $html .= '<tr>
				    <td bgcolor="#f2f2f2">Vārds, uzvārds</td>
					<td bgcolor="#f2f2f2">'.$order['sender_name'].' '.$order['sender_surname'].'</td>
				</tr>';
                $html .= '<tr>
				    <td bgcolor="#f2f2f2">Perosnas kods</td>
					<td bgcolor="#f2f2f2">'.$order['sender_pers_code'].'</td>
				</tr>';
                $html .= '<tr>
				    <td bgcolor="#f2f2f2">Tālrunis</td>
					<td bgcolor="#f2f2f2">'.$order['sender_phone'].'</td>
				</tr>';
                $html .= '<tr>
				    <td bgcolor="#f2f2f2">E-pasts</td>
					<td bgcolor="#f2f2f2">'.$order['sender_email'].'</td>
				</tr>';

                if($order['user_id'] && $order['juridic'] && $user = cms::get('users')->getItem($order['user_id'])) {
                    if($user['juridic']) {

                        $fields = array(
                            'company_name'=>'Uzņēmuma nosaukums',
                            'reg_number'=>'Reģistrācijas numurs',
                            'pvn_number'=>'PVN maksātāja numurs',
                            'jur_address'=>'Juridiskā adrese',
                            'fact_address'=>'Faktiskā adrese',
                            'iban_number'=>'Konta numurs',
                        );

                        foreach($fields as $fld=>$label) {
                            if(setIf_isset($val, $user[$fld]) && !empty($val)) {
                                $html .= '<tr>
                                <td bgcolor="#f2f2f2">'.$label.'</td>
                                <td bgcolor="#f2f2f2">'.$val.'</td>
                            </tr>';
                            }
                        }

                    }
                }


                $html .= '</table>
						';

                $pdf->writeHTML($html, false, false, true, 0);



                // ------------------------ TABLE 2

                $pdf->setY($pdf->getY() + 15);

                $orange_bg_image = $_SERVER['DOCUMENT_ROOT'].'/'.$orange_bg;
                list($w, $h) = $pdf->getWidthHeight($orange_bg_image,1.6);
                $y = $pdf->getY();




                $pdf->Image($orange_bg_image, $x, $y, $w-55, $h, '', '', '', false, 300, '', false, false, 0);

                $html = '
					<table  border="0" cellspacing="0" cellpadding="4" width="625">
						<tr>
							<th style="color:white;width:30px">Nr.</th>
							<th style="color:white;width:292px">Nosaukums</th>
							<th style="color:white;width:50px">Mērv.</th>
							<th style="color:white;width:60px">Daudz.</th>
							<th style="color:white;width:80px">Cena, EUR</th>
							<th style="color:white;width:100px">Summa, EUR</th>
						</tr>';

                $total_price_w_pvn = $price;
                $pvn = array(
                    '12'=>0,
                    '21'=>0
                );

                $pvn['21'] += (float)$price * 0.21;

                $html .= '
							<tr>
								<td style="text-align:center;">1</td>
								<td>Kurjera pakalpojumi<br />(No '.$order['location_from'].' uz '.$order['location_to'].')</td>
								<td style="border-right:1px solid #EF7D06;">km</td>
								<td style="text-align:right;border-right:1px solid #EF7D06;">'.$order['distance'].'</td>
								<td style="text-align:right;border-right:1px solid #EF7D06;">' . $price .'</td>
								<td style="text-align:right;">' . $price .'</td>
							</tr>
						';

                /*if($order['delivery_price'] > 0)
                {
                    $delivery = json_decode($order['delivery'],true);
                    //var_dump($delivery);exit;
                    $html .= '
						<tr>
							<td style="text-align:center;">' . $cnt++ . '</td><td>';

                    foreach($delivery as $i=>$del) {
                        $delz = explode(':',$del,2);
                        switch($i) {
                            case 'type':
                                $html .= $delz[0].': '. ($delz[1]=='station'?'POST24':'Latvijas Pasts');
                                break;
                            case 'station':
                                $delz[1] = str_replace(': ',', ',$delz[1]);
                            case 'mobile':
                            case 'address':
                                $html .= '<br />'.$delz[0].': '.$delz[1].'';
                                break;
                        }
                    }

                    $html .= '</td><td style="border-right:1px solid #EF7D06;"></td>
							<td style="border-right:1px solid #EF7D06;"></td>
							<td style="border-right:1px solid #EF7D06;"></td>
							<td style="text-align:right;">' . number_format($order['delivery_price'], 2, '.', '').'</td>
						</tr>
					';
                    $total_price_w_pvn += $order['delivery_price'];
                    $pvn['21'] += $order['delivery_price'] * 0.21;
                }*/

                ///$pvn = $total_price_w_pvn * 0.12;
                $total_price_wo_pvn = $total_price_w_pvn - ((float)$pvn['12'] + (float)$pvn['21']);


                $html .= '
					<tr>
						<td colspan="5" style="border-top:1px solid #EF7D06">Kopsumma bez PVN</td>
						<td style="border-top:1px solid #EF7D06;text-align:right;">'.number_format($total_price_wo_pvn, 2, '.', '').'</td>
					</tr>';

                $sx = 0;
                foreach($pvn as $nod=>$sum) {
                    if($sum!=0) {
                        $html .= '<tr>
								<td colspan="5">PVN ('.$nod.'%)</td>
								<td style="text-align:right;">'.number_format($sum, 2, '.', '').'</td>
							</tr>';
                        $sx++;
                    }
                }

                /*if($order['giftcard_summ_covered']>0) {
                    $total_price_w_pvn -= $order['giftcard_summ_covered'];
                    $html .= '<tr>
								<td colspan="5">Atlaide</td>
								<td style="text-align:right;">-'.number_format($order['giftcard_summ_covered'], 2, '.', '').'</td>
							</tr>';
                }*/

                $html .= '<tr>
						<td colspan="5" style="border-top:1px solid #EF7D06;">Summa ar PVN</td>
						<td style="text-align:right;border-top:1px solid #EF7D06;">
						    <b>'.number_format($total_price_w_pvn, 2, '.', '').' EUR</b>
						</td>
					</tr>
				';


                $html .= '
					<tr>
						<td colspan="6">Summa vārdiem: <b>'.ucfirst(trim(amountToStrint(number_format($total_price_w_pvn, 2, ',', ''), 'eiro', 'centi'))).'</b></td>
					</tr>
				';

                $html .= '</table>';


                $x = 12;
                $pdf->setX($x);

                $x = $pdf->getX();

                $pdf->writeHTML($html, false, false, true, 0);

                $z = $pdf->Output($invoicePath, $output);
            }


            return $invoicePath;


        }

	}
