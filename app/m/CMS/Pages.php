<?php

class CMS_Pages
{
	public $_active = 0;
	public $_letter = 'A';
	public $_per_page = 20;
	public $_pages = 0;	

	public function __construct($a=0, $pp=20, $p=0)
	{
		
		$this->_letter = isset($_GET['1letter']) ? $_GET['1letter'] : $this->_letter;
		
		if($a)
		{
			$this->_active = (int)$a;
		}
		if($pp)
		{
			$this->_per_page = (int)$pp;
		}
		if($p)
		{
			$this->_pages = (int)$p;
		}
	}



	public function html()
	{
		


		$max = 30;

		$html = '';

		$lastPage = ceil($this->_pages / $this->_per_page)-1;
		if( $this->_pages > $this->_per_page ) {
			
				
			
			$html.= '<ul class="nav">';

			$html.= '<li class="first"><a href="' . Engine_Controller::get('uri')->changeRoute('page',0) . '">first</a></li>';		
			
			$html.= ( 
				$this->_active>0 ? '<li class="prev"><a href="' . Engine_Controller::get('uri')->changeRoute('page',$this->_active-1) . '">prev</a></li>' : '<li class="prev"><a href="' . Engine_Controller::get('uri')->changeRoute('page',$this->_active) . '">prev</a></li>' 
			);
			
			$_min = ( ceil($max/2)-$this->_active<=0 ? $this->_active-ceil($max/2) : 0 );
			$_max = ( ceil($max/2)-$this->_active<0 ? $this->_active-ceil($max/2)+$max : $max );
			$_max = ( $_max<$lastPage ? $_max : $lastPage+1 );
			

			
			for(
				$i = $_min; 
				$i < $_max;
				$i++
			) {
				$html.= '<li '.($i==$this->_active?' class="active" ':'').'><a href="' . Engine_Controller::get('uri')->changeRoute('page',$i) . '">'.($i+1).'</a></li>';
			}


			$html.= ( 
				$this->_active<$lastPage ? 
				'<li class="next"><a href="' . Engine_Controller::get('uri')->changeRoute('page',$this->_active+1) . '">next</a></li>' : 
				'<li class="next"><a href="' . Engine_Controller::get('uri')->changeRoute('page',$lastPage) . '">next</a></li>' 
			) ;
			
			$html.= '<li class="last"><a href="' . Engine_Controller::get('uri')->changeRoute('page',$lastPage) . '">... '.($lastPage+1).'</a></li>';			
			
			$html.= '</ul>';
		}
		return $html;
	}
	
	public function letters($tbl, $field)
	{
		$letters = Registry::get('db')->_keys('SELECT DISTINCT LEFT( `' . mres($field) . '`, 1 ) FROM `' . mres($tbl) . '` WHERE `deleted`=0 order by `' . $field . '`');
		$html = '';
		
		if( count($letters) ) {
			$html.= '<ul class="pages">';
			
			foreach($letters as $l)
			{
				$html.= '<li><a '.($l==$this->_letter?' class="act" ':'').' href="' . changeURL(array('1letter'=>$l)) . '">' . strtoupper($l) . '</a></li>';
			}
			
			$html .= '</ul>';
		}
		return $html;
	}

}