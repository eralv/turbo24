<?php

	class CMS_Orders extends Engine_Model {

		const TBL = 'cms_orders';
		const SUPERUSER = 4;

        public $self_fields = array(

            'id', 'order_num', 'status', 'sender_name', 'sender_surname', 'sender_pers_code', 'sender_position', 'sender_phone', 'sender_email',
            'address_name', 'address_surname','address_phone', 'address_email', 'location_from', 'insurance_info', 'duration',
            'location_to', 'distance', 'adds', 'operator_id', 'driver_id', 'user_id', 'date', 'status', 'juridic', 'rate',
            'company_name','jur_address','fact_address','reg_number','pvn_number','iban_number', 'insurance', 'task_date',
	        'showed_in_list', 'comments',
            'bank_name', 'bank_swift'

        );

        public $validate_fields = array(
            'order_confirm'=>array('required'),
            'sender_name' => array('trim', 'required'/*,
                'only_letters'=>'/^([a-zа-яā-žõŗ ]+)$/iu',
                'length'=>'/^(.{3,})$/'*/
            ),
            'sender_surname'=>array('trim', 'required'/*,
                'only_letters'=>'/^([a-zа-яā-žõŗ ]+)$/iu',
                'length'=>'/^(.{3,})$/'*/
            ),
            'sender_pers_code'=>array('trim', 'required'/*,
                'person_id:lv'=>'/^(\d{5,6}\-\d{5})$/'*/
            ),
            'sender_phone'=>array('trim', 'required',
                'valid'=>'/^\+?[\d]+$/'
            ),
            'sender_email'=>array('trim', 'required',
                'valid'=>'/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i'
            ),
            'address_name'=>array('trim', 'required'),
            'address_surname'=>array('trim', 'required'),
            'address_phone'=>array('trim', 'required',
                'valid'=>'/^\+?[\d]+$/'
            ),
            'address_email'=>array('trim', 'required',
                'valid'=>'/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i'
            ),
            'location_from'=>array('trim', 'required'),
            'location_to'=>array('trim', 'required'),
            'distance'=>array('required')/*,
            'duration'=>array('required')*/
        );

        public $filter_fields = array(

            'sender_name', 'sender_surname', 'sender_pers_code', 'sender_phone', 'sender_position', 'sender_email',
            'address_name', 'address_surname','address_phone', 'address_email', 'location_from',
            'location_to', 'distance', 'driver_id', 'status',
            'company_name','jur_address','fact_address','reg_number','pvn_number','iban_number',
            'insurance', 'task_date', 'insurance_info', 'comments',
            'bank_name', 'bank_swift'

        );

        public function createFilter($filter, &$where) {

            if(!is_array($where))
                $where = array();

            $wh = array();
            if(is_array($filter)) {

                foreach ($filter as $key=>$value){
                    if(strstr($key,'|')) {
                        $keys = explode('|',$key);
                    } else {
                        $keys = array($key);
                    }
                    $val = '';
                    $i = 0;
                    foreach($keys as $key) {
                        if(in_array($key,$this->self_fields) && strlen($value)) {
                            if($i++) $val .= ' OR ';
                            if(in_array($key, array('date','task_date'))) {
                                $val .= 'FROM_UNIXTIME('.$key.',\'%d-%m-%Y\')="'.mres($value).'"';
                            } elseif(in_array($key, array('sender_name','sender_surname','company_name'))) {
                                $val .= $key.' LIKE "%'.mres($value).'%"';
                            } else {
                                $val .= $key.'="'.mres($value).'"';
                            }
                        }
                    }
                    if(!empty($val))
                        $wh[] = '('.$val.')';
                }
            } elseif(is_numeric($filter)) {
                $wh[] = 'id='.(int)$filter;
            }

            $where = array_merge($where, $wh);

        }


        public function getItem($filter = array(), $language = null) {

	        $where = array();

            $this->createFilter($filter, $where);

            $sql = 'SELECT * FROM `'. self::TBL .'` '. (!empty($where)?('WHERE '. implode(' AND ',$where)):'');
            return cms::get('db')->row($sql);

        }
	

        public function getList($filter = array(), $language = null, $limit = NULL, $order = null) {

            $alias = 'ord';
            $fields = $this->getFields($alias);

            $where = array();
            $this->createFilter($filter, $where);
            $where[] = 'status<>0';

            return cms::get('db')->rows(
                'SELECT '. $this->parseFields($fields) .'
                FROM `'. self::TBL .'` as `'. $alias .'`
                '. (!empty($where)?('WHERE '. implode(' AND ',$where)):'') .'
                '.(isset($order)?' ORDER BY '.$order['order_by'].
			' '.$order['sort']:'').
			(isset($limit)?' LIMIT '.$limit['start_page'].' , '.$limit['per_page']:''));
	    
        }
	public function get_Count($filter = array()){

        $this->createFilter($filter, $where);
        $where[] = 'status<>0';
	    return cms::get('db')->field('SELECT count(*) FROM `'. self::TBL .'`'.(!empty($where)?('WHERE '. implode(' AND ',$where)):''));

	}

	    public function saveItem($post, $item = false) {
            $id = $this->parseId($item);
            $fields = $this->filteredFields($post);
            $user = cms::get('users')->gi();

	        if(isset($post['driver_id']))
                $fields['driver_id'] = $post['driver_id'];
            if(isset($post['operator_id']))
                $fields['operator_id'] = $post['operator_id'];
            if(isset($post['status']))
                $fields['status'] = $post['status'];
            if(isset($post['showed_in_list']))
                $fields['showed_in_list'] = $post['showed_in_list'];

            if($post['insurance']) {
                $this->validate_fields['insurance_info'] = array('trim','required');
            }

            if($post['insurance']) {
                $this->validate_fields['insurance_info'] = array('trim','required');
            }

            if(($user && $user['juridic']) || (isset($post['juridic']) && $post['juridic'])) {
                if(isset($this->validate_fields['sender_pers_code'])) {
                    unset($this->validate_fields['sender_pers_code']);
                }
                $this->validate_fields += array(
                    'company_name' => array('trim','required'),
                    'jur_address' => array('trim','required'),
                    'fact_address' => array('trim','required'),
                    'reg_number' => array('trim','required'),
                    'pvn_number' => array('trim','required'),
                    'iban_number' => array('trim','required'),
                    'bank_name' => array('trim','required'),
                    'bank_swift' => array('trim','required')
                );

                if(!(isset($post['juridic']) && $post['juridic'])) {
                    $this->validate_fields += array('sender_position' => array('trim','required'));
                }
            }

            if($this->validate)
                $this->validate(array_merge($fields, $post),true,true);

            if((!empty($post['location_from']) && !empty($post['location_from'])) &&
                (!preg_match('/([0-9]+)/', $post['location_from']) || !preg_match('/([0-9]+)/', $post['location_to'])) &&
                !isset($post['street_nr_ok'])
            ) {
                $this->saveErrors[] = 'address_nr';
            }

            if(empty($this->saveErrors)) {

                if(!$id) {
                    $rate = new CMS_Rates();
                    $fields += array(
                        'date' => time(),
                        'user_id' => $user ? $user['id'] : 0,
                        'juridic' => $user ? $user['juridic'] : (int)(isset($post['juridic']) && $post['juridic']),
                        'rate' => $rate->getRate($user),
                        'status' => 0,
                        'distance' => $this->getDistance($fields['location_from'], $fields['location_to']),
                        'duration' => $this->getDuration($fields['location_from'], $fields['location_to'])
                    );
                    if(setIf_isset($task_date,$post['task_date']) && !empty($task_date)) {
                        $time = strtotime($post['task_date']);
                        $fields['task_date'] = $time ? $time : time();
                    }

		            $id = cms::get('db')->insert($this->getTable(), $fields);
                    $order_num = $this->orderNum($id);
                    cms::get('db')->update(self::TBL, array('order_num'=>$order_num), ' id='.$id);
                } else {
                    cms::get('db')->update($this->getTable(), $fields, ' id='.$id);
                }

                return $id;
            }


            return false;

        }

        public function deleteItem($id) {
            cms::get('db')->delete(self::TBL, ' id='.(int)$id);
        }

        public function getDistance($from, $to) {
            $data = 'http://maps.googleapis.com/maps/api/directions/json?origin='.urlencode($from).'&destination='.urlencode($to).'&sensor=false';
            $result = json_decode(file_get_contents($data));
            return $result->routes[0]->legs[0]->distance->value;
        }

        public function getDuration($from, $to) {
            $data = 'http://maps.googleapis.com/maps/api/directions/json?origin='.urlencode($from).'&destination='.urlencode($to).'&sensor=false';
            $result = json_decode(file_get_contents($data));
            return number_format($result->routes[0]->legs[0]->duration->value / 60, 2, '.', '');
        }

        public static function calcPrice($distance, $rate, $duration = 0) {

            if(!((int)($rate * 100))) {
                $rates = new CMS_Rates();
                $rate = $rates->getRate();
            }

            $price = ((($distance / 1000) * $rate) + ($duration * cms::get('cfg')->read('default_rate_minute'))) + cms::get('cfg')->read('default_rate_add');

            return number_format($price, 2, '.', '');

        }

        public function orderNum($id) {
            $num_id = 'TR-';
            if(strlen($id) > 2) {
                $num_id .= implode('-', str_split((strlen($id)%2?'0':'').$id,2));
            } else {
                $num_id .= '00-'.($id<10?'0':'').$id;
            }

            return $num_id;
        }


        public function generateBillPdf($order_id, $rewrite = false, $output = 'F'){

            $order = $this->getItem($order_id);
            $price = $this->calcPrice($order['distance'], $order['rate'], $order['duration']);

            $invoiceRootPath = _ROOT .'/storage/orders/';
            $invoicePath = $invoiceRootPath . 'order_'.$order_id.'.pdf';

            if($rewrite) @unlink($invoicePath);

            if(!file_exists($invoicePath)){

                if(!file_exists(dirname($invoicePath)))
                    mkdir(dirname($invoicePath),0775);

                require_once('library/tcpdf/tcpdf.php');
                $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


                //$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
                $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
                $pdf->SetHeaderMargin(0);
                $pdf->SetFooterMargin(0);
                $pdf->setPrintFooter(false);
                $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

                $bill_bg = 'library/tcpdf/images/bill_bg.jpg';
                $orange_bg = 'library/tcpdf/images/orange_bg.jpg';

                $pdf->AddPage();



                $x = 12;
                $y = round($pdf->getPageHeight()-660);

                $pdf->SetFont('dejavusans', '', 10, '', true);


                $html = '<table border="0" cellspacing="0" cellpadding="4" width="618">';
                $html .= '	<tr><td align="right"><b>RĒĶINS NR. '.$order['order_num'].'</b></td></tr>';

                $html .= '	<tr><td align="right">DATUMS: '.date('d.m.Y', $order['date']).'</td></tr>';
                $html .= '</table>';

                $pdf->writeHTML($html, false, false, true, 0);

                // ------------------------ TABLE 1

                $bill_bg_image = _ROOT .'/'. $bill_bg;
                list($w, $h) = $pdf->getWidthHeight($bill_bg_image,11.6);
                $y = $pdf->getY()+10;
                //$pdf->Image($bill_bg_image, $x, $y, $w, $h+30, '', '', '', false, 300, '', false, false, 0);

                $y = $pdf->getY()+13;
                $pdf->setY($y);

                $x = 10;
                $pdf->setX($x);

                $html = '
					<table  border="0" cellspacing="0" cellpadding="3" width="830">


						<tr>
							<td width="200" bgcolor="#f2f2f2">Piegādātājs</td>
							<td bgcolor="#f2f2f2"><b>SIA Turbo24</b></td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2">Reģ.Nr.</td>
							<td bgcolor="#f2f2f2">LV545626155616</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2">Adrese</td>
							<td bgcolor="#f2f2f2">Rīgas iela 10, Rīga LV-1000</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2">Banka</td>
							<td bgcolor="#f2f2f2">A/S Swedbank</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2">Bankas kods</td>
							<td bgcolor="#f2f2f2">LV80HABA</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2">Konts</td>
							<td bgcolor="#f2f2f2">LV80HABA6800543367123</td>
						</tr>
						<tr>
							<td colspan="2" bgcolor="#f2f2f2">&nbsp;</td>
						</tr>';


                if($order['juridic']) {

                    $fields = array(
                        'company_name'=>'Uzņēmuma nosaukums',
                        'reg_number'=>'Reģistrācijas numurs',
                        'pvn_number'=>'PVN maksātāja numurs',
                        'jur_address'=>'Juridiskā adrese',
                        'fact_address'=>'Faktiskā adrese',
                        'bank_name'=>'Banka',
                        'bank_swift'=>'Bankas kods',
                        'iban_number'=>'Konta numurs'
                    );

                    foreach($fields as $fld=>$label) {
                        if(setIf_isset($val, $order[$fld])) {
                            $html .= '<tr>
                                <td bgcolor="#f2f2f2">'.$label.'</td>
                                <td bgcolor="#f2f2f2">'.$val.'</td>
                            </tr>';
                        }
                    }
                    $html .= '<tr>
                        <td bgcolor="#f2f2f2"></td>
                        <td bgcolor="#f2f2f2"></td>
                    </tr>';

                }

                if(!empty($order['sender_name']) || !empty($order['sender_surname'])) {
                    $html .= '<tr>
                        <td bgcolor="#f2f2f2">Vārds, uzvārds</td>
                        <td bgcolor="#f2f2f2">'.$order['sender_name'].' '.$order['sender_surname'].'</td>
                    </tr>';
                }
                if(!$order['juridic'] || !empty($order['sender_pers_code'])) {
                    $html .= '<tr>
                        <td bgcolor="#f2f2f2">Perosnas kods</td>
                        <td bgcolor="#f2f2f2">'.$order['sender_pers_code'].'</td>
                    </tr>';
                } elseif($order['juridic'] && !empty($order['sender_position'])) {
                    $html .= '<tr>
                        <td bgcolor="#f2f2f2">Amats</td>
                        <td bgcolor="#f2f2f2">'.$order['sender_position'].'</td>
                    </tr>';
                }
                if(!empty($order['sender_phone'])) {
                    $html .= '<tr>
                        <td bgcolor="#f2f2f2">Tālrunis</td>
                        <td bgcolor="#f2f2f2">'.$order['sender_phone'].'</td>
                    </tr>';
                }
                if(!empty($order['sender_email'])) {
                    $html .= '<tr>
                        <td bgcolor="#f2f2f2">E-pasts</td>
                        <td bgcolor="#f2f2f2">'.$order['sender_email'].'</td>
                    </tr>';
                }


                $html .= '</table>';

                $pdf->writeHTML($html, false, false, true, 0);



                // ------------------------ TABLE 2

                $pdf->setY($pdf->getY() + 15);

                $orange_bg_image = $_SERVER['DOCUMENT_ROOT'].'/'.$orange_bg;
                list($w, $h) = $pdf->getWidthHeight($orange_bg_image,1.6);
                $y = $pdf->getY();




                $pdf->Image($orange_bg_image, $x, $y, $w-55, $h, '', '', '', false, 300, '', false, false, 0);

                $html = '
					<table  border="0" cellspacing="0" cellpadding="4" width="625">
						<tr>
							<th style="color:white;width:30px">Nr.</th>
							<th style="color:white;width:312px">Nosaukums</th>
							<th style="color:white;width:70px">Mērv.</th>
							<th style="color:white;width:80px">Daudz.</th>'./*
							<th style="color:white;width:80px">Cena, EUR</th>*/
							'<th style="color:white;width:120px">Summa, EUR</th>
						</tr>';

                $total_price_w_pvn = $price;
                $pvn = array(
                    '12'=>0,
                    '21'=>0
                );

                $pvn['21'] += (float)$price * 0.21;

                $html .= '
							<tr>
								<td style="text-align:center;">1</td>
								<td>Kurjera pakalpojumi<br />(No '.$order['location_from'].' uz '.$order['location_to'].')</td>
								<td style="border-right:1px solid #EF7D06;">km</td>
								<td style="text-align:right;border-right:1px solid #EF7D06;">'. number_format(($order['distance'] / 1000),2,'.','') .'</td>'./*
								<td style="text-align:right;border-right:1px solid #EF7D06;">' . $order['rate'] .'</td>*/
								'<td style="text-align:right;">' . $price .'</td>
							</tr>
						';

                $total_price_wo_pvn = $total_price_w_pvn - ((float)$pvn['12'] + (float)$pvn['21']);


                $html .= '<tr>
						<td colspan="4" style="border-top:1px solid #EF7D06">Kopsumma bez PVN</td>
						<td style="border-top:1px solid #EF7D06;text-align:right;">'.number_format($total_price_wo_pvn, 2, '.', '').'</td>
					</tr>';

                $sx = 0;
                foreach($pvn as $nod=>$sum) {
                    if($sum!=0) {
                        $html .= '<tr>
								<td colspan="4">PVN ('.$nod.'%)</td>
								<td style="text-align:right;">'.number_format($sum, 2, '.', '').'</td>
							</tr>';
                        $sx++;
                    }
                }

                $html .= '<tr>
						<td colspan="4" style="border-top:1px solid #EF7D06;">Summa ar PVN</td>
						<td style="text-align:right;border-top:1px solid #EF7D06;">
						    <b>'.number_format($total_price_w_pvn, 2, '.', '').' EUR</b>
						</td>
					</tr>
				';


                $html .= '
					<tr>
						<td colspan="5">Summa vārdiem: <b>'.ucfirst(trim(amountToStrint(number_format($total_price_w_pvn, 2, ',', ''), 'eiro', 'centi'))).'</b></td>
					</tr>
				';

                $html .= '</table>';


                $x = 12;
                $pdf->setX($x);

                $x = $pdf->getX();

                $pdf->writeHTML($html, false, false, true, 0);

                $html = '<span style="font-size:9pt;">Rēķins sagatavots elektorniski un derīgs bez paraksta.</span>';
                $pdf->setY($pdf->getY() + 2);
                $pdf->writeHTML($html, false, false, true, 0);



                $pdf->Output($invoicePath, $output);

            } elseif($output == 'I') {

                header('Content-Type: application/pdf');
                header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
                header('Pragma: public');
                header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
                header('Content-Disposition: inline; filename="'.basename($invoicePath).'";');

                echo file_get_contents($invoicePath);

            }


            return $invoicePath;


        }

        public function generateInvoicePdf($order_id, $rewrite = false, $output = 'F', $senderCopy = true){

            $cour = new CMS_Couriers();
            $order = $this->getItem($order_id);
            $courier = $cour->getItem($order['driver_id']);

            $invoiceRootPath = _ROOT .'/storage/invoices/';
            $invoicePath = $invoiceRootPath . 'invoice_'.$order_id.(!$senderCopy?'_receiver':'').'.pdf';

            if($rewrite) @unlink($invoicePath);

            if(!file_exists($invoicePath)){

                if(!file_exists(dirname($invoicePath)))
                    mkdir(dirname($invoicePath),0775);

                require_once('library/tcpdf/tcpdf.php');
                $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

                $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                $pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
                $pdf->SetHeaderMargin(0);
                $pdf->SetFooterMargin(0);
                $pdf->setPrintFooter(false);
                $pdf->SetAutoPageBreak(false, 0);
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

                $bill_bg = 'library/tcpdf/images/bill_bg.jpg';
                $orange_bg = 'library/tcpdf/images/orange_bg.jpg';




                $x = 12;
                $y = round($pdf->getPageHeight()-660);

                $pdf->SetFont('dejavusans', '', 10, '', true);

                $u = $senderCopy ? 2 : 1;
                for($i = 1; $i <= $u; $i++) {
                    $pdf->AddPage();

                    $html = '<table border="0" cellspacing="0" cellpadding="3" width="618">';
                    $html .= '	<tr><td align="right" style="font-size:9pt;">'.($senderCopy ? ($i==1?'Sūtītāja kopija':'Kurjera kopija') : 'Saņēmēja kopija').'</td></tr>';
                    $html .= '	<tr><td align="right"><b>PIEŅEMŠANAS-NODOŠANAS AKTS<br />RĒĶINA NR. '.$order['order_num'].'</b></td></tr>';

                    $html .= '	<tr><td align="right">DATUMS: '.date('d.m.Y', $order['date']).'</td></tr>';
                    $html .= '</table>';

                    $pdf->setY(5);

                    $pdf->writeHTML($html, false, false, true, 0);

                    // ------------------------ TABLE 1

                    $bill_bg_image = _ROOT .'/'. $bill_bg;
                    list($w, $h) = $pdf->getWidthHeight($bill_bg_image,11.6);
                    $y = $pdf->getY()+10;
                    //$pdf->Image($bill_bg_image, $x, $y, $w, $h+30, '', '', '', false, 300, '', false, false, 0);

                    $pdf->setY($pdf->getY()+13);

                    $x = 10;
                    $pdf->setX($x);

                    $html = '<table border="0" cellspacing="0" cellpadding="4" width="670">';


                    $html .= '<tr>
							<td width="335" bgcolor="#f2f2f2" style="border-bottom:1px solid #b9b9b9;border-right:1px solid #b9b9b9;"><b>Sūtītājs</b></td>
							<td width="335" bgcolor="#f2f2f2" style="border-bottom:1px solid #b9b9b9;"><b>Saņēmējs</b></td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2" style="border-right:1px solid #b9b9b9;">Adrese:<br />'.$order['location_from'].'</td>
							<td bgcolor="#f2f2f2">Adrese:<br />'.$order['location_to'].'</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2" style="border-right:1px solid #b9b9b9;">'.(
                        $order['juridic'] ? ('Uzņēmuma nosaukums:<br />'.$order['company_name']) : ('Vārds Uzvārds:<br />'.$order['sender_name'] .' '. $order['sender_surname'])
                        ).'</td>
							<td bgcolor="#f2f2f2">Vārds Uzvārds:<br />'. $order['address_name'] .' '. $order['address_surname'] .'</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2" style="border-right:1px solid #b9b9b9;">'.(
                        $order['juridic'] ? ('Reģistrācijas numurs:<br />'.$order['reg_number']) : ('Personas kods:<br />'.$order['sender_pers_code'])
                        ).'</td>
							<td bgcolor="#f2f2f2">Tālrunis:<br />'. $order['address_phone'] .'</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2" style="border-right:1px solid #b9b9b9;">'.(
                        $order['juridic'] ? ('PVN maksātāja numurs:<br />'.$order['pvn_number']) : ('Tālrunis:<br />'.$order['sender_phone'])
                        ).'</td>
							<td bgcolor="#f2f2f2">E-pasts:<br />'. $order['address_email'] .'</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2" style="border-right:1px solid #b9b9b9;">'.(
                        $order['juridic'] ? ('Faktiskā adrese:<br />'.$order['fact_address']) : ('E-pasts:<br />'.$order['sender_email'])
                        ).'</td>
							<td '. ($order['juridic'] ? 'rowspan="9"':'') .' bgcolor="#f2f2f2">&nbsp;</td>
						</tr>';
                    if($order['juridic']) {
                        $html .= '<tr>
							<td bgcolor="#f2f2f2" style="border-right:1px solid #b9b9b9;">Juridiskā adrese:<br />'. $order['jur_address'] .'</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2" style="border-right:1px solid #b9b9b9;">Banka:<br />'. $order['bank_name'] .'</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2" style="border-right:1px solid #b9b9b9;">Bankas kods:<br />'. $order['bank_swift'] .'</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2" style="border-right:1px solid #b9b9b9;">Konta numurs:<br />'. $order['iban_number'] .'</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2" style="border-top:1px solid #b9b9b9;border-right:1px solid #b9b9b9;">Kontaktpersona:<br />'. $order['sender_name'] .' '. $order['sender_surname'] .'</td>
						</tr>
						<!--tr>
							<td bgcolor="#f2f2f2" style="border-right:1px solid #b9b9b9;">Amats:<br />'. $order['sender_position'] .'</td>
						</tr-->
						<tr>
							<td bgcolor="#f2f2f2" style="border-right:1px solid #b9b9b9;">Tālrunis:<br />'. $order['sender_phone'] .'</td>
						</tr>
						<tr>
							<td bgcolor="#f2f2f2" style="border-right:1px solid #b9b9b9;">E-pasts:<br />'. $order['sender_email'] .'</td>
						</tr>';
                    }

                    $html .= '</table>';

                    $pdf->writeHTML($html, false, false, true, 0);



                    $html = '<div>Drošības kods: <b>'. strtoupper(substr(md5(time()), 3, 5)) .'</b></div>';

                    $pdf->setY($pdf->getY() + 3);
                    $pdf->writeHTML($html, false, false, true, 0);

                    $html = '<div style="margin-top:5px;">Kurjers: <u>'.$courier['surname'].' ('.$courier['name'].'), tālr. '.$courier['phone'].'</u></div>';
                    $pdf->writeHTML($html, false, false, true, 0);

                    $html = '<div>Apdrošināšana: <i>'. cms::get('langs')->text('site','order:insurance-'.($order['insurance']?'custom':'standard'),null,false) .'</i>
                    '.($order['insurance']?('<div style="padding:4px;">'.nl2br($order['insurance_info']).'</div>'):'').'</div>';
                    $pdf->writeHTML($html, false, false, true, 0);


                    $html = '<table style="" cellspacing="5" width="670">
                    <tr>
                        <td style="width:17px;border:1px solid black;"></td>
                        <td style="width:310px;">
                            Paku nodevu
                        </td>
                        <td style="width:17px;border:1px solid black;"></td>
                        <td style="width:310px;">
                            Paku saņēmu
                        </td>
                    </tr>
                    <tr>
                        '.($i==1&&!$senderCopy?'<td colspan="2"></td>':'<td style="width:17px;border:1px solid black;"></td>
                        <td>
                            Pretenziju nav
                        </td>').'
                        '.($i==1&&$senderCopy?'<td colspan="2"></td>':'<td style="width:17px;border:1px solid black;"></td>
                        <td>
                            Pretenziju nav
                        </td>').'
                    </tr>
                    <tr>
                        <td colspan="4" style="height:35px;"></td>
                    </tr>
                    <tr>
                        <td colspan="2">'.($i==1&&!$senderCopy?'Kurjera':'Sūtītāja').' paraksts</td>
                        <td colspan="2">'.($i==1&&$senderCopy?'Kurjera':'Saņēmēja').' paraksts</td>
                    </tr>
                    <tr>
                        <td colspan="4"></td>
                    </tr>
                    <tr>
                        <td colspan="2">______________________________</td>
                        <td colspan="2">______________________________</td>
                    </tr>
                    <tr>
                        <td colspan="4" style="height:15px;"></td>
                    </tr>
                    <tr>
                        <td colspan="2">Datums:</td>
                        <td colspan="2">Datums:</td>
                    </tr>
                    <tr>
                        <td colspan="2">Vieta:</td>
                        <td colspan="2">Vieta:</td>
                    </tr>
                </table>';

                    $pdf->setY($pdf->getY() + 5);
                    $pdf->writeHTML($html, false, false, true, 0);


                }



                $pdf->Output($invoicePath, $output);

            } elseif($output == 'I') {

                header('Content-Type: application/pdf');
                header('Cache-Control: private, must-revalidate, post-check=0, pre-check=0, max-age=1');
                header('Pragma: public');
                header('Expires: Sat, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
                header('Content-Disposition: inline; filename="'.basename($invoicePath).'";');

                echo file_get_contents($invoicePath);

            }


            return $invoicePath;


        }

	}
