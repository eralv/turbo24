<?php

	class CMS_Couriers extends Engine_Model {

		const TBL = 'cms_couriers';
		const SUPERUSER = 4;

        public $self_fields = array(
            'id', 'name', 'surname', 'phone'
        );

        public $filter_fields = array(
	    'name', 'surname', 'phone'
        );

   
		public function getName($id) {
			$sql = "SELECT name FROM ".self::TBL." WHERE id=".(int)$id;
			return cms::get('db')->field($sql);
		}

        public function getItem($filter = array(), $language = null) {

            $sql = 'SELECT '. $this->parseFields($this->getFields()) .' FROM `'. self::TBL .'` WHERE `id`='. (int)$filter;

            return cms::get('db')->row($sql);

        }

        public function getList($filters = array(), $language = null) {

            $fields = array();

            $alias = 'us';

            array_merge($fields,$this->getFields($alias));

            return cms::get('db')->rows(
                'SELECT '. $this->parseFields($fields) .'
                FROM `'. self::TBL .'` as `'. $alias .'`');

        }

        public function saveItem($post, $item = false) {

            $fields = $this->filteredFields($post);
            if(setIf_isset($id,$item['id']) && $id) {
                cms::get('db')->update(self::TBL, $fields, ' id='.$id);
            } else {
                cms::get('db')->insert(self::TBL, $fields);
            }

        }

        public function deleteItem($id) {
            cms::get('db')->delete(self::TBL, ' id='.(int)$id);
        }

		public function fetch()
		{
			if($this->is()) {

				if($this->getId()) {

                    return cms::get('db')->row('select * from `'. self::TBL .'` where id = "'. $this->getId() .'"');

				}
			}
			return false;
		}
		

		// admin controls 
		public function control($command, $param = null, $adds = array())
		{
		
			if(!$this->admin()) return null;
			
			$controls = new Engine_Users_Administration;
			$tpl = cms::get('tpls');
			
			switch($command)
			{
			
				case 'menu':
					
					if($param) {

                        return ' data-edit="admin_controls tree '. $param .'" ';

					} else {

						$controls = $controls->listControls();
						if(!empty($controls)) {
							$p = array(
								'menu' => $controls
							);

							return $tpl
								->loadVars($p)
								->returnTpl('back/admin_controls/menu');
						}

					}
					
				    break;

                case 'slider':

                    if(isset($param)) {
                        return ' data-edit="admin_controls slider ' . $param . '" ';
                    }

                    break;
				
				case 'phrase':
					
					if(isset($param)) {
						return ' data-edit="admin_controls phrase ' . $param . '" ';
					}

				    break;

			}
		
			return null;
		}
	}