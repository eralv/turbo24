<?php
include_once _ROOT .'library/tw-auth/twitteroauth/twitteroauth.php';

class CMS_Twitter {

	const TBL = 'sys_users';

	private $allowedMethods = array(
		'complete_registration'
	);

	public $consumer_key = 'f8l4wGyWqdwZlBheZEns91ApD';
	public $consumer_secret = 'g48gbOYO3LsQu1yGuKEFrAQEyYgSSVtORbs5DCiEzksmFNZqOJ';

	public $url = "ll/auth/twitter/";


	function __construct()
	{

        if(!empty($_GET['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])){

            $twitter = new TwitterOAuth($this->consumer_key, $this->consumer_secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
            $access_token = $twitter->getAccessToken($_GET['oauth_verifier']);
            $_SESSION['access_token'] = $access_token;

            if($user = $this->getUser($twitter)) {
                list($id) = $user;
                if($user_row = cms::get('users')->getItem(array('social_id'=>$id, 'social'=>'twitter'))) {
                    $this->authTwitter();
                } else {
                    redirect($this->getUrl() .'?action=complete_registration');
                }
            }

        } elseif(isset($_GET['action']) && in_array($_GET['action'], $this->allowedMethods)) {
			
			$this->{$_GET['action']}($_GET);

		}

	}


    public function getUrl() {
        return _HOST . $this->url;
    }

    /**
     * @param array|TwitterOAuth $data
     * @param bool $fullData
     * @return array|bool
     */
    public function getUser($data = null, $fullData = false) {

        if(isset($_SESSION['twitter_user'])) {
            $userData = $_SESSION['twitter_user'];
        } elseif(is_a($data,'TwitterOAuth')) {
            $_SESSION['twitter_user'] = $userData = $data->get('account/verify_credentials');
        }

        if(isset($userData) && (!isset($userData->errors) || empty($userData->errors))) {
            if($fullData)
                return $userData;
            else
                return array($userData->id, $userData->name);
        }

        return false;

    }

	public function authTwitter(){

		if($user = $this->getUser()){
            list($id) = $user;

			$user_row = cms::get('users')->getItem(array('social_id'=>$id,'social'=>'twitter'));

			if(!$user_row['id']){

				redirect($this->getUrl());

			} else {
				$_SESSION['Engine_Users'] = $user_row;
				unset($_SESSION['twitter_user']);

                 if(isset($_SESSION['referrer'])) {
                     redirect($_SESSION['referrer']);
                 }
			 }

		} else {

			CMS_SystemMessages::save( 'error', 'twitter_auth_error');
		}

        redirect(_HOST);

	}

	public function complete_registration($params){

		 if($user = $this->getUser()) {
             list($id, $name) = $user;

				$fields = array(
					'name'          => $name,
                    'social_id'     => $id,
					'social'        => 'twitter'
				);

				$uid = Engine_Controller::get('db')->insert(self::TBL, $fields);

				if($user_row = cms::get('users')->getItem($uid)) {
                    $_SESSION['Engine_Users'] = $user_row;

                    if(isset($_SESSION['referrer']))
                        redirect($_SESSION['referrer']);

                }

		 }

        redirect(_HOST);
	
	}

	public function logout(){

		session_destroy();

	}

}