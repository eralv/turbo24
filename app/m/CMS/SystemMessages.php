<?php

class CMS_SystemMessages {
	
	function __construct() 
	{
		if(!isset($_SESSION[__CLASS__]))
		{
			$_SESSION[__CLASS__] = array();
		}
	}
	
	static function save($type, $text, $add = null)
	{
		$_SESSION[__CLASS__][$type][$text] = $add === null ? $text : cms::get('langs')->text($add, $text);
	}
	
	static function out() 
	{

		if(!isset($_SESSION[__CLASS__]) || !is_array($_SESSION[__CLASS__]) || count($_SESSION[__CLASS__])<1)
            return '';

		$messages = $_SESSION[__CLASS__];
		
		$js = '<script type="text/javascript">
			var messages = ' . json_encode($_SESSION[__CLASS__]) . ';
			jQuery(document).ready(function(){
				for(x in messages)
				{
					switch(x)
					{
						case "error":
							var error_class = "error";
						break;
						case "success":
							var error_class = "success";
						break;
						default:
							var error_class = "notice";
					}
					var msgText = "";
					';

					foreach($messages as $statuss => $statuss_row){
						foreach($statuss_row as $place => $error_text){
							$js .= "msgText += '$error_text';".PHP_EOL;//<p ".Engine_Controller::get('users')->control('phrase', __CLASS__ . ".".$statuss."_".$place).">".$error_text."</p>';".PHP_EOL;
						}
					}
					
                    $js .= '
					openModal(msgText);
				}



			});
		';
		$js .= '</script>';
		
		unset($_SESSION[__CLASS__]);
		
		return $js;
	}
}