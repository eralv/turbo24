<?php

class CMS_CreditCards {
	
	const TBL = 'cms_transactions';
	const TBL_LIST = 'cms_transaction_list';
	const TBL_ERROR = 'cms_transaction_error';
	const TBL_BATCH = 'cms_transaction_batch';
	const DNB_PATH = '/home/www/e-commerce/dnb/';
	const JARS_PATH = '/ecomm/MerchantHandler/';
	const merchantProperties_PATH = '/home/www/e-commerce/dnb/merchant.properties';
	const _PAYMENS_LOG_URL = '.UPLOADS/logs/payment_creditcards.log';
	
	private $currency	= 978;	    //428=LVL 978=EUR 840=USD 941=RSD 703=SKK 440=LTL 233=EEK 643=RUB 891=YUM
	
	private $ecomm_server_url = "https://secureshop-test.firstdata.lv:8443/ecomm/MerchantHandler";
	private $ecomm_client_url = 'https://secureshop-test.firstdata.lv/ecomm/ClientHandler';

	//private $ecomm_server_url = 'https://secureshop.firstdata.lv:8443/ecomm/MerchantHandler';
	//private $ecomm_client_url = 'https://secureshop.firstdata.lv/ecomm/ClientHandler';	
	
	private $account = 0;
	private $accounts = array(
		0 => array(	// SANDBOX

			'keystore'      => '/home/www/2014/7313489_keystore.pem',
			'password'      => 'PHBdC9bNc'
		),
		1 => array( // REAL

			'keystore'      => '/home/www/2014/7313489_keystore.pem',
			'password'      => 'PHBdC9bNc'

		),
		2 => array( // DEV

			'keystore'      => '/storage/biznesam_keystore.pem',
			'password'      => 'PHBdC9bNc'

		)
	);
	
	public $url = null;

	
	
	function __construct($uId, $account=0)
	{
		//cms::get('users')->getId() = (int)$uId;
		$this->account = (int)$account;
	}

	/*
	*	Loggin results
	*/
	static function writeLog( $post ){

		$fp = fopen(self::_PAYMENS_LOG_URL, 'a');
		if(!$fp) return;
		fseek( $fp, 0 );
		fwrite( $fp, 
			date('Y-m-d H:i:s').PHP_EOL.
			'RESULT: ' . ( isset($post['LMI_PAYMENT_NO']) ? 'possibly OK' : 'FAIL' ) . PHP_EOL.
			serialize($post).PHP_EOL.PHP_EOL.PHP_EOL
		);
		fclose($fp);
		return;
	}
	
	private function sentPost($params){


		$keystore = _ROOT . $this->accounts[$this->account]['keystore'];
		$password = $this->accounts[$this->account]['password'];
		
		if(!file_exists($keystore)){
			echo $result = "file " . $keystore . " not exists"; 
		}
		
		if(!is_readable($keystore)){
			echo $result = "Please check CHMOD for file \"" . $keystore . "\"! It must be readable!";
		}
		
		$post = "";
		
		foreach ($params as $key => $value){
			 $post .= "$key=$value&";
		}

		$curl = curl_init();		
		
		curl_setopt($curl, CURLOPT_VERBOSE, TRUE);
		curl_setopt($curl, CURLOPT_URL, $this->ecomm_server_url); 
		curl_setopt($curl, CURLOPT_HEADER, 0);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_SSLCERT, $keystore);
		curl_setopt($curl, CURLOPT_CAINFO, $keystore);
		curl_setopt($curl, CURLOPT_SSLKEYPASSWD, $password);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);		
		
		
		$resp =curl_exec ($curl);
		
		if(curl_error($curl)){
			$resp = curl_error($curl);
			self::writeLog($resp,$post);
		}
		curl_close ($curl);
		
		return $resp;
	}

	
	public function confirmTransactionData($post)
	{
		//if(!cms::get('users')->is()) return;
		$lang = cms::get('langs')->getLang();
		
		$op_id = -1;
		
		$trans_id	= $post['trans_id'];

		
		$ip	= $_SERVER["REMOTE_ADDR"];  // client IP adress

		if ($trans_id) {
		
			$resp = $this -> getTransResult(urlencode($trans_id));


			
			if (strstr($resp, 'RESULT:')) { //$resp example RESULT: OK RESULT_CODE: 000 3DSECURE: NOTPARTICIPATED RRN: 915300393049 APPROVAL_CODE: 705368 CARD_NUMBER: 4***********9913 

				$response = $this->parseResponse($resp);


			}	
		
            //var_dump($resp);exit;
			
			// GET op_id by transid
			if( $op_id = $this->getOpIdByTransaction($trans_id))
			{
                //var_dump($response);exit;
                $amount = cms::get('db')->field('SELECT amount FROM '.self::TBL_LIST.' WHERE operation_id='.$op_id);
				if ($response['result']=="OK")
				{

					$fields = array(
						  'result'			=> $response['result'],
						  'result_code'		=> $response['result_code'],
						  'result_3dsecure'	=> $response['result_3dsecure'],
						  'card_number'		=> $response['card_number'],
						  'response'		=> $resp    
					);						  
					
					cms::get('db')->update(self::TBL_LIST, $fields, 'trans_id = "' . $trans_id . '"');

					$fields = array(
							'trans_id'	=> $trans_id,
							'error_time'	=> date('Y-m-d H:i:s'),
							'action'		=> 'ReturnOk',
							'response'		=> $resp
					);					
					cms::get('db')->insert(self::TBL_ERROR, $fields);
					

					$order_fields = array(
                        'statuss' => 2 ,
                        'submitted' => time(),
                        'summ_paid' => $amount
					);			
					//cms::get('db')->update(CMS_Orders::TBL, $order_fields, 'id = ' . (int)$op_id); TODO: Implement this

					CMS_SystemMessages::save('success','credit_card_success');

				}
				else
				{			

					
					$fields = array(
							'trans_id'	=> $trans_id,
							'error_time'	=> date('Y-m-d H:i:s'),
							'action'		=> 'ReturnFail',
							'response'		=> $resp
					);					
					cms::get('db')->insert(self::TBL_ERROR, $fields);					
				
					$order_fields = array(
						'status' => 4 ,
					);				
					//cms::get('db')->update(CMS_Orders::TBL, $order_fields, 'id = "' . $op_id . '" and lid = "' . cms::get('users')->getId() . '"'); TODO: Implement this
				

					$op_id = -1;
					CMS_SystemMessages::save('error','cc_error_'.__LINE__);

				}
				/*		
				$trans_fields = array(
						'changed' => date('Y-m-d H:i:s'),
						'resp' => $resp,
				);
				cms::get('db')->update(self::TBL, $trans_fields, 'trans_id = "' . $trans_id . '"');		
				*/

			}
			else 
			{
				$return = 'No operation Id found';
				CMS_SystemMessages::save('error','cc_error_'.__LINE__);
			}
			
			if($op_id==-1)
			{
				mysql_query('update ' . self::TBL_LIST .' set status=1 where trans_id = "' . $trans_id . '"');
			}

            if(isset($_SESSION['payment_referrer'])) {
                redirect($_SESSION['payment_referrer']);
            }

		}
		else
		{
			$return = 'No transaction ID';
			CMS_SystemMessages::save('error','cc_error_'.__LINE__);
		}
		
		return $op_id;
	}
	

	

		
	
	
	private function getOpIdByTransaction( $trans_id )
	{
		$sql = 'select operation_id from ' . self::TBL_LIST . ' where trans_id="' . mysql_real_escape_string($trans_id) . '"';
//user_id = ' . cms::get('users')->getId() . ' and 

		return cms::get('db')->field($sql, false);
	}
	
	
	public function getTransactionByOpId( $op_id )
	{
		$sql = '
			select trans_id, totalprice, status, reversed
			from '.self::TBL.' 
			where user_id = ' . cms::get('users')->getId() . ' and operation_id="' . mysql_real_escape_string($op_id) . '"
		';
		return cms::get('db')->row($sql);
	}
	
	public function getTransactionById( $trans_id )
	{
		$sql = '
			select trans_id, totalprice, status, reversed
			from '.self::TBL.' 
			where trans_id="' . mysql_real_escape_string($trans_id) . '"
		';
		return cms::get('db')->row($sql);
	}
	
	public function parseResponse($resp){
	
		$response = array();
	
		if (strstr($resp, 'RESULT:')) {
			$result = explode('RESULT: ', $resp);
			$result = preg_split( '/\r\n|\r|\n/', $result[1] );
			$response['result'] = $result[0];
		}else{
			$response['result'] = '';
		}

		if (strstr($resp, 'RESULT_CODE:')) {
			$result_code = explode('RESULT_CODE: ', $resp);
			$result_code = preg_split( '/\r\n|\r|\n/', $result_code[1] );
			$response['result_code'] = $result_code[0];
		}else{
			$response['result_code'] = '';
		}

		if (strstr($resp, '3DSECURE:')) {
			$result_3dsecure = explode('3DSECURE: ', $resp);
			$result_3dsecure = preg_split( '/\r\n|\r|\n/', $result_3dsecure[1] );
			$response['result_3dsecure'] = $result_3dsecure[0];
		}else{
			$response['result_3dsecure'] = '';
		}

		if (strstr($resp, 'CARD_NUMBER:')) {
			$card_number = explode('CARD_NUMBER: ', $resp);
			$card_number = preg_split( '/\r\n|\r|\n/', $card_number[1] );
			$response['card_number'] = $card_number[0];
		}else{
			$response['card_number'] = '';
		}


		if (strstr($resp, 'FLD_075:')) {
			$count_reversal = explode('FLD_075: ', $resp);
			$count_reversal = preg_split( '/\r\n|\r|\n/', $count_reversal[1] );
			$response['count_reversal'] = $count_reversal[0];
		}else{
			$response['count_reversal'] = '';
		}

		if (strstr($resp, 'FLD_076:')) {
			$count_transaction = explode('FLD_076: ', $resp);
			$count_transaction = preg_split( '/\r\n|\r|\n/', $count_transaction[1] );
			$response['count_transaction'] = $count_transaction[0];
		}else{
			$response['count_transaction'] = '';
		}

		if (strstr($resp, 'FLD_087:')) {
			$amount_reversal = explode('FLD_087: ', $resp);
			$amount_reversal = preg_split( '/\r\n|\r|\n/', $amount_reversal[1] );
			$response['amount_reversal'] = $amount_reversal[0];
		}else{
			$response['amount_reversal'] = '';
		}

		if (strstr($resp, 'FLD_088:')) {
			$amount_transaction = explode('FLD_088: ', $resp);
			$amount_transaction = preg_split( '/\r\n|\r|\n/', $amount_transaction[1] );
			$response['amount_transaction'] = $amount_transaction[0];
		}else{
			$response['amount_transaction'] = '';
		}
		
		
		return $response;
	
	}
		

	/**
	 * Registering of SMS transaction
	 * @param int $amount transaction amount in minor units, mandatory
	 * @param string $desc description of transaction, optional
	 * @return string TRANSACTION_ID
	*/

	function startSMSTrans($amount, $desc=''){
	
		$language = cms::get('langs')->getLang();
		$ip = $_SERVER['REMOTE_ADDR'];
		$currency = $this->currency;	

		$params = array(
			'command' => 'v',
			'amount'  => $amount*100,
			'currency'=> $currency,
			'client_ip_addr'      => $ip,
			'description'    => $desc,
			'language'=> $language	  
		);
		return $this->sentPost($params);
	}

	/**
	 * Registering of DMS authorisation
	 * @param int $amount transaction amount in minor units, mandatory 
	 * @param int $op_id transaction currency code, mandatory
	 * @param string $desc description of transaction, optional
	 * @param bool $auto_redirect description of transaction, optional
	 * @return string TRANSACTION_ID
	*/

	function startDMSAuth($amount, $op_id, $desc='', $auto_redirect = true){
	
		//if(!cms::get('users')->is()) return;

		$language = cms::get('langs')->getLang();
		$ip = $_SERVER['REMOTE_ADDR'];
		$currency = $this->currency;
		
		$params = array(
			'command' 			=> 'a',
			'msg_type'			=> 'DMS',
			'amount'  			=> $amount*100,
			'currency'			=> $currency,
			'client_ip_addr'	=> $ip,
			'language'    		=> $language,  
		);

		$resp = $this->sentPost($params);

		if (substr($resp,0,14)=="TRANSACTION_ID") {
		
			$trans_id = urlencode(substr($resp,16,28));

			$url = $this->ecomm_client_url.'?trans_id='. $trans_id;
		
			$status=1;
			$now= date( "Y-m-d H:i:s", time() );
		
			$fields = array(
					'operation_id'		=> (int)$op_id,
					'trans_id'			=> urldecode($trans_id),
					'amount'			=> (float)$amount,
					'currency'			=> $currency,
					'client_ip_addr'	=> $ip,
					'description'		=> mres($desc),
					'language' 			=> $language,
					'dms_ok'			=> 'NO',
					'result'			=> '???',
					'result_code'		=> '???',
					'result_3dsecure'	=> '???',
					'card_number'		=> '???',
					't_date'			=> date('Y-m-d H:i:s'),
					'response'			=> $resp
			);			


			cms::get('db')->insert(self::TBL_LIST, $fields, true, true);

			//redirect($url);
			//redirect('/'.$lang.'/firstdata'); // FOR TEST ONLY

            if($auto_redirect) {
                redirect($url);
            } else {
                $this->url = $url;
            }

		} else {
			
            $resp = htmlentities($resp, ENT_QUOTES);
			
			$fields = array(			
					'trans_id'	        => '',
					'error_time'		=> date('Y-m-d H:i:s'),
					'action'			=> 'startDMSAuth',
					'response'			=> $resp
			);
			
			cms::get('db')->insert(self::TBL_ERROR, $fields);

            CMS_SystemMessages::save('error','cc_error_'.__LINE__);
            if($auto_redirect) {
                redirect( '/' .cms::get('langs')->getLang() . '/cart' );
            }

		} // No trans_id received
		
	}

	/**
	 * Making of DMS transaction
	 * @param int $trans_id id of previously made successeful authorisation
	 * @param int $amount transaction amount in minor units, mandatory
	 * @param string $desc description of transaction, optional
	 * @return string RESULT, RESULT_CODE, RRN, APPROVAL_CODE
	*/

	function makeDMSTrans($trans_id, $amount, $desc = ''){

		$ip = $_SERVER['REMOTE_ADDR'];
		$currency = $this->currency;	
	
		$params = array(
			'command' => 't',
			'msg_type'=> 'DMS',
			'trans_id' => $trans_id, 
			'amount'  => $amount*100,
			'currency'=> $currency,
			'client_ip_addr' => $ip	
		);

		$resp = $this->sentPost($params);

			if (substr($resp,8,2)=="OK") {

				$fields = array(
						'dms_ok'	 		=> 'YES',
						'makeDMS_amount'	=> $amount,
				);
				cms::get('db')->update(self::TBL_LIST, $fields, 'trans_id = "' . $trans_id . '"');	
											  

            }
             else{

			 
			    $resp = htmlentities($resp, ENT_QUOTES);
			   
				$fields = array(
						'trans_id'	=> $trans_id,
						'error_time'	=> date('Y-m-d H:i:s'),
						'action'		=> 'makeDMSTrans',
						'response'		=> $resp
				);					
				cms::get('db')->insert(self::TBL_ERROR, $fields);				 
			 


            }
	}

	/**
	 * Transaction result
	 * @param int $trans_id transaction identifier, mandatory
	 * @return string RESULT, RESULT_CODE, 3DSECURE, AAV, RRN, APPROVAL_CODE
	*/

	function getTransResult($trans_id){

		$ip = $_SERVER['REMOTE_ADDR'];
		
		$params = array(
			'command' => 'c',
			'trans_id' => $trans_id, 
			'client_ip_addr' => $ip
		);

		$str = $this->sentPost($params);
		return $str;
	}

	/**
	 * Transaction reversal
	 * @param int $trans_id transaction identifier, mandatory
	 * @param int $amount transaction amount in minor units, mandatory 
	 * @return string RESULT, RESULT_CODE
	*/

	function reverse($trans_id, $amount){

		
		$params = array(
			'command' => 'r',
			'trans_id' => urlencode($trans_id), 
			'amount'      => $amount*100
		);

		$resp = $this->sentPost($params);

          
		if (substr($resp,8,2) == "OK" OR substr($resp,8,8) == "REVERSED") {           
			   
			$response = $this->parseResponse($resp);

			$fields = array(
					'reversal_amount'	 => $amount,
					'result_code'		 => $response['result_code'],
					'result'			 => $response['result'],
					'response'			 => $resp
			);
 			cms::get('db')->update(self::TBL_LIST, $fields, 'trans_id = "' . $trans_id . '"');	


                                           


        }
        else{
           
		   $resp = htmlentities($resp, ENT_QUOTES);
		   
			$fields = array(
					'trans_id'	=> $trans_id,
					'error_time'	=> date('Y-m-d H:i:s'),
					'action'		=> 'reverse',
					'response'		=> $resp
			);					
			cms::get('db')->insert(self::TBL_ERROR, $fields);	                 

	
		}
	}

	/**
	 * Closing of business day
	 * @return string RESULT, RESULT_CODE, FLD_075, FLD_076, FLD_087, FLD_088
	*/

	function closeDay(){
	  
	  $params = array(
			'command' => 'b',
		);
		
		$resp = $this->sentPost($params);


		
		
		if (strstr($resp, 'RESULT:')) { //RESULT: OK RESULT_CODE: 500 FLD_075: 4 FLD_076: 6 FLD_087: 40 FLD_088: 60  

			$response = $this->parseResponse($resp);

			$fields = array(
					'result'				=> $response['result'],
					'result_code'			=> $response['result_code'],
					'count_reversal'		=> $response['count_reversal'],
					'count_transaction'		=> $response['count_transaction'],
					'amount_reversal'		=> $response['amount_reversal']/100,
					'amount_transaction'	=> $response['amount_transaction']/100,
					'close_date'			=> date('Y-m-d H:i:s'),
					'response'				=> $resp
			);
			
			cms::get('db')->insert(self::TBL_BATCH, $fields);	
		

		}
		else{
      
     
			$resp = htmlentities($resp, ENT_QUOTES);
	  
	  
			$fields = array(
					
					'error_time'	=> date('Y-m-d H:i:s'),
					'action'		=> 'closeDay',
					'response'		=> $resp
			);					
			cms::get('db')->insert(self::TBL_ERROR, $fields);		  
	  
		}		
		
		
	}

	
}
