<?php

class CMS_Facebook {

    private $allowedMethods = array(
        'complete_registration'
    );

    public $app_id = "898735583476397";
    public $app_secret = "3b4569ec9f407918d4273b95c316efe9";

    public $url = "ll/auth/facebook/";

    function __construct()
    {
        if(isset($_GET['error'])) {
            redirect(_HOST);
        }

        if(isset($_GET['action']) && in_array($_GET['action'], $this->allowedMethods)) {

            $this->{$_GET['action']}($_GET);

        } elseif(!empty($_GET['access_token']) && $user = $this->getUser($_GET['access_token'])) {
            list($id) = $user;

            if($user_row = cms::get('users')->getItem(array('social_id'=>$id,'social'=>'facebook'))) {
                $this->authFacebook();
            } else {
                redirect($this->getUrl() .'?action=complete_registration&access_token='. $_GET['access_token']);
            }

        } elseif($_GET["code"]) {

            $this->authFacebook($_GET["code"]);

        }

    }

    public function getUrl() {
        return _HOST . $this->url;
    }

    public function getUser($token) {

        $url = "https://graph.facebook.com/me?access_token=" . $token;
        if($data = $this->curl_get_file_contents($url)) {
            if(null !== $parsed = json_decode($data)) {
                if(!isset($parsed->error)) {
                    return array($parsed->id, $parsed->email, $parsed->first_name, $parsed->last_name);
                }
            }
        }
        return false;

    }

    public function curl_get_file_contents($URL) {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        //$err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
        curl_close($c);
        if ($contents) return $contents;
        else return FALSE;
    }

    public function authFacebook($code = ''){

        if(empty($code)) {
            $_SESSION['state'] = md5(uniqid(rand(), TRUE)); //CSRF protection
            $dialog_url = 	"http://www.facebook.com/dialog/oauth?client_id=" . $this->app_id .
                "&redirect_uri=" . $this->getUrl() .
                "&state=" . $_SESSION['state'] .
                "&scope=email"
            ;

            redirect( $dialog_url );

        } else {

            $token_url = "https://graph.facebook.com/oauth/access_token?" .
                "client_id=" . $this->app_id .
                "&redirect_uri=" . $this->getUrl() .
                "&client_secret=" . $this->app_secret .
                "&code=" . $code;

            $response = $this->curl_get_file_contents($token_url);

            $params = null;
            parse_str($response, $params);

            if($params && $user = $this->getUser($params['access_token'])) {
                list($id) = $user;

                $user_row = cms::get('users')->getItem(array('social_id'=>$id,'social'=>'facebook'));

                if(!$user_row['id']){

                    redirect(_HOST . $this->url .'?access_token='.$params['access_token']);

                } else {
                    $_SESSION['Engine_Users'] = $user_row;


                    if(isset($_SESSION['referrer']))
                        redirect($_SESSION['referrer']);
                    else
                        redirect(_HOST);
                }

            }

        }
        redirect();

    }

    public function complete_registration($params){

        if($params['access_token'] && $user = $this->getUser($params['access_token'])) {
            list($id, $email, $name, $surname) = $user;

                $fields = array(
                    'email'			=> $email,
                    'name'	        => $name,
                    'surname'       => $surname,
                    'social'	    => 'facebook',
                    'social_id'     => $id
                );

                if($uid = cms::get('db')->insert(cms::get('users')->getTable(), $fields)) {

                    $_SESSION['Engine_Users'] = cms::get('users')->getItem($uid);

                    if(isset($_SESSION['referrer']))
                        redirect($_SESSION['referrer']);
                }

        }

        redirect(_HOST);

    }

    public function logout(){

        $token_url = "https://graph.facebook.com/oauth/access_token?client_id=".$this->app_id."&client_secret=".$this->app_secret."&grant_type=client_credentials";

        $response = file_get_contents($token_url);
        $params = null;
        parse_str($response, $params);

        $logout_url = "https://www.facebook.com/logout.php?next=".(_HOST . $this->url)."&access_token=".$params['access_token'];
        echo  "<script>top.location.href='". $logout_url ."';</script>";

    }

}