<?php

	function __autoload($c)
	{
		$c = basename($c);
		
		if (strpos($c, '_'))
		{
			$c = str_replace('_', '/', $c);
		}

		require_once $c . '.php';

	}