<?php

interface Administration {

    public function getList($page = 1, $filter = array());

    public function getRecord($idf);

    public function saveRecord($idf = null, $data = array(), $file = null);

} 