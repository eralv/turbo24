<div class="slider">

    <?php if(empty($slides)) : ?>
        <div class="slides-placeholder"></div>
    <?php else : ?>
        <ul class="slides">
            <?php foreach($slides as $slide) :
                if($slide->slide) : ?>
                    <li>
                        <div class="slide" <?=$this->get('users')->control('slider', $slide->id);?>>
                            <?php if(!empty($slide->link)) : ?>
                            <a href="<?=$slide->link;?>" >
                                <?php endif; ?>
                                <img src="<?=$slide->getImgPath();?>" alt="<?=$slide->title;?>" />
                                <div class="text">
                                    <?=$slide->getText('text', true);?>
                                </div>
                                <?php if($slide->link) : ?>
                            </a>
                        <?php endif; ?>
                        </div>
                    </li>
                <?php endif;
            endforeach; ?>
        </ul>
    <?php endif; ?>

</div>