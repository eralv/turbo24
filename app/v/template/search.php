<?php
/**
 * @var CMS_TourType[] $types
 * @var CMS_Country[] $countries
 * @var array $filter
 */
?>
<div class="tour-search">

    <div class="form">

        <div class="email">
            <a href="#"><?=$this->get('langs')->text('site','email');?></a>
        </div>

        <div class="phone"><?=$this->get('langs')->text('site','phone');?></div>

        <hr class="border" />

        <form action="<?=$tree->controllerLink('tours', 'default');?>" method="post">

            <h4><?=$this->get('langs')->text('site','tour_search');?></h4>

            <label class="input-label input datepicker" <?=$this->get('users')->control('phrase','site.tour_search:date_from');?>>
                <input name="filter[date_from]" value="<?=$filter['date_from'];?>" type="text" placeholder="<?=$this->get('langs')->text('site','tour_search:date_from',null,false);?>" />
                <span class="arrow"></span>
            </label>
            <label class="input-label input datepicker" <?=$this->get('users')->control('phrase','site.tour_search:date_to');?>>
                <input name="filter[date_to]" value="<?=$filter['date_to'];?>" type="text" placeholder="<?=$this->get('langs')->text('site','tour_search:date_to',null,false);?>" />
                <span class="arrow"></span>
            </label>

            <div class="vertical-space"></div>

            <label class="input" <?=$this->get('users')->control('phrase','site.tour_search:price_from');?>>
                <input name="filter[price_from]" value="<?=$filter['price_from'];?>" type="text" placeholder="<?=$this->get('langs')->text('site','tour_search:price_from',null,false);?>" />
            </label>
            <label class="input" <?=$this->get('users')->control('phrase','site.tour_search:price_to');?>>
                <input name="filter[price_to]" value="<?=$filter['price_to'];?>" type="text" placeholder="<?=$this->get('langs')->text('site','tour_search:price_to',null,false);?>" />
            </label>

            <div class="vertical-space"></div>

            <div class="select" <?=$this->get('users')->control('phrase','site.tour_search:tour_type');?>>
                <select name="filter[type]">
                    <option value="0"><?=$this->get('langs')->text('site','tour_search:tour_type',null,false);?></option>
                    <?php foreach($types as $type) : ?>
                        <option <?=strtoupper(trim($filter['type']))==$type->idf?'selected':'';?> value="<?=$type->idf;?>"><?=$type->getTitle();?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <br />

            <div class="select" <?=$this->get('users')->control('phrase','site.tour_search:country');?>>
                <select name="filter[country]">
                    <option value="0"><?=$this->get('langs')->text('site','tour_search:country',null,false);?></option>
                    <?php foreach($countries as $country) : ?>
                        <option <?=strtoupper(trim($filter['country']))==$country->code?'selected':'';?> value="<?=$country->code;?>"><?=$country->title;?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-button">
                <button class="button" type="submit">
                    <span class="before"></span>
                    <span class="inner"><?=$this->get('langs')->text('site','tour_search:search_button');?></span>
                    <span class="after"></span>
                </button>
            </div>

        </form>
    </div>

</div>