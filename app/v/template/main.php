<?php
/**
 * @var array[] $vars
 */
?><!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="author" content="ERA.LV" />
        <meta name="description" content="<?php
            echo !empty($tree->treeInfo['metadescription'])
                ? $tree->treeInfo['metadescription']
                : $this->get('langs')->text('site','description',null,false);
        ?>" />
        <meta name="keywords" content="<?php
            echo !empty($tree->treeInfo['metakeywords'])
                ? $tree->treeInfo['metakeywords']
                : $this->get('langs')->text('site','keywords',null,false);
        ?>" />
        <title><?php
            echo (
                !empty($tree->treeInfo['metatitle'])
                    ?$tree->treeInfo['metatitle'].' - '
                    :''
                ) . $this->get('langs')->text('site','title',null,false);
        ?></title>

        <link rel="icon" type="image/png" href="/www/img/favicon.png" />
        <link href="/www/css/style.css" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" type="text/css" href="/www/css/flexslider.css">
	<link rel="stylesheet" type="text/css" href="/www/css/jquery-ui-1.10.4.custom.min.css">
	


        <!--[if lt IE 10]>
            <link rel="stylesheet" href="/www/css/ie.css" type="text/css" />
        <![endif]-->
        <?= $css ?>
	
	<script type="text/javascript" src="/www/js/jquery-1.11.0.min.js"></script>
        <!--script type="text/javascript" src="/www/js/jquery-min.js"></script-->
        <script type="text/javascript" src="/www/js/jquery-ui-min.js"></script>
        <script type="text/javascript" src="/www/js/jquery-ui.timepicker.js"></script>
        <script type="text/javascript" src="/www/js/jquery.flexslider-min.js"></script>
        <script type="text/javascript" src="/www/js/jquery.dropkick-min.js"></script>
	<!--script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script-->
	<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBQN9z-DAQXte9gOMH23JxAj6VyF27_fzs&sensor=true&libraries=places"></script>
	<script type="text/javascript" src="/www/js/jquery.geocomplete.min.js"></script>
	<script type="text/javascript" src="/www/js/jquery.validate.js"></script>
        <script type="text/javascript" src="/www/js/main.js"></script>

        <script type="text/javascript">
            var datePicker = {
                dateFormat: 'dd-mm-yy',
                showAnim:'slideDown',
                firstDay: 1,
                dayNames: ['Svetdiena','Pirmdiena','Otrdiena','Trešdiena','Ceturtdiena','Piektdiena','Sestdiena'],
                dayNamesMin: ['Sv','Pr','Ot','Tr','Ce','Pi','Se'],
                monthNames: ['Janvāris','Februāris','Marts','Aprīlis','Maijs','Jūnijs','Jūlijs','Augusts','Septembris','Oktobris','Novembris','Decembris']
            },
            dateTimePicker = {
                dateFormat: 'dd-mm-yy',
                showAnim:'slideDown',
                firstDay: 1,
                dayNames: ['Svetdiena','Pirmdiena','Otrdiena','Trešdiena','Ceturtdiena','Piektdiena','Sestdiena'],
                dayNamesMin: ['Sv','Pr','Ot','Tr','Ce','Pi','Se'],
                monthNames: ['Janvāris','Februāris','Marts','Aprīlis','Maijs','Jūnijs','Jūlijs','Augusts','Septembris','Oktobris','Novembris','Decembris'],
                minDate: new Date(<?=intval(date('Y'));?>, <?=intval(date('m'))-1;?>, <?=intval(date('d'));?>, <?=intval(date('H'));?>, <?=intval(date('i'));?>)
            };
        </script>

        <?php if($this->get('users')->admin() /*|| ($this->get('users')->is(Engine_Users::OPERATOR))*/) : ?>
            <script type="text/javascript" src="/library/tinymce/jquery.tinymce.min.js"></script>
            <script type="text/javascript" src="/library/tinymce/tinymce.min.js"></script>
            <script type="text/javascript">

                var tinyMceSelector = "textarea.textEditor",
                    tinyMceInit = {
                        selector: tinyMceSelector,
                        plugins: [
                            "advlist autolink autosave link image lists charmap print preview hr anchor pagebreak spellchecker",
                            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                            "table contextmenu directionality emoticons template textcolor paste textcolor moxiemanager"
                        ],

                        toolbar1: "newdocument | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | styleselect formatselect fontselect fontsizeselect",
                        toolbar2: "cut copy paste | searchreplace | bullist numlist | outdent indent blockquote | undo redo | link unlink anchor image media code | inserttime preview | forecolor backcolor",
                        toolbar3: "table | hr removeformat | subscript superscript | charmap emoticons | print fullscreen | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft",

                        menubar: false,
                        toolbar_items_size: 'small',
                        forced_root_block : false,
                        relative_urls: false,


                        content_css : "/www/css/style.css"/*,
                        style_formats: [
                            {title: 'Bold text', inline: 'b'},
                            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                            {title: 'Example 1', inline: 'span', classes: 'example1'},
                            {title: 'Example 2', inline: 'span', classes: 'example2'},
                            {title: 'Table styles'},
                            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                        ],

                        templates: [
                            {title: 'Test template 1', content: 'Test 1'},
                            {title: 'Test template 2', content: 'Test 2'}
                        ]*/
                    };

                tinymce.init(tinyMceInit);

                $(tinyMceSelector).tinymce(tinyMceInit);

            </script>
        <?php endif; ?>

        <?= $js ?>
    </head>
    <body>
        <?php if(isset($_GET['restore_password']) && isset($_GET['token'])):?>
            <script>
                loadModal('/ll/restore_password/<?=$_GET['token'];?>');
            </script>
        <?php endif;?>
        <?=CMS_SystemMessages::out();?>
        <div class="wrapper">
            <?=$this->getContent('header');?>

            <?=$this->getContent('main');?>
            <div class="footer-push"></div>
        </div>
        <div class="footer-wrapper">
            <?=$this->getContent('footer');?>
        </div>

        <?= $this->get('users')->control('menu') ?>
        <?php $this->get('cfg')->analytics(); ?>
    </body>
</html>
