<div id="user-panel">
    <div class="panel">
        <?php if($this->get('users')->is()) : ?>
            <a href="/ll/logout"><?=$this->get('langs')->text('site','login:logout');?></a>
        <?php else : ?>
            <form action="/ll/login" method="post">
                <label>
                    <?=$this->get('langs')->text('site','login:username');?><br />
                    <input type="text" name="user[username]" />
                </label>
                <label>
                    <?=$this->get('langs')->text('site','login:password');?><br />
                    <input type="password" name="user[password]" />
                </label>
                <div <?=$this->get('users')->control('phrase','site.login:submit');?>>
                    <input type="submit" value="<?=$this->get('langs')->text('site','login:submit',null,false);?>" />
                </div>
            </form>
        <?php endif; ?>
    </div>
    <div class="marker" onclick="$(this).toggleClass('close');$('#user-panel .panel').toggleClass('open');"></div>
</div>