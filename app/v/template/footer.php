	<?php if(!isset($admin)) : ?>
        <div class="delivery-info-holder">
            <div class="delivery-content">
            <div class="delivery-info">
                <div class="delivery-info-text">
                <?=$this->get('langs')->text('admin','delivery_info_text:delivery');?>
                </div>
            </div>
            <div class="delivery-info">
                <div class="delivery-info-pic"><img src="/www/images/1km.png"></div>
                <div class="delivery-info-text">
                <?=$this->get('langs')->text('admin','delivery_info_text:rate');?>
                </div>
            </div>
            <div class="delivery-info">
                <div class="delivery-info-pic"><img src="/www/images/girja.png"></div>
                <div class="delivery-info-text">
                <?=$this->get('langs')->text('admin','delivery_info_text:weight');?>
                </div>
            </div>
            <div class="delivery-info">
                <div class="delivery-info-pic"><img src="/www/images/sizes.png"></div>
                <div class="delivery-info-text">
                <?=$this->get('langs')->text('admin','delivery_info_text:sizes');?>
                </div>
            </div>
            </div>
        </div>
    <?php else: ?>
        <div style="height:108px;"></div>
    <?php endif; ?>
	<div class="footer-holder">
	    <div class="footer">
            <div class="footer-link"><?=$this->get('langs')->text('admin','footer:rules');?> </div>
            <div class="footer-link"><?=$this->get('langs')->text('admin','footer:qna');?> </div>
            <div class="footer-link"><?=$this->get('langs')->text('admin','footer:contacts');?></div>
            <div class="footer-link"><?=$this->get('langs')->text('admin','footer:link_1');?></div>
            <div class="footer-link"><?=$this->get('langs')->text('admin','footer:link_2');?></div>
            <div class="footer-social">
                <a href="<?=$this->get('cfg')->read('social.twitter_link');?>" target="_blank" <?=$this->get('users')->control('config','social.twitter_link');?>><img src="/www/images/social/twitter.jpg" /></a>
                <a href="<?=$this->get('cfg')->read('social.vkontakte_link');?>" target="_blank" <?=$this->get('users')->control('config','social.vkontakte_link');?>><img src="/www/images/social/vk.jpg" /></a>
                <a href="<?=$this->get('cfg')->read('social.facebook_link');?>" target="_blank" <?=$this->get('users')->control('config','social.facebook_link');?>><img src="/www/images/social/fb.jpg" /></a>
            </div>
            <div class="footer-logo"><img src="/www/images/eralogo.png" /></div>
	    </div>
	</div>
  
