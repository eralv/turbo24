<div class="header">
    <div class="inner-header relative">
        <div class="logo-holder">
            <a href="/ll/"><img src="/www/images/logo.png"></a>
        </div>
        <div class="info-holder">
            <div class="info-top-holder">
                <div class="info-top">
                    <div class="lang-holder">
                        <?php foreach ($this->get('langs')->listLangs() as $lang) : ?>
                        <div class="lang<?= $this->get('langs')->getLang() == $lang ? ' active"' : ' "'; ?>><a href="
                        /<?= $lang; ?>/"><?= $this->get('langs')->text('site', 'label:lang-' . $lang); ?></a></div>
                    <?php endforeach; ?>

                </div>
                <?php if ($this->get('users')->is()) : ?>
                    <a href="/ll/profile"><span class="login"><img class="login-pick" src="/www/images/social/user.png" alt="userpic"/>
                            <?= $this->get('langs')->text('site', 'login:profile'); ?>
                    </span></a>
                    <a href="/ll/logout"><span class="login"><img class="login-pick" src="/www/images/social/lock.png" alt="userpic"/>
                            <?= $this->get('langs')->text('site', 'login:logout'); ?>
                    </span></a>
                <?php endif; if(!$this->get('users')->is() || $this->get('users')->admin()) : ?>
                    <div id="login">
                        <span class="login"><img class="login-pick" src="/www/images/social/unlock.png" alt="login"/>
                            <?= $this->get('langs')->text('site', 'login:login'); ?></span>
                        <div class="form-order-login" id="login-user">
                            <div class="form-row">
                                <form method="post" action="/ll/auth/login">
                                    <div class="input-heading"><?= $this->get('langs')->text('site', 'login:email'); ?></div>
                                    <div><input type="text" name="user[username]"></div>

                                    <div class="input-heading"><?= $this->get('langs')->text('site', 'login:password'); ?></div>
                                    <div><input type="password" name="user[password]"></div>

                                    <div class="button-holder">
                                        <a class="input-heading" data-modal="/ll/restore_password" href="#"><?= $this->get('langs')->text('site', 'login:restore_password'); ?></a>
                                        <span <?= $this->get('users')->control('phrase', 'site.login:submit_login'); ?>>
                                            <button type="submit" class="submit-button orange-grad submit-button-login">
                                                <?= $this->get('langs')->text('site', 'login:submit_login',null,false); ?>
                                            </button>
                                        </span>
                                    </div>
                                </form>

                                <hr/>

                                <div class="form-row social_login">
                                    <span><?=$this->get('langs')->text('site','login:login_social');?></span>
                                    <div>
                                        <a href="/ll/auth/twitter?link"><img src="/www/images/social/twitter.png"></a>
                                        <a href="/ll/auth/draugiem?link"><img src="/www/images/social/draugiem.png"></a>
                                        <a href="/ll/auth/facebook?link"><img src="/www/images/social/fbbig.png"></a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div id="register">
                        <span class="login"><img class="login-pick" src="/www/images/social/user.png" alt="userpic"/>
                            <?= $this->get('langs')->text('site', 'register:register'); ?></span>
                        <div id="reg-user" class="form-order-register">
                            <div class="form-row">
                                <form action="/ll/auth/register" method="post">
                                    <div class="input-heading"><?= $this->get('langs')->text('site', 'register:name_companyname'); ?></div>
                                    <div><input type="text" name="register[name]"></div>

                                    <div class="input-heading"><?= $this->get('langs')->text('site', 'register:surname'); ?></div>
                                    <div><input type="text" name="register[surname]"></div>

                                    <div class="input-heading"><?= $this->get('langs')->text('site', 'register:email'); ?></div>
                                    <div><input type="text" name="register[email]"></div>

                                    <div class="input-heading"><?= $this->get('langs')->text('site', 'register:password'); ?></div>
                                    <div><input type="password" name="register[password]"></div>

                                    <div class="input-heading"><?= $this->get('langs')->text('site', 'register:repeat_password'); ?></div>
                                    <div><input type="password" name="register[repeat_password]"></div>

                                    <div class="input-heading"><a href="#"><?= $this->get('langs')->text('site', 'register:rules'); ?></a>
                                    </div>
                                    <div class="input-heading">
                                        <input type="checkbox" id="confirm" name="register[confirm]">
                                        <label for="confirm">
                                            <span></span>
                                            <div class="form-down-text"><?= $this->get('langs')->text('site', 'register:rule_confirmation'); ?></div>
                                        </label>
                                    </div>
                                    <div class="button-holder" <?= $this->get('users')->control('phrase', 'site.login:submit_login'); ?>>
                                        <button type="submit" class="submit-button orange-grad">
                                            <?= $this->get('langs')->text('site', 'register:submit_register', null, false); ?>
                                        </button>
                                    </div>
                                </form>

                                <hr/>

                                <div class="form-row social_login">
                                    <span><?=$this->get('langs')->text('site','register:register_social');?></span>
                                    <div>
                                        <a href="/ll/auth/twitter?link"><img src="/www/images/social/twitter.png"></a>
                                        <a href="/ll/auth/draugiem?link"><img src="/www/images/social/draugiem.png"></a>
                                        <a href="/ll/auth/facebook?link"><img src="/www/images/social/fbbig.png"></a>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                <?php endif; ?>


            </div>
        </div>
        <div class="info-bottom">
            <?=$this->get('langs')->text('site','header:call_us_text');?> <b><?=$this->get('langs')->text('site','header:call_us_phone');?></b>
        </div>
    </div>

</div>
</div>
<div class="col col-2">
    <div class="navigation">
        <ul <?= $this->get('users')->admin() ? 'data-sortable="tree"' : '' ?>>
            <?php foreach ($tree->getMenu('top') as $item) : ?>
                <li <?= $this->get('users')->admin() ? 'data-id="' . $item['id'] . '"' : '' ?> <?= $tree->is_active($item['id']) ? 'class="active"' : ''; ?> <?= $this->get('users')->control('menu', $item['id']); ?>>
                    <a href="<?= $tree->linkById($item['id']); ?>"><?= $item['title']; ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>


