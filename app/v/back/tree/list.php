<div class="content content-bg" id="adminContent">

    <h3>
        <a href="/ll/admin/tree/add">
            <?= $this->get('langs')->text('admin', 'tree:add'); ?>
        </a>
    </h3>
    <hr/>

    <table>
        <tr>
            <th></th>

        </tr>
        <tbody data-sortable="tree">
            <?php
            $t1 = $tree->byParent(0, true);
            if (!empty($t1)) {
                //var_dump($t1);
                foreach ($t1 as $k1 => $v1) // Parent Menus
                {
                    echo '<tr data-id="'.$v1['id'].'">';
                    echo '<td style="padding-left:0px;padding-bottom:15px;padding-top:10px;">
                                    <span ' . $this->get('users')->control('menu', $v1['id']) . '>
                                        <a href="/ll/' . $v1['link'] . '">' . $v1['title'] . '</a>
                                    </span>
                                </td>';

                    echo '<td style="font-size:13px;color:red;">' . (!in_array($v1['id'], array(1, 66666, 99999)) ? '<a onClick="if(!confirm(\'Delete?\')) return false" href="/ll/admin/tree/delete/' . $v1['id'] . '">Delete</a>' : '') . '</td>';


                    $t2 = $tree->byParent($k1, $this->get('langs')->defLang()); //submenus
                    if (!empty($t2)) {

                        foreach ($t2 as $k2 => $v2) {
                            echo '<tr>';
                            echo '<td style="padding-left:50px;"><span ' . $this->get('users')->control('menu', $v2['id']) . '><a href="/ll/' . $v2['link'] . '">' . $v2['title'] . '</span></a></td>';

                            echo '<td style="padding-left:50px;font-size:13px;color:red;"><a onClick="if(!confirm(\'Delete?\')) return false" href="/ll/admin/tree/delete/' . $v2['id'] . '">[Delete child]</a></td>';
                            if ($v1['default_child'] != $v2['id'])
                                echo '<td style="padding-left:10px;font-size:13px;color:red;"><a onClick="if(!confirm(\'Make this page as main?\')) return false" href="/ll/admin/tree/makemain/' . $v2['parent'] . '/' . $v2['id'] . '">[Make main]</a></td>';

                            $t3 = $tree->byParent($k2, $this->get('langs')->defLang());
                            if (!empty($t3)) {

                                foreach ($t3 as $k3 => $v3) {
                                    echo '<tr>';
                                    echo '<td style="padding-left:100px;"><span ' . $this->get('users')->control('menu', $v3['id']) . '><a href="/ll/' . $v3['link'] . '">' . $v3['title'] . '</span></a></td>';

                                    echo '<td style="font-size:13px;color:red;"><a onClick="if(!confirm(\'Delete?\')) return false" href="/ll/admin/tree/delete/' . $v3['id'] . '">Delete</a></td>';

                                    echo '</tr>';
                                }
                            }
                            echo '</tr>';
                        }
                    }
                    echo '</tr>';
                }
            }
            ?>
        </tbody>

    </table>

</div>