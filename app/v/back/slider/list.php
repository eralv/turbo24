<?php
/**
 * @var CMS_Slider[] $slides
 */
?>
<div class="page" id="adminContent">

    <h3>
        <a href="/ll/admin/slider/add/">
            <?=$this->get('langs')->text('admin','slider:add');?>
        </a>
    </h3>
    <hr />

    <table>
        <tr>
            <th style="width:20%;"><?=$this->get('langs')->text('admin','slider:img');?></th>
            <th><?=$this->get('langs')->text('admin','slider:img-name');?></th>
            <th><?=$this->get('langs')->text('admin','slider:options');?></th>
        </tr>
        <tbody data-sortable="slider">
            <?php foreach($slides as $slide) : ?>
                <tr data-id="<?=$slide->id;?>">
                    <td>
                        <img style="width:100px;height:auto;" src="<?=$slide->getImgPath('thumb');?>" alt="<?=$slide->title;?>" />
                    </td>
                    <td><?=$slide->title;?></td>
                    <td>
                        <a href="/ll/admin/slider/edit/<?=$slide->id;?>"><?=$this->get('langs')->text('admin','option:edit');?></a>
                        | <a href="/ll/admin/slider/delete/<?=$slide->id;?>"
                             data-confirm="<?=$this->get('langs')->text('admin','confirm:delete-slide',true,false);?>"><?=$this->get('langs')->text('admin','option:delete');?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>