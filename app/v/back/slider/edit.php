<?php
/**
 * @var CMS_Slider $slide
 */
?>
<div class="page" id="adminContent">

    <form action="/ll/admin/slider/edit/<?=$slide->id;?>" method="post" enctype="multipart/form-data">
        <div>
            <?php if(!empty($slide->slide)) : ?>
                <img src="<?=CMS_Uploads::getPath($slide->slide,'slider');?>" alt="Slide image" />
            <?php endif; ?>
        </div>
        <fieldset>

            <div class="block-tabs-4">
                <ul class="tabs-list">
                    <?php foreach($this->get('langs')->listLangs() as $lang):?>
                        <li class="<?=$lang?><?=($lang==$this->get('langs')->getLang()?' active':'')?>"><?=mb_strtoupper($lang)?></li>
                    <?php endforeach;?>
                </ul>
                <div class="block-content">

                    <?php foreach($this->get('langs')->listLangs() as $lang):?>

                        <div class="tab-block-1 <?=$lang?>" style="display: <?=($lang==Engine_Controller::get('langs')->getLang()?'block':'none')?>;">
                            <br /><textarea class="textEditor" name="slider[text][<?=$lang;?>]" id="description_<?=$lang;?>"><?= $slide->getText('text',$lang)?></textarea><br />
                        </div>

                    <?php endforeach;?>
                </div>
            </div>

            <label>
                <input type="text" name="slider[title]" value="<?=$slide->title;?>" />
            </label>

            <label>
                <input type="text" name="slider[link]" value="<?=$slide->link;?>" />
            </label>

            <label>
                <input type="file" name="slide" />
            </label>

        </fieldset>
        <input type="submit" value="<?=$this->get('langs')->text('admin','save',true,false);?>" />
    </form>

</div>