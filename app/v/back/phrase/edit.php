
		<h1 <?= $this->get('users')->control('phrase','admin.phrase:edit_title') ?>><?= $this->get('db')->phrase('admin.phrase:edit_title') ?></h1>
		
		<input type="text" disabled="disabled" value="<?= $alias;?>" /><br /><br />
		
		<form action="<?= $this->get('uri')->genUri() ?>" method="post">
			<input type="hidden" name="save_phrase" value="1">
                     
			
			<div class="block-tabs-4">
				<ul class="tabs-list">
					<?php foreach($this->get('langs')->listLangs() as $lang):?>
						<li class="<?=$lang?> <?=($lang==$this->get('langs')->getLang()?'active':'')?>"><?=mb_strtoupper($lang)?></li>
					<?php endforeach;?>
				</ul>
				<div class="block-content">

				<?php foreach($aliases as $lang => $a):?>

					<div class="tab-block-1 <?=$lang?>" style="display: <?=($lang==Engine_Controller::get('langs')->getLang()?'block':'none')?>;">
						<br /><textarea class="textEditor" name="<?=$lang?>[phrase]" id="description_<?=$lang?>_<?=$alias;?>"><?= $a?></textarea><br />
					</div>
					
				<?php endforeach;?>
				</div>
			</div>
			<input type="submit" class="admin-submit" value="SAVE" />
		</form>
