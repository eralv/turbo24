<?php
/**
 * @var array[] $list
 */
?>
<div class="col col-2-3" id="adminContent">

    <h3>
        <a href="/ll/admin/users/add/">
            <?=$this->get('langs')->text('admin','user:add');?>
        </a>
    </h3>
    <div>
	<form method="post" action="">
	    <table class="user-search">
		<tr>
		    <td><?=$this->get('langs')->text('admin','user:search_by_name');?></td>
		    <td><?=$this->get('langs')->text('admin','user:search_by_surname');?></td>
		    <td><?=$this->get('langs')->text('admin','user:search_by_company');?></td>
		    <td><?=$this->get('langs')->text('admin','user:search_by_juridic');?></td>
		    <td><?=$this->get('langs')->text('admin','user:search_by_rate');?></td>
		    <td></td>
		</tr>
		<tr>
		    <td><input type="text" name="search[name]" <?=setIf_isset($name,$_POST['search']['name']) ? 'value="'.$name.'"' : '';?> /></td>
		    <td><input type="text" name="search[surname]" <?=setIf_isset($surname,$_POST['search']['surname']) ? 'value="'.$surname.'"' : '';?> /></td>
		    <td><input type="text" name="search[company]" <?=setIf_isset($company,$_POST['search']['company']) ? 'value="'.$company.'"' : '';?> /></td>
		    <td>
                <select name="search[juridic]">
                    <option value="-1" <?=isset($_POST['search']['juridic'])&&$_POST['search']['juridic']<0?'selected':'';?>><?=$this->get('langs')->text('admin','user:is_juridic');?></option>
                    <option value="0" <?=isset($_POST['search']['juridic'])&&$_POST['search']['juridic']==0?'selected':'';?>><?=$this->get('langs')->text('admin','user:non_juridic');?></option>
                    <option value="1" <?=isset($_POST['search']['juridic'])&&$_POST['search']['juridic']>0?'selected':'';?>><?=$this->get('langs')->text('admin','user:juridic');?></option>
                </select>
		    </td>
		    <td>
                <select name="search[rate]">
                    <option value="-1" <?=isset($_POST['search']['rate'])&&$_POST['search']['rate']<0?'selected':'';?>><?=$this->get('langs')->text('admin','user:by_rate');?></option>
                    <option value="0" <?=isset($_POST['search']['rate'])&&$_POST['search']['rate']==0?'selected':'';?>><?=$this->get('langs')->text('admin','user:default_rate');?></option>
                    <?php foreach($rate_list as $rt) : ?>
                        <option value="<?=$rt['id'];?>" <?=isset($_POST['search']['rate'])&&$_POST['search']['rate']==$rt['id']?'selected':'';?>><?=$rt['name'];?></option>
                    <?php endforeach; ?>
                </select>
		    </td>
		    <td><input type="submit"></td>
		</tr>
	    </table>
	</form>
    </div>
    <hr />

    <table>
        <tr>
            <th><?=$this->get('langs')->text('admin','user:rate');?></th>
            <th><?=$this->get('langs')->text('admin','user:name');?></th>
            <th><?=$this->get('langs')->text('admin','user:company_name');?></th>
            <th><?=$this->get('langs')->text('admin','user:phone');?></th>
            <th><?=$this->get('langs')->text('admin','user:fact_address');?></th>
            <th><?=$this->get('langs')->text('admin','user:email');?></th>
            <th style="width:100px;"><?=$this->get('langs')->text('admin','user:order_count');?></th>
            <th><?=$this->get('langs')->text('admin','user:status');?></th>
            <th style="width:110px;"><?=$this->get('langs')->text('admin','user:options');?></th>

        </tr>
        <?php foreach($list as $user) : ?>
            <tr>
                <td>
                    <form action="" method="post" name="rate[<?=$user['id']?>]">
                        <select name="rate[rate]" class="rate">
                            <option value="0" <?=0==$user['rate']?'selected':''; ?>><?=$this->get('langs')->text('admin','user:default_rate');?></option>
                            <?php foreach ($rate_list as $rate):?>
                            <option value="<?=$rate['id']?>" <?=$rate['id']==$user['rate']?'selected':''; ?>><?=$rate['name']?></option>
                            <?php endforeach;?>
                        </select>
                        <input type="hidden" name="rate[id]" value="<?=$user['id']?>" />
                    </form>
                </td>
                <td><?=$user['name'];?> <?=$user['surname'];?></td>
                <td><?=$user['company_name'];?></td>
                <td><?=$user['phone'];?></td>
                <td><?=$user['juridic']?$user['fact_address']:$user['address'];?></td>
                <td><?=$user['email'];?></td>
                <td><?=$user['order_count'];?></td>
                <td>
                    <form action="" method="post" name="status[<?=$user['id']?>]">
                        <select name="status[status]" class="status">
                            <option value="0" <?=$user['status']=='0'?'selected':''?>><?=$this->get('langs')->text('admin','user:status_inactive',null,false);?></option>
                            <option value="1" <?=$user['status']=='1'?'selected':''?>><?=$this->get('langs')->text('admin','user:status_active',null,false);?></option>
                        </select>
                        <input type="hidden" name="status[id]" value="<?=$user['id']?>" />
                    </form>
                </td>
                <td>
                    <?php if(($user['rules']==Engine_Users::SUPERUSER && $this->get('users')->su()) || $user['rules']!=Engine_Users::SUPERUSER) : ?>
                        <a href="/ll/admin/users/edit/<?=$user['id'];?>"><?=$this->get('langs')->text('admin','option:edit');?></a> |
                        <a href="/ll/admin/users/delete/<?=$user['id'];?>"
                           data-confirm="<?=$this->get('langs')->text('admin','confirm:delete-user',true,false);?>"><?=$this->get('langs')->text('admin','option:delete');?></a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <center>
        <?php $page=0;
        while ($page++ < $num_pages): ?>
            <?php if ($page == $cur_page): ?>
                <b><?=$page?></b>
            <?php else: ?>
                <a href="?page=<?=$page?>"><?=$page?></a>
            <?php endif ?>
        <?php endwhile ?>
    </center>
    <script>
        $('.status').on('change',function(){
            $(this).parents('form:first').submit();
        });
        $('.rate').on('change',function(){
            $(this).parents('form:first').submit();
        });
    </script>

</div>