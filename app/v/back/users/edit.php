<?php
/**
 * @var array[] $item
 */
?>
<div class="content">
    <div class="edit-field">
	<?php if($item && $item['rules']==Engine_Users::SUPERUSER && !$this->get('users')->su()) : ?>
	    <h3><?=$this->get('langs')->text('admin','user-edit:cant_edit');?></h3>
	<?php elseif($item) : ?>
	    <form action="" method="post" autocomplete="off">
		<input type="hidden" name="user[type_add]" value="admin" />
		<label>
		    <?=$this->get('langs')->text('admin','user-edit:username');?><br />
		    <input type="text" name="user[name]" value="<?=$item['username'];?>" />
		</label>
		<label>
		    <?=$this->get('langs')->text('admin','user-edit:username');?><br />
		    <input type="text" name="user[surname]" value="<?=$item['username'];?>" />
		</label>
		<label>
		    <?=$this->get('langs')->text('admin','user-edit:password');?><br />
		    <input type="password" name="user[password]" />
		</label>
		<label>
		    <?=$this->get('langs')->text('admin','user-edit:email');?><br />
		    <input type="text" name="user[email]" value="<?=$item['email'];?>" />
		</label>
		<label>
		    <?=$this->get('langs')->text('admin','user-edit:rules');?><br />
		    <select name="user[rules]">
			<?php foreach($this->get('users')->rules as $rule=>$label) :
			    if(($rule==Engine_Users::SUPERUSER && $item['rules']==Engine_Users::SUPERUSER) || $rule!=Engine_Users::SUPERUSER) : ?>
			    <option <?=$item['rules']==$rule?'selected':'';?> value="<?=$rule;?>"><?=$this->get('langs')->text('admin','user-edit:rule-'.$label,true,false);?></option>
			    <?php endif;
			endforeach; ?>
		    </select>
		</label>
		<input type="submit" class="orange-button orange-grad" value="<?=$this->get('langs')->text('admin','save',true,false);?>" />
	    </form>
	<?php else : ?>
	    <h3><?=$this->get('langs')->text('admin','user-edit:not_found');?></h3>
	<?php endif; ?>
    </div>
</div>