<?php
/**
 * @var CMS_News $item
 */
?>
<form action="" method="post" enctype="multipart/form-data" class="content content-bg">
    <fieldset>
        <h3><?=$this->get('langs')->text('admin','news_edit:title',true);?></h3>

        <?php if($img = $item->getImage()): ?>
            <img src="<?=$img;?>" style="max-width:200px;" /><br />
        <?php endif; ?>
        <label>
            <?=$this->get('langs')->text('admin','news_add:image');?><br />
            <input type="file" name="image" />
        </label><br />

        <div class="block-tabs-4">
            <ul class="tabs-list">
                <?php foreach($this->get('langs')->listLangs() as $lang):?>
                    <li class="<?=$lang?> <?=($lang==$this->get('langs')->getLang()?'active':'')?>"><?=mb_strtoupper($lang)?></li>
                <?php endforeach;?>
            </ul>
            <div class="block-content">
                <?php foreach($this->get('langs')->listLangs() as $lang):?>
                    <div class="tab-block-1 <?=$lang?>" style="display: <?=($lang==$this->get('langs')->getLang()?'block':'none')?>;">
                            <label>
                                <?=$this->get('langs')->text('admin','news_edit:phrase-title',true);?>
                                <input type="text" name="news[title][<?=$lang?>]" value="<?=$item->getText('title',$lang)?>" />
                            </label>
                            <label>
                                <?=$this->get('langs')->text('admin','news_edit:phrase-text',true);?>
                                <textarea class="textEditor" name="news[news][<?=$lang?>]"><?=$item->getText('news',$lang)?></textarea>
                            </label>
                    </div>
                <?php endforeach;?>
            </div>
        </div>

    </fieldset>
    <hr />
  
    <div <?=$this->get('users')->control('phrase','admin.save');?>>
        <input type="submit" value="<?=$this->get('langs')->text('admin','save',true,false);?>" />
    </div>
</form>
