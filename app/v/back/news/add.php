<?php
/**
 * @var String[] $phrases
 */
?>
<form action="" method="post" enctype="multipart/form-data">
    <fieldset>
        <h3><?=$this->get('langs')->text('admin','news_add:title',true);?></h3>

        <label>
            <?=$this->get('langs')->text('admin','news_add:image');?><br />
            <input type="file" name="image" />
        </label>

        <div class="block-tabs-4">
            <ul class="tabs-list">
                <?php foreach($this->get('langs')->listLangs() as $lang):?>
                    <li class="<?=$lang?> <?=($lang==$this->get('langs')->getLang()?'active':'')?>"><?=mb_strtoupper($lang)?></li>
                <?php endforeach;?>
            </ul>
            <div class="block-content">
                <?php foreach($this->get('langs')->listLangs() as $lang):?>
                    <div class="tab-block-1 <?=$lang?>" style="display: <?=($lang==$this->get('langs')->getLang()?'block':'none')?>;">
                            <label>
                                <?=$this->get('langs')->text('admin','news_add:phrase-title',true);?>
                                <input type="text" name="news[title][<?=$lang?>]" />
                            </label>
                            <label>
                                <?=$this->get('langs')->text('admin','news_add:phrase-text',true);?>
                                <textarea class="textEditor" name="news[news][<?=$lang?>]"></textarea>
                            </label>
                    </div>
                <?php endforeach;?>
            </div>
        </div>

    </fieldset>
    <hr />
  
    <div <?=$this->get('users')->control('phrase','admin.save');?>>
        <input type="submit" value="<?=$this->get('langs')->text('admin','save',true,false);?>" />
    </div>
</form>