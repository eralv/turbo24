<?php
/**
 * @var CMS_News[] $list
 */

?>
<div class="content">
    <div class="col col-2-3" id="adminContent">

	<h3>
	    <a href="/ll/admin/news/add/">
		<?=$this->get('langs')->text('admin','news:add');?>
	    </a>
	</h3>
	<hr />

	<table>
	    <tr>
		<th><?=$this->get('langs')->text('admin','news:title');?></th>
		<th><?=$this->get('langs')->text('admin','news:date');?></th>
		<?php if($this->get('users')->rule()==Engine_Users::SUPERUSER) : ?>
		    <th><?=$this->get('langs')->text('admin','news:options');?></th>
		<?php endif; ?>
	    </tr>
	    <?php foreach($list as $item) : ?>
		<tr>
		    <td><?=$item->title;?></td>
		    <td><?=date("Y-m-d H:i:s",$item->date)?></td>
		    <td>
			<?php if(($this->get('users')->admin())) : ?>
			    <a href="/ll/admin/news/edit/<?=$item->id;?>"><?=$this->get('langs')->text('admin','news:edit');?></a> |
			    <a href="/ll/admin/news/delete/<?=$item->id;?>"
			       data-confirm="<?=$this->get('langs')->text('admin','confirm:delete-news',true,false);?>"><?=$this->get('langs')->text('admin','news:delete');?></a>
			<?php endif; ?>
		    </td>
		</tr>
	    <?php endforeach; ?>
	</table>

    </div>
</div>