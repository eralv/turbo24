<?php
/**
 * @var array[] $list
 */
?>
<div class="content content-bg">
    <div class="col col-2-3" id="adminContent">

	<h3>
	    <a href="/ll/admin/rates/add/">
		<?=$this->get('langs')->text('admin','rates:add');?>
	    </a>
	</h3>


    <form action="" method="post">
        <table style="width:auto;">
            <tr>
                <td>
                    <?=$this->get('langs')->text('admin','rates:filter-name');?>
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="text-align:left;">
                    <input type="text" name="search[name]" />
                </td>
                <td>
                    <input type="submit" />
                </td>
            </tr>
        </table>
    </form>

        <hr />

	<table>
	    <tr>
		<th><?=$this->get('langs')->text('admin','rates:name');?></th>
		<th><?=$this->get('langs')->text('admin','rates:value');?></th>
		<?php if($this->get('users')->rule()==Engine_Users::SUPERUSER) : ?>
		    <th><?=$this->get('langs')->text('admin','rates:options');?></th>
		<?php endif; ?>
	    </tr>
	    <?php foreach($list as $rate) : ?>
		<tr>
		    <td><?=$rate['name'];?></td>
		    <td><?=$rate['value'];?></td>
		    <td>
			<?php if(($this->get('users')->rule()==Engine_Users::SUPERUSER)) : ?>
			    <a href="/ll/admin/rates/edit/<?=$rate['id'];?>"><?=$this->get('langs')->text('admin','option:edit');?></a> |
			    <a href="/ll/admin/rates/delete/<?=$rate['id'];?>"
			       data-confirm="<?=$this->get('langs')->text('admin','confirm:delete-rate',true,false);?>"><?=$this->get('langs')->text('admin','option:delete');?></a>
			<?php endif; ?>
		    </td>
		</tr>
	    <?php endforeach; ?>
	</table>

    </div>
</div>