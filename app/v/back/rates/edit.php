<?php
/**
 * @var array[] $item
 */
	//var_dump($item);die();
?>

<div class="content">
    <div class="edit-field content-bg">
    <?php if($item && $this->get('users')->rule()==Engine_Users::SUPERUSER) : ?>
        <form action="" method="post" autocomplete="off">
	    <input type="hidden" name="rate[id]" value="<?=$item['id'];?>" />
            <label>
                <?=$this->get('langs')->text('admin','rates-edit:name');?><br />
                <input type="text" name="rate[name]" value="<?=$item['name'];?>" /><br />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','rates-edit:surname');?><br />
                <input type="text" name="rate[value]" value="<?=$item['value'];?>"/><br />
            </label>
            
            <input type="submit" class="orange-button orange-grad" value="<?=$this->get('langs')->text('admin','save',true,false);?>" /><br />
        </form>
    <?php else : ?>
        <h3><?=$this->get('langs')->text('admin','rates-edit:not_found');?></h3>
    <?php endif; ?>
    </div>
</div>