<?php
/**
 * @var array[] $item
 */
?>
<div class="content">
    <div class="edit-field content-bg">
    <?php if($this->get('users')->rule()==Engine_Users::SUPERUSER) : ?>
        <h3><?=$this->get('langs')->text('admin','rates-add:cant_edit');?></h3>

        <form action="" method="post" autocomplete="off">
            <label>
                <?=$this->get('langs')->text('admin','rates-add:name');?><br />
                <input type="text" name="rate[name]" value="<?=$item['name'];?>" /><br />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','rates-add:value');?><br />
                <input type="text" name="rate[value]" value="<?=$item['value'];?>"/><br />
            </label>
            
            <input type="submit" class="orange-button orange-grad" value="<?=$this->get('langs')->text('admin','save',true,false);?>" /><br />
        </form>
    <?php else : ?>
        <h3><?=$this->get('langs')->text('admin','rates-add:not_found');?></h3>
    <?php endif; ?>
    </div>
</div>