<form action="/ll/admin/config" method="post">
    <fieldset>
        <label>
            <?=$this->get('langs')->text('admin','config:'.$config,true);?>
            <input type="text" name="config[<?=$config;?>]" value="<?=$this->get('cfg')->read($config);?>" />
        </label>
    </fieldset>
    <div <?=$this->get('users')->control('phrase','admin.save');?>>
        <input type="submit" value="<?=$this->get('langs')->text('admin','save',true,false);?>" />
    </div>
</form>