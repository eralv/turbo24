<?php
/**
 * @var String[] $phrases
 */
?>
<div class="content content-bg">
    <form action="" method="post">
        <fieldset>
            <h3><?=$this->get('langs')->text('admin','config:title',true);?></h3>

            <div class="block-tabs-4">
                <ul class="tabs-list">
                    <?php foreach($this->get('langs')->listLangs() as $lang):?>
                        <li class="<?=$lang?> <?=($lang==$this->get('langs')->getLang()?'active':'')?>"><?=mb_strtoupper($lang)?></li>
                    <?php endforeach;?>
                </ul>
                <div class="block-content">
                    <?php foreach($this->get('langs')->listLangs() as $lang):?>
                        <div class="tab-block-1 <?=$lang?>" style="display: <?=($lang==$this->get('langs')->getLang()?'block':'none')?>;">

                            <table style="width:100%">
                                <?php foreach($phrases as $phrase=>$type) : ?>
                                    <tr>
                                        <td style="width:30%">
                                            <label for="site.<?=$phrase;?>.<?=$lang?>">
                                                <?=$this->get('langs')->text('admin','config:phrase-'.$phrase,true);?>
                                            </label>
                                        </td>
                                        <td style="text-align:left">
                                            <?php switch($type):
                                                case 'text': ?>
                                                    <textarea id="site.<?=$phrase;?>.<?=$lang?>" name="phrase[<?=$lang?>][site.<?=$phrase;?>]"><?=$this->get('langs')->text('site',$phrase,$lang,false);?></textarea>
                                                    <?php break;
                                                case 'field':
                                                default: ?>
                                                    <input id="site.<?=$phrase;?>.<?=$lang?>" name="phrase[<?=$lang?>][site.<?=$phrase;?>]" type="text" value="<?=$this->get('langs')->text('site',$phrase,$lang,false);?>" />
                                                <?php endswitch; ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </table>

                        </div>
                    <?php endforeach;?>
                </div>
            </div>

        </fieldset>
        <hr />
        <fieldset>

            <table style="width:100%">
                <tr>
                    <td style="width:30%">
                        <label for="site.config.analytics">
                            <?=$this->get('langs')->text('admin','config:analytics',true);?>
                        </label>
                    </td>
                    <td style="text-align:left">
                        <input id="site.config.analytics" type="text" name="config[analytics]" value="<?=$this->get('cfg')->read('analytics');?>" />
                    </td>
                </tr>
                <tr>
                    <td style="width:30%">
                        <label for="site.config.default_rate">
                            <?=$this->get('langs')->text('admin','config:default_rate',true);?>
                        </label>
                    </td>
                    <td style="text-align:left">
                        <input id="site.config.default_rate" type="text" name="config[default_rate]" value="<?=$this->get('cfg')->read('default_rate');?>" />
                    </td>
                </tr>
                <tr>
                    <td style="width:30%">
                        <label for="site.config.default_rate_minute">
                            <?=$this->get('langs')->text('admin','config:default_rate_minute',true);?>
                        </label>
                    </td>
                    <td style="text-align:left">
                        <input id="site.config.default_rate_minute" type="text" name="config[default_rate_minute]" value="<?=$this->get('cfg')->read('default_rate_minute');?>" />
                    </td>
                </tr>
                <tr>
                    <td style="width:30%">
                        <label for="site.config.default_rate_add">
                            <?=$this->get('langs')->text('admin','config:default_rate_add',true);?>
                        </label>
                    </td>
                    <td style="text-align:left">
                        <input id="site.config.default_rate_add" type="text" name="config[default_rate_add]" value="<?=$this->get('cfg')->read('default_rate_add');?>" />
                    </td>
                </tr>
                <tr>
                    <td style="width:30%">
                        <label for="site.config.default_language">
                            <?=$this->get('langs')->text('admin','config:default_language',true);?>
                        </label>
                    </td>
                    <td style="text-align:left">
                        <select name="config[default_language]">
                            <?php foreach($this->get('langs')->listLangs() as $lang) : ?>
                                <option value="<?=$lang;?>" <?=($lang==$this->get('langs')->defLang()?'selected':'')?>><?=$this->get('langs')->text('admin','label:language-'.$lang,true,false);?></option>
                            <?php endforeach ?>
                        </select>
                    </td>
                </tr>
            </table>

        </fieldset>
        <div <?=$this->get('users')->control('phrase','admin.save');?>>
            <input type="submit" value="<?=$this->get('langs')->text('admin','save',true,false);?>" />
        </div>
    </form>
</div>