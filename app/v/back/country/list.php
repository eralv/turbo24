<div class="page" id="adminContent">
    <h3>
        <a href="/ll/admin/country/add/">
            <?=$this->get('langs')->text('admin','country:add');?>
        </a>
    </h3>
    <hr />

    <table>
        <tr>
            <th style="width:15%;">Kods</th>
            <th>Nosaukums</th>
            <th>Darbības</th>
        </tr>
        <tbody data-sortable="country">
            <?php if(!empty($list)) :
                foreach($list as $item) : ?>
                    <tr data-id="<?=$item->id;?>">
                        <td><?=$item->code;?></td>
                        <td><?=$item->title;?></td>
                        <td><a href="/ll/admin/country/edit/<?=$item->id;?>">Rediģēt</a></td>
                    </tr>
                <?php endforeach;
            endif; ?>
        </tbody>
    </table>
</div>