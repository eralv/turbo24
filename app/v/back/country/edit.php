<div class="page" id="adminContent">

    <form action="/ll/admin/country/edit/<?=$item->id;?>" method="post">
        <fieldset>

            <div class="block-tabs-4">

                <ul class="tabs-list">

                    <?php foreach ($this->get('langs')->listLangs() as $lang): ?>

                        <li class="<?= $lang ?> <?= ($lang == $this->get('langs')->getLang() ? 'active' : '') ?>"><?= mb_strtoupper($lang) ?></li>

                    <?php endforeach; ?>

                </ul>

                <div class="block-content">

                    <?php foreach ($this->get('langs')->listLangs() as $lang): ?>

                        <div class="tab-block-1 <?= $lang ?>" style="display: <?= ($lang == $this->get('langs')->getLang() ? 'block' : 'none') ?>;">

                            <label>
                                <?=$this->get('langs')->text('admin','country:title');?>
                                <input type="text" value="<?=$item->getText('title', $lang);?>" name="country[title][<?=$lang;?>]" />
                            </label>

                        </div>

                    <?php endforeach; ?>

                </div>

            </div>

            <hr />

            <label>
                <?=$this->get('langs')->text('admin','country:code');?>
                <input type="text" value="<?=$item->code;?>" name="country[code]" />
            </label>

            <input type="submit" value="Saglabāt" />

        </fieldset>
    </form>

</div>