<?php
/**
 * @var array[] $list
 */
//var_dump($orders);
?>
<div class="">
<div class="col col-2-3" id="adminContent">
<div>
    <form method="post" action="">
        <table class="user-search">
            <tr>
                <td><?= $this->get('langs')->text('admin', 'user:search_by_name'); ?></td>
                <td><?= $this->get('langs')->text('admin', 'user:search_by_surname'); ?></td>
                <td><?= $this->get('langs')->text('admin', 'user:search_by_company'); ?></td>
                <td><?= $this->get('langs')->text('admin', 'user:date'); ?></td>
                <td></td>
            </tr>
            <tr>
                <td><input type="text"
                           name="search[name]" <?= isset($_POST['search']['name']) ? ('value="' . $_POST['search']['name'] . '"') : ''; ?> />
                </td>
                <td><input type="text"
                           name="search[surname]" <?= isset($_POST['search']['surname']) ? ('value="' . $_POST['search']['surname'] . '"') : ''; ?> />
                </td>
                <td><input type="text"
                           name="search[company]" <?= isset($_POST['search']['company']) ? ('value="' . $_POST['search']['company'] . '"') : ''; ?> />
                </td>
                <td><input type="text" class="datepicker"
                           name="search[date]" <?= isset($_POST['search']['date']) ? ('value="' . $_POST['search']['date'] . '"') : ''; ?> />
                </td>
                <td><input type="submit" value="Meklēt"/></td>
            </tr>
        </table>
    </form>
</div>
<hr/>
<hr/>

<div style="overflow-y:scroll;max-height:600px;border-bottom:1px solid silver;">
    <table id="user-list" style="width:100%;min-width:100%;">
        <tr id="user-list-head">
            <th><?= $this->get('langs')->text('admin', 'order_list:sender_name'); ?></th>
            <th><?= $this->get('langs')->text('admin', 'order_list:sender_contacts'); ?></th>
            <th><?= $this->get('langs')->text('admin', 'order_list:address_name'); ?></th>
            <th><?= $this->get('langs')->text('admin', 'order_list:address_contacts'); ?></th>
            <th><?= $this->get('langs')->text('admin', 'order_list:location_from'); ?></th>
            <th><?= $this->get('langs')->text('admin', 'order_list:location_to'); ?></th>
            <th><?= $this->get('langs')->text('admin', 'order_list:distance'); ?></th>
            <th><?= $this->get('langs')->text('admin', 'order_list:comments'); ?></th>
            <th><?= $this->get('langs')->text('admin', 'order_list:insurance'); ?></th>
            <?php if ($this->user->is()) : ?>
                <th><?= $this->get('langs')->text('admin', 'order_list:operator_id'); ?></th>
            <?php endif; ?>
            <th><?= $this->get('langs')->text('admin', 'order_list:driver_id'); ?></th>
            <th><?= $this->get('langs')->text('admin', 'order_list:date'); ?></th>
            <th><?= $this->get('langs')->text('admin', 'order_list:invoice'); ?></th>
            <th><?= $this->get('langs')->text('admin', 'order_list:status'); ?></th>
        </tr>
        <?php foreach ($orders as $order) : ?>
            <tr>
                <td>
                    <?php
                    echo $order['sender_name'] . ' ' . $order['sender_surname'];
                    if ($order['juridic']) {
                        echo '<br />(' . $order['company_name'] . ')';
                    } ?>
                </td>
                <td>
                    <?= $order['sender_phone']; ?><br/>
                    <?= $order['sender_email']; ?>
                </td>
                <td><?= $order['address_name']; ?> <?= $order['address_surname']; ?></td>
                <td>
                    <?= $order['address_phone']; ?><br/>
                    <?= $order['address_email']; ?>
                </td>
                <td><?= $order['location_from']; ?></td>
                <td><?= $order['location_to']; ?></td>
                <td><?= number_format(($order['distance'] / 1000), 2, '.', ''); ?></td>
                <td><?= $order['comments']; ?></td>
                <td style="position:relative;"><?=
                    ($order['insurance']
                        ? ('<span data-hint="#insurance_info' . $order['id'] . '" style="cursor:pointer;text-decoration:underline;">' . $this->get('langs')->text('admin', 'order_list:insurance-custom') . '</span>')

                        : $this->get('langs')->text('admin', 'order_list:insurance-standard'));?>

                    <div id="insurance_info<?= $order['id']; ?>"
                         style="text-align:left;position:absolute;border:1px solid silver;background:white;padding:5px;white-space:pre-line;z-index:25;display:none;"><?= $order['insurance_info']; ?></div>
                </td>
                <?php if ($this->user->admin()) : ?>
                    <td><?= $order['operator_id']; ?></td>
                <?php endif; ?>
                <td>
                    <form id="couriers_list<?= $order['id']; ?>" name="couriers_list" method="post" action="">
                        <select name="courier[driver_id]" class="courier">
                            <option value="0"></option>
                            <?php foreach ($couriers as $courier): ?>
                                <option
                                    value="<?= $courier['id'] ?>" <?= $courier['id'] == $order['driver_id'] ? 'selected' : '' ?>><?= $courier['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                        <input type="hidden" name="courier[id]" value="<?= $order['id'] ?>"/>
                    </form>
                </td>
                <td>
                    <?=
                    $order['task_date']
                        ? (date("d-m-Y H:i", $order['date']) . '<br /><b>' . date("d-m-Y H:i", $order['task_date']) . '</b>')
                        : ('<b>' . date("d-m-Y H:i", $order['date']) . '</b>');
                    ?>
                </td>
                <td>
                    <?php if ($order['status'] >= 2) : ?>
                        <a href="/ll/invoice/<?= $order['id']; ?>" target="_blank">
                            <span class="pdf_icon"></span>
                        </a>
                    <?php endif; ?>
                </td>
                <td>
                    <form id="order_list<?= $order['id']; ?>" name="order_list" method="post" action="">
                        <select name="status[status]" class="status">
                            <option
                                value="1" <?= $order['status'] == 1 ? 'selected' : '' ?>><?= $this->get('langs')->text('admin', 'order_list:status_new'); ?></option>
                            <option
                                value="2" <?= $order['status'] == 2 ? 'selected' : '' ?>><?= $this->get('langs')->text('admin', 'order_list:status_inprocess'); ?></option>
                            <option
                                value="3" <?= $order['status'] == 3 ? 'selected' : '' ?>><?= $this->get('langs')->text('admin', 'order_list:status_done'); ?></option>
                        </select>
                        <input type="hidden" name="status[id]" value="<?= $order['id'] ?>"/>
                    </form>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
<center>
    <?php $page = 0;
    while ($page++ < $num_pages): ?>
        <?php if ($page == $cur_page): ?>
            <b><?= $page ?></b>
        <?php else: ?>
            <a href="?page=<?= $page ?>"><?= $page ?></a>
        <?php endif ?>
    <?php endwhile ?>
</center>
<script>
    $(document).on('change', '.status',function () {
        $(this).parents('form:first').submit();
    }).on('change', '.courier', function () {
        $(this).parents('form:first').submit();
    });

    function playaudio() {
        var audioElement = document.createElement('audio');
        audioElement.setAttribute('src', '/www/audio/ring.mp3');
        audioElement.setAttribute('autoplay', 'autoplay');
        audioElement.load()

        $.get();

        audioElement.addEventListener("load", function () {
            //    audioElement.play();
        }, true);

    }
    ;
    setInterval(function () {
        $.ajax({
            type: "POST",
            url: "/ll/admin/order_list",
            data: "get_new_orders=1",
            success: function (arg) {
                var order = $.parseJSON(arg);
                //console.log(order);
                $.each(order, function (index, value) {
                    if (this.juridic == '1') {
                        var company = this.company_name;
                    } else {
                        var company = '';
                    }
                    var distance = Number(this.distance);
                    distance = distance / 1000;
                    var dist = distance.toFixed(2);

                    unix_date = Number(this.date);
                    var date = new Date(unix_date * 1000);
                    var year = date.getFullYear();
                    var month = date.getMonth();
                    var day = date.getDay();
                    var hour = date.getHours();
                    var minute = date.getMinutes();
                    if (this.insurance == '1') {
                        var insurance = '<span data-hint="#insurance_info' + this.insurance + '" style="cursor:pointer;text-decoration:underline;"><?=$this->get('langs')->text('admin','order_list:insurance-custom')?></span>';
                    } else {
                        var insurance = '<?=$this->get('langs')->text('admin','order_list:insurance-standard')?>';
                    }

                    if (this.status >= '2') {
                        var invoice = '<a href="/ll/invoice/' + this.id + '" target="_blank"><span class="pdf_icon"></span></a>';
                    } else {
                        var invoice = '';
                    }
                    var auto = ' <form id="couriers_list' + this.id + '" name="couriers_list" method="post" action="">\n\
                    <select name="courier[driver_id]" class="courier">\n\
                        <option value="0"></option>\n\
                        <?php foreach ($couriers as $courier): ?>\n\
                        <option value="<?=$courier['id']?>"><?=$courier['name']?></option>\n\
                        <?php endforeach;?>\n\
                    </select>\n\
                    <input type="hidden" name="courier[id]" value="' + this.id + '" />\n\
                </form>';
                    var status = '<form id="order_list' + this.id + '" name="order_list" method="post" action="">\n\
                    <select name="status[status]" class="status">\n\
                        <option value="1"><?=$this->get('langs')->text('admin','order_list:status_new');?></option>\n\
                        <option value="2"><?=$this->get('langs')->text('admin','order_list:status_inprocess');?></option>\n\
                        <option value="3"><?=$this->get('langs')->text('admin','order_list:status_done');?></option>\n\
                    </select>\n\
                    <input type="hidden" name="status[id]" value="<?=$order['id']?>" />\n\
                </form>';
                    $('#user-list #user-list-head').after('<tr>\n\
				<td>' + this.sender_name + ' ' + this.sender_surname + '<br/>' + company + '</td>\n\
				<td>' + this.sender_phone + ' ' + this.sender_email + '</td>\n\
				<td>' + this.address_name + ' ' + this.address_surname + '</td>\n\
				<td>' + this.address_phone + ' ' + this.address_email + '</td>\n\
				<td>' + this.location_from + '</td>\n\
				<td>' + this.location_to + '</td>\n\
				<td>' + dist + '</td>\n\
				<td>' + this.comments + '</td>\n\
				<td>' + insurance + '</td>\n\<?php if($this->user->admin()) : ?>
				<td>' + this.operator_id + '</td>\n\ <?php endif; ?>\n\
				<td>' + auto + '</td>\n\
				<td>' + day + '-' + month + '-' + year + ' ' + hour + ':' + minute + '</td>\n\
				<td>' + invoice + '</td>\n\
				<td>' + status + '</td>\n\
			    </tr>');
                    playaudio();
                });
            }
        });
    }, 5000);


</script>
</div>
</div>
