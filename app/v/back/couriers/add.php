<?php
/**
 * @var array[] $item
 */
?>
<div class="content">
    <div class="edit-field">
    <?php if($this->get('users')->rule()==Engine_Users::SUPERUSER) : ?>
        <h3><?=$this->get('langs')->text('admin','couriers_add:cant_edit');?></h3>

        <form action="" method="post" autocomplete="off">
            <label>
                <?=$this->get('langs')->text('admin','couriers_add:name');?><br />
                <input type="text" name="courier[name]" value="<?=$item['name'];?>" /><br />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','couriers_add:surname');?><br />
                <input type="text" name="courier[surname]" value="<?=$item['surname'];?>"/><br />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','couriers_add:phone');?><br />
                <input type="text" name="courier[phone]" value="<?=$item['phone'];?>" /><br />
            </label>
            
            <input type="submit" class="orange-button orange-grad" value="<?=$this->get('langs')->text('admin','save',true,false);?>" /><br />
        </form>
    <?php else : ?>
        <h3><?=$this->get('langs')->text('admin','couriers_add:not_found');?></h3>
    <?php endif; ?>
    </div>
</div>