<?php
/**
 * @var array[] $list
 */

?>
<div class="content">
    <div class="col col-2-3" id="adminContent">

	<h3>
	    <a href="/ll/admin/couriers/add/">
		<?=$this->get('langs')->text('admin','couriers:add');?>
	    </a>
	</h3>
	<hr />

	<table>
	    <tr>
		<th><?=$this->get('langs')->text('admin','couriers:name');?></th>
		<th><?=$this->get('langs')->text('admin','couriers:surname');?></th>
		<th><?=$this->get('langs')->text('admin','couriers:phone');?></th>
		<?php if($this->get('users')->rule()==Engine_Users::SUPERUSER) : ?>
		    <th><?=$this->get('langs')->text('admin','user:options');?></th>
		<?php endif; ?>
	    </tr>
	    <?php foreach($list as $courier) : ?>
		<tr>
		    <td><?=$courier['name'];?></td>
		    <td><?=$courier['surname'];?></td>
		    <td><?=$courier['phone'];?></td>
		    <td>
			<?php if(($this->get('users')->rule()==Engine_Users::SUPERUSER)) : ?>
			    <a href="/ll/admin/couriers/edit/<?=$courier['id'];?>"><?=$this->get('langs')->text('admin','option:edit');?></a> |
			    <a href="/ll/admin/couriers/delete/<?=$courier['id'];?>"
			       data-confirm="<?=$this->get('langs')->text('admin','confirm:delete-courier',true,false);?>"><?=$this->get('langs')->text('admin','option:delete');?></a>
			<?php endif; ?>
		    </td>
		</tr>
	    <?php endforeach; ?>
	</table>

    </div>
</div>