<?php
/**
 * @var array[] $item
 */
?>
<div class="content">
    <div class="edit-field">
    <?php if($item && !$this->get('users')->rule()==Engine_Users::SUPERUSER) : ?>
        <h3><?=$this->get('langs')->text('admin','couriers_edit:cant_edit');?></h3>
    <?php elseif($item) : ?>
        <form action="" method="post" autocomplete="off">
            <label>
                <?=$this->get('langs')->text('admin','couriers_edit:name');?><br />
                <input type="text" name="courier[name]" value="<?=$item['name'];?>" /><br />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','couriers_edit:surname');?><br />
                <input type="text" name="courier[surname]" value="<?=$item['surname'];?>"/><br />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','couriers_edit:phone');?><br />
                <input type="text" name="courier[phone]" value="<?=$item['phone'];?>" /><br />
            </label>
            
            <input type="submit" class="orange-button orange-grad" value="<?=$this->get('langs')->text('admin','save',true,false);?>" /><br />
        </form>
    <?php else : ?>
        <h3><?=$this->get('langs')->text('admin','couriers_edit:not_found');?></h3>
    <?php endif; ?>
    </div>
</div>