<?php
/**
 * @var array[] $locations
 */
?>
<div class="page contacts">

    <div class="description">
        <div class="text"><?=$tree->treeInfo['description'];?></div>

        <div class="sub-text"><?=$this->get('langs')->text('site','contacts:subtext');?></div>
    </div>

    <?php if(!empty($consultants)) : ?>
    <div class="agents">

        <?php foreach($consultants as $item) : ?>
        <div class="agent">
            <div class="person-info">
                <div class="image">
                    <img src="<?=CMS_Uploads::getPath($item->image['file'], $item->dir);?>" alt="<?=$item->name;?>" />
                </div>
                <div class="person">
                    <span class="prefix">ASdasdfdf</span>
                    <span class="position"><?=$item->position;?></span>
                    <span class="full-name"><?=$item->name;?></span>
                </div>
                <br class="clear" />
            </div>
            <div class="contact-info">
                <div class="phone">
                    <span class="title">Tālrunis:</span>
                    <span class="phones"><?=$item->phone;?></span>
                    <br class="clear" />
                </div>
                <div class="email">
                    <span class="title">E-pasts:</span>
                    <span class="emails"><?=$item->email;?></span>
                    <br class="clear" />
                </div>
            </div>
            <div class="additional-info">
                <div class="fax">
                    <span class="title">Fakss:</span>
                    <span class="fax-nr"><?=$item->fax;?></span>
                    <br class="clear" />
                </div>
                <div class="language">
                    <span class="title">Valodas:</span>
                    <span class="languages"><?=$item->lang;?></span>
                    <br class="clear" />
                </div>
                <div class="working-time">
                    <span class="title">Darba laiks:</span>
                    <span class="time"><?=$item->work_hours;?></span>
                    <br class="clear" />
                </div>
            </div>
            <br class="clear" />
        </div>
        <?php endforeach; ?>

    </div>
    <?php endif;?>

    <div class="locations">

        <?php $i = 0; foreach($locations as $loc) : ?>
            <div class="location">
                <div class="head">
                    <span class="loc"><?=$this->get('langs')->text('site','contacts:location-'. $loc['alias']);?></span>
                    <span class="address"><?=$loc['address'];?></span>
                    <span class="phone"><?=$this->get('langs')->text('site','contacts:phone_prefix');?> <?=$loc['phone'];?></span>
                    <br class="clear" />
                </div>
                <div id="map-office" data-map-init="<?=$loc['coord'];?>" style="height: 250px;"></div>
            </div>

            <?php if($i++==0) : ?><hr /><?php endif; ?>

        <?php endforeach; ?>

    </div>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script type="text/javascript">
        var initialize = function() {

            $('[data-map-init]').each(function(){

                var coord = $(this).data('map-init').split(','),
                    location = new google.maps.LatLng(coord[0], coord[1]),
                    map = new google.maps.Map(this, {
                        zoom: 14,
                        center: location
                    });

                new google.maps.Marker({
                    position: location,
                    map: map
                });
            });

        };

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>

</div>