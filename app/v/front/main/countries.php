<?php
/**
 * @var array[] $list
 */
if(!empty($list)) :
?>
<ul>
    <?php foreach($list as $country) : ?>
    <li><?=$country['code'];?></li>
    <?php endforeach; ?>
</ul>
<?php endif;