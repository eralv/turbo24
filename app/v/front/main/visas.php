<div class="page visas">

    <div class="visas-list">

        <?php foreach($tree->getMenu('visa') as $visa) : ?>

            <div class="visa" <?=$this->get('users')->control('menu',$visa['id']);?>>
                <div class="image">
                    <img src="<?=CMS_Uploads::getPath($visa['image'], 'tree');?>" alt="adsf" />
                </div>
                <div class="visa-info">
                    <div class="info">

                        <h2><?=$visa['title'];?></h2>
                        <p><?=$visa['description'];?></p>
                    </div>
                    <div class="button-holder">
                        <a href="<?=$tree->linkById($visa['id']);?>" class="button">
                            <span class="before"></span>
                            <span class="inner"><?=$this->get('langs')->text('site','visas:details');?></span>
                            <span class="after"></span>
                        </a>
                    </div>
                </div>
                <br class="clear" />
            </div>

        <?php endforeach; ?>

    </div>

    <?php if(setIf_isset($item, $consultant)) : ?>
    <div class="agents">

        <div class="agent blue">
            <div class="person-info">
                <div class="image">
                    <img src="<?=CMS_Uploads::getPath($item->image['file'], $item->dir);?>" alt="<?=$item->name;?>" />
                </div>
                <div class="person">
                    <span class="prefix">ASdasdfdf</span>
                    <span class="position"><?=$item->position;?></span>
                    <span class="full-name"><?=$item->name;?></span>
                </div>
                <br class="clear" />
            </div>
            <div class="contact-info">
                <div class="phone">
                    <span class="title">Tālrunis:</span>
                    <span class="phones"><?=$item->phone;?></span>
                    <br class="clear" />
                </div>
                <div class="email">
                    <span class="title">E-pasts:</span>
                    <span class="emails"><?=$item->email;?></span>
                    <br class="clear" />
                </div>
            </div>
            <div class="additional-info">
                <div class="fax">
                    <span class="title">Fakss:</span>
                    <span class="fax-nr"><?=$item->fax;?></span>
                    <br class="clear" />
                </div>
                <div class="language">
                    <span class="title">Valodas:</span>
                    <span class="languages"><?=$item->lang;?></span>
                    <br class="clear" />
                </div>
                <div class="working-time">
                    <span class="title">Darba laiks:</span>
                    <span class="time"><?=$item->work_hours;?></span>
                    <br class="clear" />
                </div>
            </div>
            <br class="clear" />
        </div>

    </div>
    <?php endif; ?>

</div>