<div class="page agents">

    <p><?=$tree->treeInfo['description'];?></p>

    <div class="center">
        <a href="#" class="button">
            <span class="before"></span>
            <span class="inner">Peislēgties mūsu rezervācijas sistēmai</span>
            <span class="after"></span>
        </a>
    </div><br />

    <hr />

    <form action="" method="post">

        <div class="fieldset">

            <table>
                <tbody>
                    <tr>
                        <td>

                            <label>
                                <span class="label">
                                    <?=$this->get('langs')->text('site','agents:form-company');?>
                                    <span class="required">*</span>
                                </span>
                                <input type="text" required name="agents[company]" />
                            </label>

                        </td>
                        <td>

                            <label>
                                <span class="label">
                                    <?=$this->get('langs')->text('site','agents:form-phone');?>
                                    <span class="required">*</span>
                                </span>
                                <input type="text" required name="agents[phone]" />
                            </label>

                        </td>
                        <td rowspan="2">

                            <label>
                                <span class="label">
                                    <?=$this->get('langs')->text('site','agents:form-info');?>
                                </span>
                                <textarea name="agents[info]"></textarea>
                            </label>

                        </td>
                    </tr>
                    <tr>
                        <td>

                            <label>
                                <span class="label">
                                    <?=$this->get('langs')->text('site','agents:form-person');?>
                                    <span class="required">*</span>
                                </span>
                                <input type="text" required name="agents[person]" />
                            </label>

                        </td>
                        <td>

                            <label>
                                <span class="label">
                                    <?=$this->get('langs')->text('site','agents:form-email');?>
                                    <span class="required">*</span>
                                </span>
                                <input type="text" required name="agents[email]" />
                            </label>

                        </td>
                    </tr>
                </tbody>
            </table>

        </div>

        <div class="required-msg">* <?=$this->get('langs')->text('site','agents:form-required_fields');?></div>
        <br class="clear" />

        <div class="center">

            <button type="submit" class="button">
                <span class="before"></span>
                <span class="inner">Nosūtīt pieteikumu</span>
                <span class="after"></span>
            </button>
        </div>

    </form>

</div>
