<?php
/**
 * @var CMS_TourType[] $list
 */
?>
<div class="submenu">

    <div class="col col-2-3">
        <div class="tour-types">
            <ul>
                <?php if(!empty($list)) :
                    foreach($list as $item) : ?>
                        <li>
                            <a href="<?=$tree->controllerLink('tours','default');?>/<?=$item->filter(array('type'=>$item->idf));?>">
                                <img src="<?=CMS_Uploads::getPath($item->image, 'tour_type');?>" alt="<?=$item->getTitle();?>" />
                                <div class="title"><?=$item->getTitle(true);?></div>
                            </a>
                        </li>
                    <?php endforeach;
                endif; ?>
            </ul>
        </div>
    </div>

</div>