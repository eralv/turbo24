<?php
/**
 * @var array[] $item
 */
?>
<div class="content">
    <?php if(!$this->get('users')->gi()) : ?>
        <h3><?=$this->get('langs')->text('admin','user:cant_edit');?></h3>
    <?php elseif($item) : ?>
        <form action="" method="post" autocomplete="off">
	    <input type="hidden" name="route[id]" value="<?=$item['id'];?>" />
            <label>
                <?=$this->get('langs')->text('admin','profile:location_from');?><br />
                <input type="text" name="route[location_from]" value="<?=$item['location_from'];?>" />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','profile:location_to');?><br />
                <input type="password" name="route[location_to]"  value="<?=$item['location_to'];?>"/>
            </label>
            <label>
                <?=$this->get('langs')->text('admin','profile:address_name');?><br />
                <input type="text" name="route[address_name]" value="<?=$item['address_name'];?>" />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','profile:address_surname');?><br />
                <input type="text" name="route[address_surname]" value="<?=$item['address_surname'];?>" />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','profile:address_name');?><br />
                <input type="text" name="route[address_phone]" value="<?=$item['address_phone'];?>" />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','profile:address_name');?><br />
                <input type="text" name="route[address_email]" value="<?=$item['address_email'];?>" />
            </label>
           
            <input type="submit" value="<?=$this->get('langs')->text('admin','save',true,false);?>" />
        </form>
    <?php else : ?>
        <h3><?=$this->get('langs')->text('admin','user:not_found');?></h3>
    <?php endif; ?>
</div>