<?php
/**
 * @var array[] $item
 */
$admin = (isset($admin) && $admin);
?>
<div class="content">
    <div class="table-left-holder content-bg">
<form method="post" autocomplete="off" <?=$admin?'action=""':'id="profile-form" action="/ll/profile"';?>>
    <table id="user">
        <?php if($admin) : ?>
            <tr>
                <td><?=$this->get('langs')->text('admin','user-edit:rules');?></td>
                <td>
                    <select name="user[rules]">
                        <?php foreach($this->get('users')->rules as $rule=>$label) :
                            if(($rule==Engine_Users::SUPERUSER && $item['rules']==Engine_Users::SUPERUSER) || $rule!=Engine_Users::SUPERUSER) : ?>
                                <option <?=$item['rules']==$rule?'selected':'';?> value="<?=$rule;?>"><?=$this->get('langs')->text('admin','user-edit:rule-'.$label,true,false);?></option>
                            <?php endif;
                        endforeach; ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <hr />
                </td>
            </tr>
        <?php endif; ?>
        <tr>
            <td style="width:165px;"><?=$this->get('langs')->text('admin','user_front:name');?></td>
            <td><input type="text" name="user[name]" value="<?=$item['name'];?>" /></td>
        </tr>
        <tr>
            <td><?=$this->get('langs')->text('admin','user_front:surname');?></td>
            <td><input type="text" name="user[surname]" value="<?=$item['surname'];?>"/></td>
        </tr>
        <tr>
            <td><?=$this->get('langs')->text('admin','user_front:pers_code');?></td>
            <td><input type="text" name="user[pers_code]" value="<?=$item['pers_code'];?>"/></td>
        </tr>
        <tr>
            <td><?=$this->get('langs')->text('admin','user_front:address');?></td>
            <td><input type="text" name="user[address]" value="<?=$item['address'];?>"/></td>
        </tr>
        <tr>
            <td><?=$this->get('langs')->text('admin','user_front:phone');?></td>
            <td><input type="text" name="user[phone]" value="<?=$item['phone'];?>"/></td>
        </tr>
        <tr>
            <td><?=$this->get('langs')->text('admin','user_front:email');?></td>
            <td><input type="text" name="user[email]" value="<?=$item['email'];?>"/></td>
        </tr>
        <tr <?=!$item['juridic']?'style="display:none;"':'';?> id="position-field">
            <td><?=$this->get('langs')->text('admin','user_front:position');?></td>
            <td><input type="text" name="user[position]" value="<?=$item['position'];?>"/></td>
        </tr>

        <?php if($soc = $this->get('users')->social()) : ?>
            <tr>
                <td><?=$this->get('langs')->text('admin','user_front:social_acc-'.$item['social']);?></td>
                <td><input type="text" disabled value="<?=$item['social_id'];?>" /></td>
            </tr>
        <?php else : ?>
            <tr>
                <td><?=$this->get('langs')->text('admin','user_front:password');?></td>
                <td><input type="text" name="user[password]"/></td>
            </tr>
        <?php endif; ?>

    </table>

    <div>
        <input id="uj" name="user[juridic]" type="checkbox" data-slide-toggle="#company, #position-field" <?=$item['juridic']?'checked':'';?> value="1" />
        <label for="uj" <?=$this->get('users')->control('phrase','site.profile:is_juridic');?>>
            <span></span>
            <?=$this->get('langs')->text('site','profile:is_juridic',null,false);?>
        </label>
        <div id="company" <?=!$item['juridic']?'style="display:none;"':'';?>>
            <table>
                <tr style="width:165px;">
                    <td><?=$this->get('langs')->text('admin','user_front:company_name');?></td>
                    <td><input type="text" name="user[company_name]" value="<?=$item['company_name'];?>" /></td>
                </tr>
                <tr>
                    <?php
                    $link_to = false;
                    if(is_numeric($item['reg_number'])) {
                        $link_to = 'https://www.lursoft.lv/uznemuma-pamatdati/'. $item['reg_number'];
                    }
                    ?>
                    <td>
                        <? if(false!==$link_to) : ?><a href="<?=$link_to?>" target="_blank"><? endif; ?>
                            <?=$this->get('langs')->text('admin','user_front:reg_number');?>
                        <? if(false!==$link_to) : ?></a<? endif; ?>
                    </td>
                    <td><input type="text" name="user[reg_number]" value="<?=$item['reg_number'];?>"/></td>
                </tr>
                <tr>
                    <td><?=$this->get('langs')->text('admin','user_front:pvn_number');?></td>
                    <td><input type="text" name="user[pvn_number]" value="<?=$item['pvn_number'];?>"/></td>
                </tr>
                <tr>
                    <td><?=$this->get('langs')->text('admin','user_front:jur_address');?></td>
                    <td><input type="text" name="user[jur_address]" value="<?=$item['jur_address'];?>"/></td>
                </tr>
                <tr>
                    <td><?=$this->get('langs')->text('admin','user_front:fact_address');?></td>
                    <td><input type="text" name="user[fact_address]" value="<?=$item['fact_address'];?>"/></td>
                </tr>
                <tr>
                    <td><?=$this->get('langs')->text('admin','user_front:bank_name');?></td>
                    <td><input type="text" name="user[bank_name]" value="<?=$item['bank_name'];?>"/></td>
                </tr>
                <tr>
                    <td><?=$this->get('langs')->text('admin','user_front:bank_swift');?></td>
                    <td><input type="text" name="user[bank_swift]" value="<?=$item['bank_swift'];?>"/></td>
                </tr>
                <tr>
                    <td><?=$this->get('langs')->text('admin','user_front:iban_number');?></td>
                    <td><input type="text" name="user[iban_number]" value="<?=$item['iban_number'];?>"/></td>
                </tr>
            </table>
        </div>
    </div>

    <div <?=$this->get('users')->control('phrase','site.profile:save');?>>
        <input class="submit-button submit-button-login orange-grad" type="submit" value="<?=$this->get('langs')->text('site','profile:save',true,false);?>" />
    </div>
</form>
    </div>
    <div class="table-right-holder">
        <table class="orders-table">
            <thead>
                <!--tr>
                    <td><?=$this->get('langs')->text('admin','user:datums');?></td>
                    <td><?=$this->get('langs')->text('admin','user:distance');?></td>
                    <td style="width:25px;"><?=$this->get('langs')->text('admin','user:pdf');?></td>
                </tr-->
                <tr>
                    <th colspan="4" class="table_head">
                        <?=$this->get('langs')->text('site','user_front:my_orders');?>
                        <?php if(!$admin) : ?>
                            <a href="/ll/#order">
                                <?=$this->get('langs')->text('admin','user_front:new_order');?>
                            </a>
                        <?php endif; ?>
                    </th>
                </tr>
            </thead>
            <?php if(!empty($orders)) :
                foreach($orders as $order):?>
                    <tr>
                        <td style="text-align: left;">
                            <span>
                                <b>No:</b> <?=$order['location_from'];?>
                            </span><br />
                            <span>
                                <b>Uz:</b> <?=$order['location_to'];?>
                            </span>
                        </td>
                        <td style="width:80px;"><?=number_format(($order['distance'] / 1000), 2, '.', ''); ?> km</td>
                        <td style="width:80px;text-align:right;">€ <?=CMS_Orders::calcPrice($order['distance'], $order['rate'])?></td>
                        <td style="width:40px;"><a target="_blank" href="/ll/order/<?=$order['id'];?>">
                            <span class="pdf_icon"></span>
                        </a></td>
                    </tr>
                <?php endforeach;
            else : ?>
                <tr>
                    <td colspan="4">
                        <?=$this->get('langs')->text('site','profile:no_orders');?>
                    </td>
                </tr>
            <?php endif; ?>
        </table>

        <?php if(!$admin) : ?>
            <table class="orders-table">
                <thead>
                <!--tr>
                <td><?=$this->get('langs')->text('admin','user:location_from');?></td>
                <td><?=$this->get('langs')->text('admin','user:location_to');?></td>
                <td><?=$this->get('langs')->text('admin','user:options');?></td>
            </tr-->
                    <tr>
                        <th colspan="3">
                            <?=$this->get('langs')->text('site','user_front:saved_routes');?>
                            <a href="/ll/profile/add_route/">
                                <?=$this->get('langs')->text('admin','user_front:add_route');?>
                            </a>
                        </th>
                    </tr>
                </thead>
                <?php if(!empty($routes)) : ?>
                    <tr class="small-head">
                        <th>No</th>
                        <th>Uz</th>
                        <th>Opcijas</th>
                    </tr>
                    <?php foreach($routes as $route):?>
                        <tr class="routes">
                            <td><?=$route['location_from']?></td>
                            <td><?=$route['location_to']?></td>
                            <td>
                                <a href="/ll/profile/edit_route/<?=$route['id'];?>">Rediģēt</a> |
                                <a href="/ll/profile/delete_route/<?=$route['id'];?>"
                                   data-confirm="<?=$this->get('langs')->text('admin','confirm:delete-route',true,false);?>">Dzēst</a>
                            </td>
                        </tr>
                    <?php endforeach;
                else: ?>
                    <tr>
                        <td colspan="3"><?=$this->get('langs')->text('site','profile:no_routes');?></td>
                    </tr>
                <?php endif; ?>
            </table>
        <?php endif; ?>

    </div>
</div>
