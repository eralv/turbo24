<?php
/**
 * @var array[] $item
 */
?>
<div class="content content-bg">
    <div class="edit-field" style="margin:0;float:left;">
    <?php if(!$this->get('users')->gi()) : ?>
        <h3><?=$this->get('langs')->text('admin','user_front_add_route:cant_edit');?></h3>
    <?php elseif($item) :  ?>
        <form action="" method="post" autocomplete="off">
            <label>
                <?=$this->get('langs')->text('admin','user_front_add_route:location_from');?><br />
                <input type="text" name="route[location_from]" value="<?=$item['location_from'];?>" id="cost_departure_place" /><br />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','user_front_add_route:location_to');?><br />
                <input type="text" name="route[location_to]"  value="<?=$item['location_to'];?>" id="cost_arrival_place" /><br />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','user_front_add_route:address_name');?><br />
                <input type="text" name="route[address_name]" value="<?=$item['address_name'];?>" /><br />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','user_front_add_route:address_surname');?><br />
                <input type="text" name="route[address_surname]" value="<?=$item['address_surname'];?>" /><br />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','user_front_add_route:address_phone');?><br />
                <input type="text" name="route[address_phone]" value="<?=$item['address_phone'];?>" /><br />
            </label>
            <label>
                <?=$this->get('langs')->text('admin','user_front_add_route:address_email');?><br />
                <input type="text" name="route[address_email]" value="<?=$item['address_email'];?>" /><br />
            </label>
           
            <input type="submit" class="ororange-button orange-grad" value="<?=$this->get('langs')->text('admin','save',true,false);?>" /><br />
        </form>
    <?php else : ?>
        <h3><?=$this->get('langs')->text('admin','user_front_add_route:not_found');?></h3>
    <?php endif; ?>
    </div>
    <div id="map-block" style="width:750px;float:right;height:350px;"></div>
</div>