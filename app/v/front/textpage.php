<div class="content">
    <div class="news" style="float:left;width:66%;" <?= $this->get('users')->control('menu', $tree->treeId)?>>
	    <?=$tree->treeInfo['description'];?>
    </div>
    <div class="news" style="float:right;width:29%;">
        <div <?=$this->get('users')->control('phrase','site.textpage:sidebar_1');?>>
            <?=$this->get('langs')->text('site','textpage:sidebar_1',null,false);?>
        </div>
        <div <?=$this->get('users')->control('phrase','site.textpage:sidebar_2');?>>
            <?=$this->get('langs')->text('site','textpage:sidebar_2',null,false);?>
        </div>
    </div>
    <br class="clear" />
</div>