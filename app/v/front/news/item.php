<?php
/**
 * @var array[] $list
 */
?>
<div class="content news">
    <h2> <?=$item->title?></h2>
    <div>
        <?php if($item->image) : ?>
            <img src="<?=$item->getImage();?>" alt="<?=$item->title;?>" style="float:left;max-width:50%;margin: 0 8px 8px 0;" />
        <?php endif; ?>
        <?=$item->news?>
    </div>
</div>
