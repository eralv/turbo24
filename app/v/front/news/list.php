<?php
/**
 * @var CMS_News[] $list
 * @var integer $pages
 * @var integer $page
 */
?>
<div class="content news">
    <?php foreach($list as $item) : ?>
        <h3>
            <a href="<?=$tree->linkById($tree->treeId);?>/<?=$item->id;?>"><?=$item->title?></a>
        </h3>
        <div>
            <?php if($item->image) : ?>
                <img src="<?=$item->getImage();?>" alt="<?=$item->title;?>" style="float:left;max-width:150px;margin: 0 10px 0 0;" />
            <?php endif; ?>
            <?=substr(strip_tags($item->news), 0, 250) . (strlen(strip_tags($item->news))>250?'...':'');?>
            <br class="clear" />
        </div>
    <?php endforeach; ?>

    <?php if($pages > 1) : ?>
    <div class="pagination-news" >
            <?php for($i = 1; $i <= $pages; $i++) :
                if($page!=$i) : ?>
                    <a href="?page=<?=$i;?>"><?=$i?></a>
                <?php else : ?>
                    <b><?=$i?></b>
                <?php endif;
            endfor; ?>
    </div>
    <?php endif; ?>
</div>
